from pyspark import Row


class TestBizLoader:
    def load(self, spark):
        return spark.createDataFrame([
            Row(
                id='123',
                biz_id='biz_id_1',
                biz_name='biz_name_1',
                biz_code='biz_code_1',
                biz_description='biz_description_1',
                qr_code='qr_code_1',
                timestamp=123,
            )
        ])