from bomoda.cleaners import BizCleaner, PageCleaner, ClicksCleaner



class Pipeline:
    def run(self, spark, biz_loader, click_loader, pages_loader):
        biz = biz_loader.load(spark)
        pages = pages_loader.load(spark)
        clicks = click_loader.load(spark)

        biz_cleaner = BizCleaner()
        pages_cleaner = PageCleaner()
        clicks_cleaner = ClicksCleaner()

        biz_cleaned = biz_cleaner.clean(biz)
        pages_cleaned = pages_cleaner.clean(pages)
        clicks_cleaned = clicks_cleaner.clean(clicks)

        pages_cleaned \
            .join(biz_cleaned, 'biz_id', how='left') \
            .join(clicks_cleaned, 'url', how='left') \
            .select(
                'title',
                'url',
                'content',
                'read_number',
                'like_number',
                'biz_id',
                'biz_description',
                'biz_name',
                'publish_date',
        )

        return pages_cleaned