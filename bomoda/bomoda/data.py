import re

from pyspark.sql.functions import udf
from pyspark.sql.types import StructType, StructField, StringType, LongType, DateType
from pyspark.sql import functions as F


class TSVBizLoader:
    def __init__(self, path):
        self.path = path

    def load(self, spark):
        data = spark.read.csv(
            self.path,
            sep='\t',
            schema=StructType([
                StructField('id', StringType()),
                StructField('biz_id', StringType()),
                StructField('biz_name', StringType()),
                StructField('biz_code', StringType()),
                StructField('biz_description', StringType()),
                StructField('qr_code', StringType()),
                StructField('timestamp', LongType())
            ])
        )

        return data


class TSVClickLoader:
    def __init__(self, path):
        self.path = path

    def load(self, spark):
        data = spark.read.csv(
            self.path,
            sep='\t',
            schema=StructType([
                StructField('id', StringType()),
                StructField('url', StringType()),
                StructField('title', StringType()),
                StructField('read_number', LongType()),
                StructField('like_number', LongType()),
                StructField('timestamp', LongType()),
                StructField('date', DateType())
            ])
        )

        return data


class HTMLPagesLoader:
    _DOCUMENT_PATTERN = re.compile("^<URL>(?P<url>.*)</URL>\n" +
                                   "<TITLE>(?P<title>.*)</TITLE>\n" +
                                   "<BODY>(?P<body>.*)</BODY>\n" +
                                   "<TIME>(?P<time>.*)</TIME>\n" +
                                   "<DATE>(?P<date>.*)</DATE>\n" +
                                   "<ID>(?P<id>.*)</ID>\n+$", flags=re.DOTALL)

    _SCHEMA = StructType([
        StructField('url', StringType()),
        StructField('title', StringType()),
        StructField('body', StringType()),
        StructField('time', LongType()),
        StructField('date', StringType()),
        StructField('id', LongType()),
    ])

    def __init__(self, path):
        self.path = path

    def load(self, spark):
        spark.sparkContext._jsc.hadoopConfiguration().set("textinputformat.record.delimiter", "<URL>")

        # TODO: Instead of changing a global variable, we should create a sparkContext.hadoopFile and pass this option there

        rdd = spark.sparkContext.textFile(self.path).map(lambda r: (r,))
        df = spark.createDataFrame(rdd, schema=StructType([StructField('value', StringType())]), verifySchema=False)

        df = (
            df
                .where("value != ''")  # drop first, empty value
                .select(F.concat(F.lit('<URL>'), F.col('value')).alias('value'))  # add <URL> separator at the beginning
        )

        parse_document = udf(self._SCHEMA)(self._parse_document)

        return df.select(parse_document('value').alias('data')).selectExpr('data.*')

    def _parse_document(self, page):
        m = re.match(self._DOCUMENT_PATTERN, page)
        if m:
            document = m.groupdict()
            return (
                document['url'],
                document['title'],
                document['body'],
                int(document['time']),
                document['date'],
                int(document['id']),
            )
        else:
            raise Exception("Couldn't parse!")
