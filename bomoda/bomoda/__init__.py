# -*- coding: utf-8 -*-

"""Top-level package for bomoda."""

__author__ = """Mateusz Buskiewicz"""
__email__ = 'buskiewicz@gmail.com'
__version__ = '0.0.1'
