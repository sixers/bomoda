import html
import re

from bs4 import BeautifulSoup
from datetime import date
from pyspark.sql.functions import udf, regexp_replace
from pyspark.sql import functions as F
from pyspark.sql.types import StringType, LongType, DateType, TimestampType


@udf(StringType())
def _double_unescape_html_text(text):
    """
    Unescapes html content, including "double-escaped" html, which include cases like &amp;nbsp;
    """
    if text is not None:
        return html.unescape(html.unescape(text)).replace('\xa0', ' ')


def _merge_subsequent_whitespace(text):
    """
    Reducess excessive whitespace (including newlines), by merging subsequent whitespace values together
    :param text:
    :return:
    """
    return regexp_replace(regexp_replace(text, '\s+', ' '), '(<br/>\s*)+', '\n')


@udf(StringType())
def _extract_raw_content_from_html(body):
    """
    Extracts text contents of HTML page (strips all the tags)
    """
    soup = BeautifulSoup(body)
    [s.extract() for s in soup('script')]
    return soup.body.text


@udf(StringType())
def _extract_biz_from_url(url):
    """
    Extracts biz_id form url in a weixin form
    """
    ARTICLE_URL_PATTERN = r"^http://.*?__biz=(?P<biz>.*)&mid.*$"
    m = re.match(ARTICLE_URL_PATTERN, url)
    if m:
        return m.groupdict()['biz']
    else:
        raise Exception("Incorrect URL!")


@udf(LongType())
def _extract_publish_time_from_body(body):
    """
    Extracts publish time from article body, bo looking and variable named ct
    """
    PUBLISH_TIMESTAMP_PATTERN = r"var ct = \"(?P<ct>\d+)\";"
    cts = re.findall(PUBLISH_TIMESTAMP_PATTERN, body)
    if len(cts) != 1:
        raise Exception("Incorrect number of publish times in body!")
    return int(cts[0])


class Cleaner:
    """
    Cleaner takes a raw dataset and outputs a new, parsed dataset (possibly with new columns)
    """
    columns = {}
    new_columns = {}
    keep_columns = []
    drop_duplicates = None

    def __init__(self):
        pass

    def clean(self, data):
        # Add new columns
        for (name, column) in self.new_columns.items():
            data = data.withColumn(name, column)

        # Apply column fixers
        for (name, fixers) in self.columns.items():
            for fixer in fixers:
                data = data.withColumn(name, fixer(name))

        # Drop duplicates
        if self.drop_duplicates:
            data = data.drop_duplicates(self.drop_duplicates)

        # Keep only selected columns
        if self.keep_columns:
            return data.select(*self.keep_columns)
        else:
            return data


class BizCleaner(Cleaner):
    columns = {
        'biz_name': [_double_unescape_html_text],
        'biz_description': [_double_unescape_html_text, _merge_subsequent_whitespace],
    }

    drop_duplicates = ['biz_id']
    keep_columns = ['biz_id', 'biz_name', 'biz_code', 'biz_description']


class PageCleaner(Cleaner):
    columns = {
        'title': [_double_unescape_html_text],
    }

    new_columns = {
        'biz_id': _extract_biz_from_url('url'),
        'content': _extract_raw_content_from_html('body'),
        'publish_date': _extract_publish_time_from_body('body').cast(TimestampType()).cast(DateType()),
    }

    keep_columns = ['title', 'url', 'biz_id', 'content', 'publish_date']


class ClicksCleaner(Cleaner):
    columns = {
        'title': [_double_unescape_html_text]
    }

    keep_columns = ['url', 'read_number', 'like_number']

    def clean(self, data):
        data = super().clean(data)

        # select max read_number by url
        return data.groupBy('url') \
            .agg(F.max(F.struct('read_number', 'like_number')).alias('data')) \
            .selectExpr('url', 'data.*')
