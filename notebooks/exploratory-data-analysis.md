
# Bomoda Data Engineering Exercise - Exploratory Data Analysis

The goal for this exercise is to develop an ETL solution that cleans the provided page data and creates relations between the other provided files, ultimately joining them as a single CSV file per the schemas described below. The approach you take is purely your choice, but please note that this is a small sample of real data, and as such you should take into account any scaling and edge-case concerns. When you are finished, please push your code to a Github or Bitbucket repository and make it available to us.


**Output Result Schema**

- Article title
- Article URL
- Content without HTML
- Read number
- Like number
- Biz ID
- Biz description
- Biz name
- Publish date


Archive the resulting files as part of the process

The data follows the following data schema:


**Biz - Delimited by tabs**

- `ID`: Source database incremental id
- `Biz ID`: Biz account unique id
- `Biz Name`: Account name shown on the platform
- `Biz Code`: Account registered name
- `Biz Description`: Description of account
- `QRcode`: Account’s QR code
- `Timestamp`: Time of record creation


**Click - Delimited by tabs**

- `ID`: Source database incremental id
- `URL`: Corresponding page URL 
- `Title`: Corresponding page title
- `Read Number`: Page’s read number
- `Like Number`: Page’s like number
- `Timestamp`: Time of record creation


**Page - Raw html data**

- `ID`: Source database incremental id
- `URL`: URL for page content
- `Title`: Page title
- `Content`: Page raw html content *
- `Publish Date`: Date the article was made public *
- `Timestamp`: Time of record creation

> Within the content is a javascript variable “ct” that denotes publish time


Notes:

1. Click data might have duplicate records in terms of URL, please extract the one with maximum read number.
2. Biz ID can be parsed from URL.
3. If the final CSV result is too large to push to repository, please use cloud like google drive to share result with us.


## Loading files

First, I'm going to load all the files from the zip to DataFrames and see if the schema matches description.

I'm going to use Pandas DataFrames, because the data is fairly small (~1.5 GB uncompressed). I know that it's only the subset of the data, and the actual data is much larger. However, this is exploratory data analysis and the purpose of it is to get to know the data, and working on a subset of data is perfectly fine. The final solution will be implemeneted in something more scalable


```python
import pandas as pd
%matplotlib inline
```


```python
!head -n1 ../wechat_data_medium/weixin_biz
```

    


Looks like the data doesn't have a header


```python
biz = pd.read_table("../wechat_data_medium/weixin_biz", names=[
    'id',
    'biz_id',
    'biz_name',
    'biz_code',
    'biz_description',
    'qr_code',
    'timestamp',
], low_memory=False, index_col=False)
biz.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>biz_id</th>
      <th>biz_name</th>
      <th>biz_code</th>
      <th>biz_description</th>
      <th>qr_code</th>
      <th>timestamp</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2394660</td>
      <td>MzIyNjA3ODA0OA==</td>
      <td>天津师大史学社</td>
      <td>tianshidashixueshe</td>
      <td>天津师大史学社是天津师范大学学生自愿结成的进行历史学习与交流的学术型社团，致力于为广大师大学...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1459685449</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2394664</td>
      <td>MzI5MTEwMzUxMA==</td>
      <td>爆笑女神经</td>
      <td>bxns666888</td>
      <td>本平台为您推送史上最最无厘头的搞笑段子，欢迎关注姐！！！</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1459685484</td>
    </tr>
    <tr>
      <th>2</th>
      <td>2394668</td>
      <td>MjM5NTQ0MzQ1Nw==</td>
      <td>专治各种不服</td>
      <td>zhibufu</td>
      <td>花花世界，无奇不有，汇聚各种奇闻异事，亮点内容，让您的生活从此丰富多彩。不服哥：专治各种不服...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1459685489</td>
    </tr>
    <tr>
      <th>3</th>
      <td>2394672</td>
      <td>MzIyMDIwMjkyOQ==</td>
      <td>名牌皮具厂批发商会</td>
      <td>NaN</td>
      <td>香奈儿LV爱马仕MIUMIU古驰等名牌原版&amp;nbsp;超A原单女包！各名牌经典，各名牌包包最...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1459685495</td>
    </tr>
    <tr>
      <th>4</th>
      <td>2394676</td>
      <td>MzIwMjI2NDMzOQ==</td>
      <td>说话逻辑与技巧</td>
      <td>shuohua148</td>
      <td>教会你如何说话才让人感觉最舒服、最感动;帮助你快速掌握说话的技巧,说话改变命运;口才决定人生!</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1459685504</td>
    </tr>
  </tbody>
</table>
</div>



Let's run some basic exploratory data analysis


```python
biz.shape
```




    (2559818, 7)



We have 2.5 milion rows and 7 columns

## Checking `biz_id`

Let's check a sample of values


```python
biz.biz_id.sample(n=10)
```




    856986     MzA3MDk0NjMzOQ==
    441218     MjM5NzkyNDc2NQ==
    850662     MjM5ODk2Nzg2NQ==
    1573742    MzAxMDM5NTc3NA==
    1955882    MzIwNjI0NzIxNA==
    409523     MjM5NDAyMDk0OQ==
    2212025    MzA4ODg2MDMyMQ==
    1519154    MzA5OTQxMjg3NA==
    396943     MzAwOTI1NjA0Mg==
    764529     MzI1ODE4NTk0OA==
    Name: biz_id, dtype: object



Let's check missing values


```python
biz.biz_id.isnull().value_counts()
```




    False    2559817
    True           1
    Name: biz_id, dtype: int64



We have a single null value in `biz_id`. Let's check it


```python
missing_biz = biz[biz.biz_id.isnull() | (biz.biz_id.str.len() == 0)]
missing_biz
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>biz_id</th>
      <th>biz_name</th>
      <th>biz_code</th>
      <th>biz_description</th>
      <th>qr_code</th>
      <th>timestamp</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1328037</th>
      <td>2236033</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1453705561</td>
    </tr>
  </tbody>
</table>
</div>



The only information that we have is `timestamp` and `qr_code`.


```python
missing_biz.qr_code.iloc[0]
```




    'http://mp.weixin.qq.com/mp/qrcode?scene=10000004&size=102&__biz='



Let's go back to this when we better understand `qr_code`, maybe we'll be able to match this row to some actual Biz.


```python
biz_id_value_counts = biz.biz_id.value_counts()
biz_id_value_counts.head()
```




    MzA3NjA2NDIxNQ==    2
    MzIxNDIyOTMzOQ==    2
    MzIyNzIxNTA2Mg==    2
    MzI1MjIwNTI4Ng==    2
    MzIwODE5NDMyOQ==    2
    Name: biz_id, dtype: int64



There are definetely some duplicates. Let's check how many


```python
(biz_id_value_counts >= 2).sum()
```




    13



13 out of 2.5 million, so it's not that much.


```python
duplicated_biz_ids = biz_id_value_counts[biz_id_value_counts >= 2].index
```

Let's see if we can easily resolve those duplicates


```python
biz[biz.biz_id.isin(duplicated_biz_ids)].drop_duplicates().shape
```




    (13, 7)



Looks like we can simply drop those duplicates


```python
biz = biz.drop_duplicates(subset=['biz_id'])
```

## Checking `biz_name`


```python
biz.biz_name.sample(n=10)
```




    511086            GM工作室
    2315766       MISSVLOVE
    1820745    柳林白瑾舞韵舞蹈培训中心
    1397401          大肠癌那些事
    1855896      道通汽车导航地图升级
    287158      青岛一木家具上海直营店
    1409278           说话智慧学
    780752           老板商业智慧
    2062718             银盛祥
    1335246            一人一犬
    Name: biz_name, dtype: object




```python
missing_biz_name = biz[biz.biz_name.isnull() | (biz.biz_name.str.len() == 0)]
missing_biz_name.shape
```




    (17249, 7)




```python
missing_biz_name.sample(n=10)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>biz_id</th>
      <th>biz_name</th>
      <th>biz_code</th>
      <th>biz_description</th>
      <th>qr_code</th>
      <th>timestamp</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2368872</th>
      <td>1660075</td>
      <td>MzA4ODExMjM1OA==</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1439289148</td>
    </tr>
    <tr>
      <th>2266733</th>
      <td>1251519</td>
      <td>MjM5MjM4MDQ0Ng==</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436317324</td>
    </tr>
    <tr>
      <th>2529325</th>
      <td>2301887</td>
      <td>MzA4ODYzMjQzMw==</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1457538874</td>
    </tr>
    <tr>
      <th>1719249</th>
      <td>1431394</td>
      <td>MzA4NDk5Njc2MA==</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1437370602</td>
    </tr>
    <tr>
      <th>987162</th>
      <td>872533</td>
      <td>MzAxNDYxNTY1Nw==</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434789538</td>
    </tr>
    <tr>
      <th>1732959</th>
      <td>1486234</td>
      <td>MzAxODA0NDgxMA==</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1437723413</td>
    </tr>
    <tr>
      <th>964200</th>
      <td>780685</td>
      <td>MzA3MDI3NzAwMA==</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434577822</td>
    </tr>
    <tr>
      <th>1654667</th>
      <td>1173066</td>
      <td>MzA3NTgzODEwNw==</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435929538</td>
    </tr>
    <tr>
      <th>2191286</th>
      <td>949731</td>
      <td>MzA3MzYwMjYwNw==</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435014789</td>
    </tr>
    <tr>
      <th>389428</th>
      <td>851360</td>
      <td>MzAxMjU5MzcwOA==</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434565463</td>
    </tr>
  </tbody>
</table>
</div>



Observation: There are much more missing names than ids

Maybe QR code has some info?


```python
missing_biz_name.qr_code.sample(10).values
```




    array(['http://mp.weixin.qq.com/mp/qrcode?scene=10000004&size=102&__biz=MzA4NjkwNDEzOQ==',
           'http://mp.weixin.qq.com/mp/qrcode?scene=10000004&size=102&__biz=MzAxNTYzOTk0MQ==',
           'http://mp.weixin.qq.com/mp/qrcode?scene=10000004&size=102&__biz=MzAwOTM4MzIzMg==',
           'http://mp.weixin.qq.com/mp/qrcode?scene=10000004&size=102&__biz=MjM5NDg0OTIwMQ==',
           'http://mp.weixin.qq.com/mp/qrcode?scene=10000004&size=102&__biz=MzA3MjU5MTM5NQ==',
           'http://mp.weixin.qq.com/mp/qrcode?scene=10000004&size=102&__biz=MzA3NDYyNTU3Mg==',
           'http://mp.weixin.qq.com/mp/qrcode?scene=10000004&size=102&__biz=MzA3NjkyNjM3OQ==',
           'http://mp.weixin.qq.com/mp/qrcode?scene=10000004&size=102&__biz=MzA3MzU3MjkzMQ==',
           'http://mp.weixin.qq.com/mp/qrcode?scene=10000004&size=102&__biz=MzA4NjAwMTI3MA==',
           'http://mp.weixin.qq.com/mp/qrcode?scene=10000004&size=102&__biz=MzIyMTAwMTg1OQ=='],
          dtype=object)



Looks like QR code has only Biz ID in it.

Let's check duplicates


```python
biz_name_value_counts = biz.biz_name.value_counts()
biz_name_value_counts.head(n=10)
```




    新注册公众号           37066
    美文心赏               321
    IMU幻响LED智能护眼灯      315
    Q2485738554        218
    韩国ZINGO柠檬杯         205
    心灵日记               197
    成功新天地              184
    陈安之                174
    智慧人生               160
    健康养生               118
    Name: biz_name, dtype: int64




```python
print((biz_name_value_counts >= 2).sum())
print(biz_name_value_counts[biz_name_value_counts >=2].sum())
```

    99698
    325377


Looks like 99698 biz names are duplicated, and in total they occur in 325377 rows.

It's hard for me to judge if that's ok or not. I can try to translate some of the names to see if they're not some "encodings" like "empty value" in chinese, and they really mean that there is no value.


```python
import sys
```


```python
!{sys.executable} -m pip install googletrans
```

    Collecting googletrans
      Downloading googletrans-2.2.0.tar.gz
    Requirement already satisfied: requests in /Users/mateusz/.virtualenvs/deep-learning-with-python/lib/python3.6/site-packages (from googletrans)
    Requirement already satisfied: chardet<3.1.0,>=3.0.2 in /Users/mateusz/.virtualenvs/deep-learning-with-python/lib/python3.6/site-packages (from requests->googletrans)
    Requirement already satisfied: urllib3<1.23,>=1.21.1 in /Users/mateusz/.virtualenvs/deep-learning-with-python/lib/python3.6/site-packages (from requests->googletrans)
    Requirement already satisfied: certifi>=2017.4.17 in /Users/mateusz/.virtualenvs/deep-learning-with-python/lib/python3.6/site-packages (from requests->googletrans)
    Requirement already satisfied: idna<2.7,>=2.5 in /Users/mateusz/.virtualenvs/deep-learning-with-python/lib/python3.6/site-packages (from requests->googletrans)
    Building wheels for collected packages: googletrans
      Running setup.py bdist_wheel for googletrans ... [?25ldone
    [?25h  Stored in directory: /Users/mateusz/Library/Caches/pip/wheels/65/11/fc/44d36c61c9a0405de9f46022f503de767b95003065b448aed3
    Successfully built googletrans
    Installing collected packages: googletrans
    Successfully installed googletrans-2.2.0
    [33mYou are using pip version 9.0.1, however version 9.0.3 is available.
    You should consider upgrading via the 'pip install --upgrade pip' command.[0m



```python
from googletrans import Translator
translator = Translator()
```


```python
translations = [translator.translate(text).text for text in biz_name_value_counts.head(n=10).index]
translations
```




    ['Newly registered public number',
     'Appreciation of the United States',
     'IMU LED smart eye lamp',
     'Q 485738554',
     'Korea ZINGO Lemon Cup',
     'Diary of mind',
     'New world of success',
     'Chen Anzhi',
     'Smart life',
     'Healthy health']



The first entry, which occurs 37066 times is likely some placeholder.

**We could replace it with null. I'm not going to do this right now, but it's one of the question I'd have to the team.**

Let's see the distribution of lengths of texts, to check if there are any outliers.


```python
import numpy as np
biz_name_len = biz.biz_name.str.len()
biz_name_len.describe(percentiles=[0.5, 0.75, 0.9, 0.95, 0.999]).astype(int)
```




    count    2542556
    mean           6
    std            2
    min            1
    50%            6
    75%            8
    90%           10
    95%           12
    99.9%         22
    max          200
    Name: biz_name, dtype: int64



Looks like there is an outlier with 200 characters len


```python
biz_long_names = biz.loc[biz_name_len[biz_name_len > 20].sort_values(ascending=False).index]
print(biz_long_names.shape)
biz_long_names
```

    (3712, 7)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>biz_id</th>
      <th>biz_name</th>
      <th>biz_code</th>
      <th>biz_description</th>
      <th>qr_code</th>
      <th>timestamp</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2113012</th>
      <td>636635</td>
      <td>MzAwMDAxMTY1NQ==</td>
      <td>呵呵&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;n...</td>
      <td>nvrenshi1</td>
      <td>《喵美人》精选视频，每天带你装B，带你飞。欢淫关注小喵，小喵有好多爽爽的视频等着你哦！</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
    </tr>
    <tr>
      <th>1144765</th>
      <td>1502945</td>
      <td>MzA3Mjk0MjQyNg==</td>
      <td>中孟贸易&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;...</td>
      <td>bmcq18952631611</td>
      <td>所有孩子都有好奇心，“梦镜盒子”以3D的形式让好奇心有了想象的翅膀，去追求世界的奇妙；台湾G...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1437844278</td>
    </tr>
    <tr>
      <th>1483349</th>
      <td>487794</td>
      <td>MzA4NDg1OTAzNA==</td>
      <td>爱&amp;nbsp;sm8336&amp;nbsp;sm8337&amp;nbsp;sm8755&amp;nbsp;sm8...</td>
      <td>nvrenwzc</td>
      <td>小妹每天会在这里陪你爽，陪你嗨哦！&amp;nbsp;记得要关注小妹，小妹有好多爽爽的精彩等着你哦！</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
    </tr>
    <tr>
      <th>782503</th>
      <td>53897</td>
      <td>MzAxMjI2MDE1MQ==</td>
      <td>知味&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;n...</td>
      <td>z624151489</td>
      <td>为大家传递餐旅系最新信息。</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434740946</td>
    </tr>
    <tr>
      <th>2493125</th>
      <td>2157087</td>
      <td>MzA5ODAzMTcyOQ==</td>
      <td>纤纤美业&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;...</td>
      <td>xxy758258</td>
      <td>给顾客带来最新的潮流时尚&amp;nbsp;&amp;nbsp;&amp;nbsp;自信</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1450663718</td>
    </tr>
    <tr>
      <th>612301</th>
      <td>1742852</td>
      <td>MzA3NDA3MTMyNQ==</td>
      <td>TRBJU&amp;nbsp;-&amp;nbsp;TRUE&amp;nbsp;RELIGION&amp;nbsp;BRAN...</td>
      <td>truereligionchina</td>
      <td>TRUE&amp;nbsp;RELIGION由Jeffery&amp;nbsp;Lubell夫妇创立于200...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1440096196</td>
    </tr>
    <tr>
      <th>205221</th>
      <td>114532</td>
      <td>MzAwMzEyMjUwMg==</td>
      <td>搞笑女神simi2015&amp;nbsp;ysh5511&amp;nbsp;rrniubi&amp;nbsp;yi...</td>
      <td>VIPdud</td>
      <td>男人的终极禁地，女人的私密花园，欢淫关注哦！</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434752704</td>
    </tr>
    <tr>
      <th>1953116</th>
      <td>2366862</td>
      <td>MjM5MDY4Mzc2MA==</td>
      <td>sunny&amp;#39;s&amp;nbsp;zakka&amp;nbsp;hand&amp;nbsp;made&amp;nbs...</td>
      <td>zhufushougong</td>
      <td>主妇手工——”爱生活，爱手工“&amp;nbsp;sunny&amp;#39;s&amp;nbsp;zakka&amp;nb...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1459163058</td>
    </tr>
    <tr>
      <th>925960</th>
      <td>627725</td>
      <td>MzA5OTE4NDc4OQ==</td>
      <td>IWA&amp;nbsp;(International&amp;nbsp;Water&amp;nbsp;Associ...</td>
      <td>IWA_Network</td>
      <td>国际水协会（International&amp;nbsp;Water&amp;nbsp;Associatio...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
    </tr>
    <tr>
      <th>1000451</th>
      <td>925689</td>
      <td>MzA4NTIxNjgyNg==</td>
      <td>健康&amp;nbsp;&amp;nbsp;养生&amp;nbsp;休闲&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbs...</td>
      <td>NaN</td>
      <td>健康&amp;nbsp;&amp;nbsp;养生&amp;nbsp;&amp;nbsp;休闲&amp;nbsp;&amp;nbsp;医学&amp;n...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434940334</td>
    </tr>
    <tr>
      <th>768796</th>
      <td>2368832</td>
      <td>MzA3ODA5ODAwOQ==</td>
      <td>沈&amp;nbsp;阳&amp;nbsp;装&amp;nbsp;修&amp;nbsp;设&amp;nbsp;计&amp;nbsp;e&amp;nb...</td>
      <td>NaN</td>
      <td>以质量为生命，以信誉求发展，为您的装修提供最真诚、最正规、最贴心的服务，您可以拨打电话：13...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1459180656</td>
    </tr>
    <tr>
      <th>2431951</th>
      <td>1912391</td>
      <td>MzAxNTIyNTQ1NA==</td>
      <td>依然汗蒸瑜伽会馆&amp;amp;nbsp;&amp;amp;nbsp;&amp;amp;nbsp;&amp;amp;nbsp;</td>
      <td>yiranhuiguan888</td>
      <td>韩式托玛琳汗蒸能量房、瑜伽（哈他瑜伽、瘦身瑜伽、产后修复、理疗瑜伽、断食排毒等）</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1443079336</td>
    </tr>
    <tr>
      <th>1601893</th>
      <td>961970</td>
      <td>MjM5ODExMzA5NQ==</td>
      <td>台&amp;nbsp;湾&amp;nbsp;J&amp;nbsp;&amp;amp;&amp;nbsp;K&amp;nbsp;影&amp;nbsp;像</td>
      <td>JK-photo</td>
      <td>专注服务于商业摄影、服饰（箱包鞋）摄影等各相关领域。关注我们，可及时获得拍摄、客片、样片、产...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435046137</td>
    </tr>
    <tr>
      <th>1540428</th>
      <td>716110</td>
      <td>MzAxMzAxMTQwMA==</td>
      <td>内涵小师妹ap5050&amp;nbsp;v5aabb&amp;nbsp;ztv555&amp;nbsp;ppp776</td>
      <td>zuonvrenii</td>
      <td>我这么内涵。你能hold住吗？每天精彩不断！</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
    </tr>
    <tr>
      <th>1426634</th>
      <td>260934</td>
      <td>MjM5NDQzMDY2Nw==</td>
      <td>Fashion&amp;nbsp;Tv&amp;nbsp;Club&amp;nbsp;by&amp;nbsp;Babyface</td>
      <td>FTV_Babyface</td>
      <td>Bonjour！法国fashiontv大中华区首家官方俱乐部-fashiontv&amp;nbsp;...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
    </tr>
    <tr>
      <th>832182</th>
      <td>252613</td>
      <td>MjM5MzkxNjQzNg==</td>
      <td>Cultural&amp;nbsp;Care&amp;nbsp;Au&amp;nbsp;Pair&amp;nbsp;China</td>
      <td>ccapchina</td>
      <td>Cultural&amp;nbsp;Care&amp;nbsp;Au&amp;nbsp;Pair(CC协会)是世界上...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
    </tr>
    <tr>
      <th>1454599</th>
      <td>372794</td>
      <td>MzA3MzAwMDU2NQ==</td>
      <td>脸红那些事sm8337&amp;nbsp;sm8875&amp;nbsp;sm7588&amp;nbsp;auv788</td>
      <td>NTF899</td>
      <td>谁的青春不饥渴，每天分享大量真实但是又让你脸红的那些事！</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
    </tr>
    <tr>
      <th>838088</th>
      <td>276237</td>
      <td>MjM5NTU2NTMxMg==</td>
      <td>冬日暖阳&amp;nbsp;warm&amp;nbsp;&amp;amp;&amp;nbsp;love&amp;nbsp;notes</td>
      <td>Dongrnuanyang</td>
      <td>冬日暖阳发热地板，只装地板，就有地暖。即开即热，节能省电，轻松安装，智能温控，低碳环保。冬日...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
    </tr>
    <tr>
      <th>899018</th>
      <td>519957</td>
      <td>MzA4ODE1NjMxMw==</td>
      <td>吉开门业&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;开门大吉</td>
      <td>liyi19720901</td>
      <td>传播正能量，介绍卷帘门，工业门，伸缩门使用与保养及行业最新资讯</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
    </tr>
    <tr>
      <th>140465</th>
      <td>2551284</td>
      <td>MzAwNzc0NzIzMA==</td>
      <td>First&amp;nbsp;National&amp;nbsp;Waverley&amp;nbsp;City</td>
      <td>FNREWC</td>
      <td>墨尔本房产买卖、租赁</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1461814312</td>
    </tr>
    <tr>
      <th>1126629</th>
      <td>1430401</td>
      <td>MzAxMDE3NDM2MA==</td>
      <td>Thaihot&amp;nbsp;Hotels&amp;nbsp;&amp;amp;&amp;nbsp;Resorts</td>
      <td>NaN</td>
      <td>用于酒店信息发布及服务</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1437365909</td>
    </tr>
    <tr>
      <th>1440539</th>
      <td>316554</td>
      <td>MjM5ODI3ODA0NQ==</td>
      <td>MODERN&amp;nbsp;RAILWAYS铁路展及METRO&amp;nbsp;CHINA城轨展</td>
      <td>NaN</td>
      <td>关注ModernRailways铁路展及MetroChina城轨展，获取最新行业资讯及展会信...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
    </tr>
    <tr>
      <th>643454</th>
      <td>1867464</td>
      <td>MzA3NDAwNTI5Ng==</td>
      <td>Little&amp;nbsp;Scholar&amp;nbsp;Academy&amp;nbsp;德意学堂</td>
      <td>LittleScholarAcademy</td>
      <td>Little&amp;nbsp;Scholar&amp;nbsp;Academy&amp;nbsp;(LSA)&amp;nb...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1441648717</td>
    </tr>
    <tr>
      <th>1460753</th>
      <td>397410</td>
      <td>MzA3NTU2OTExMw==</td>
      <td>澳洲品牌达人AUSTRALIAN&amp;nbsp;INDULGENCE&amp;nbsp;CLUB</td>
      <td>NaN</td>
      <td>澳洲好东东的集散地</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
    </tr>
    <tr>
      <th>1695351</th>
      <td>1335802</td>
      <td>MzA5OTA2OTUzNw==</td>
      <td>享未来&amp;nbsp;&amp;nbsp;&amp;nbsp;素养人生&amp;nbsp;&amp;nbsp;健康是福</td>
      <td>yofogo-ssjg</td>
      <td>有享网，一站式综合性购物平台，为用户提供优质实惠的健康家庭生活化产品，为合作伙伴提供宽广的创...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436709921</td>
    </tr>
    <tr>
      <th>1310290</th>
      <td>2165045</td>
      <td>MzA5MTQwMDcxNw==</td>
      <td>N殿&amp;nbsp;发型设计&amp;nbsp;眉&amp;nbsp;唇&amp;nbsp;眼&amp;nbsp;纹艺</td>
      <td>ND15090763995</td>
      <td>N殿眉眼唇工作室</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1450991039</td>
    </tr>
    <tr>
      <th>253485</th>
      <td>307588</td>
      <td>MjM5NzY2OTgzMw==</td>
      <td>史考姆&amp;nbsp;&amp;nbsp;SCOM&amp;nbsp;-&amp;nbsp;供应链与运营管理人</td>
      <td>China-SCOM</td>
      <td>中国供应链与运营管理人俱乐部（SCOM）服务于制造业和零售业的供应链、采购等相关经理人的非盈...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
    </tr>
    <tr>
      <th>818604</th>
      <td>198301</td>
      <td>MjM5MDM0NzY5NA==</td>
      <td>CLA&amp;nbsp;&amp;nbsp;&amp;nbsp;爱健康&amp;nbsp;&amp;nbsp;享品质生活</td>
      <td>ah88702883</td>
      <td>为大家打造一个分享健康的交流平台</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
    </tr>
    <tr>
      <th>1071659</th>
      <td>1210521</td>
      <td>MzAxODAzODExNg==</td>
      <td>一汽丰田&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;武威赛弛丰田</td>
      <td>wwsc-ft</td>
      <td>本店销售一汽丰田所有车型：RAV4、卡罗拉、威驰、花冠、普拉多、锐志、皇冠、兰德酷路泽、柯斯达。</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436148643</td>
    </tr>
    <tr>
      <th>259781</th>
      <td>332772</td>
      <td>MjM5OTM2MjY2MA==</td>
      <td>Enjoy&amp;nbsp;the&amp;nbsp;Best&amp;nbsp;of&amp;nbsp;上海</td>
      <td>EnjoyTheBest</td>
      <td>我们的理念：帮助人们更好地享受上海的生活！现在加入您能得到：1）优惠券：&amp;nbsp;超过5万...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>1150330</th>
      <td>1525205</td>
      <td>MzA4NDE2ODcyMw==</td>
      <td>&amp;quot;幸善精&amp;quot;专属商旅定制</td>
      <td>xsj678678</td>
      <td>旅游.咨询.考察.会务.签证.欢迎热线:023-88323223</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1438019234</td>
    </tr>
    <tr>
      <th>1154837</th>
      <td>1543233</td>
      <td>MzA3NjI2MTgzNQ==</td>
      <td>Mandarin&amp;nbsp;teacher</td>
      <td>NaN</td>
      <td>汉语培训，对外汉语教师professional&amp;nbsp;mandarin&amp;nbsp;tea...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1438147347</td>
    </tr>
    <tr>
      <th>1156498</th>
      <td>1549877</td>
      <td>MzA5NjA5OTIyOQ==</td>
      <td>menopausal&amp;nbsp;women</td>
      <td>sdyy211</td>
      <td>信息传递。</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1438198352</td>
    </tr>
    <tr>
      <th>1157205</th>
      <td>1552705</td>
      <td>MzA3ODMwODAzMw==</td>
      <td>爱安玖InaturalAngel儿童礼服裙</td>
      <td>NaN</td>
      <td>每个女孩都是天使！这里是一个关于女孩教育，穿衣搭配，潮流资讯，高端私人定制，美食亲子旅游分享...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1438227272</td>
    </tr>
    <tr>
      <th>1164812</th>
      <td>1583133</td>
      <td>MzI1MDAwNzY2MQ==</td>
      <td>JasonHuangPhotography</td>
      <td>HZXphoto</td>
      <td>与大家分享本人的摄影作品，并介绍一些摄影小常识和技巧</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1438473150</td>
    </tr>
    <tr>
      <th>1169760</th>
      <td>1602925</td>
      <td>MzIwNDAzNDc2Nw==</td>
      <td>limeifenggongzhonghao</td>
      <td>NaN</td>
      <td>做美丽的女人，从洗脸开始！</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1438662889</td>
    </tr>
    <tr>
      <th>1179345</th>
      <td>1641265</td>
      <td>MzI0OTAzODQ4OQ==</td>
      <td>上海财富双语Toastmasters俱乐部</td>
      <td>SHWMB_TMC</td>
      <td>财富双语TMC，致力于塑造会员的领导力与沟通能力，并着力提升每一名会员的财商，传播科学的财富...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1439058264</td>
    </tr>
    <tr>
      <th>1105052</th>
      <td>1344093</td>
      <td>MzA4ODM3NDQzOQ==</td>
      <td>r100叮当猫童装珠海专卖店2636961</td>
      <td>ddc-r100_zh</td>
      <td>时尚　健康　产品展示　客户交流　儿童　少年天地</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436777252</td>
    </tr>
    <tr>
      <th>1092716</th>
      <td>1294749</td>
      <td>MzAwODA0NDg2OA==</td>
      <td>minxinyuanchaoshi2011</td>
      <td>minxinchaoshi2011</td>
      <td>新鲜你我生活，实现共同梦想。</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436506899</td>
    </tr>
    <tr>
      <th>1033717</th>
      <td>1058753</td>
      <td>MzA4NzI4NDcyOA==</td>
      <td>www.66.ca&amp;nbsp;多伦多六六网</td>
      <td>ca66ca</td>
      <td>www.66.ca&amp;nbsp;多伦多六六网</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435395241</td>
    </tr>
    <tr>
      <th>1062386</th>
      <td>1173429</td>
      <td>MzA4NDAyMjI2OQ==</td>
      <td>timeoutshanghaifamily</td>
      <td>NaN</td>
      <td>Showcasing&amp;nbsp;the&amp;nbsp;best&amp;nbsp;venues,&amp;nbs...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435933997</td>
    </tr>
    <tr>
      <th>1035313</th>
      <td>1065137</td>
      <td>MjM5MjAyMjc5Nw==</td>
      <td>完美&amp;nbsp;&amp;nbsp;赢康养生服务部</td>
      <td>NaN</td>
      <td>赢康养生服务部，以专业经络培训，社会养生咨询，养生产品研发，品牌连锁加盟及店铺经营管理等为己...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435417390</td>
    </tr>
    <tr>
      <th>1043036</th>
      <td>1096029</td>
      <td>MzA5ODM4ODUwNg==</td>
      <td>歙砚故乡&amp;nbsp;&amp;nbsp;寒山艺术馆</td>
      <td>NaN</td>
      <td>歙砚故乡&amp;nbsp;&amp;nbsp;&amp;nbsp;龙尾典藏</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435538587</td>
    </tr>
    <tr>
      <th>1049351</th>
      <td>1121289</td>
      <td>MzA4MTA4ODExNA==</td>
      <td>Lara家&amp;nbsp;&amp;nbsp;右手拉拉</td>
      <td>larastyle</td>
      <td>发布最新资讯、新品公告、活动预告&lt;br/&gt;&lt;br/&gt;Lara家&amp;nbsp;&amp;nbsp;右手拉...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435663282</td>
    </tr>
    <tr>
      <th>1049780</th>
      <td>1123005</td>
      <td>MzA4NDMwNjI2MA==</td>
      <td>Toastmasters89大区福建M中区</td>
      <td>DivisionM</td>
      <td>服务会员和俱乐部运作、推广89大区M中区俱乐部、活动预告、扩大Toastmesters品牌效应</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435675960</td>
    </tr>
    <tr>
      <th>1053026</th>
      <td>1135989</td>
      <td>MzIzMjAwOTAyNw==</td>
      <td>亚太位置服务与智慧城市产业技术创新战略联盟</td>
      <td>NaN</td>
      <td>该联盟是云南省科技厅组织由云南世纪位置服务平台有限公司作为牵头发起单位之一，联合老挝、缅甸、...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435737407</td>
    </tr>
    <tr>
      <th>1054983</th>
      <td>1143817</td>
      <td>MjM5NDAzNzczNQ==</td>
      <td>水晶之恋婚纱摄影-摄影师&amp;nbsp;YAN</td>
      <td>yanrongshusheng</td>
      <td>您美丽婚姻的开始，个性、时尚、唯美婚纱，幸福从这里起航.让我们的团队一起来见证您最、幸福最美...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435777388</td>
    </tr>
    <tr>
      <th>1059136</th>
      <td>1160429</td>
      <td>MzA4MzM2NzcxMA==</td>
      <td>HAWK&amp;nbsp;Performance</td>
      <td>HawkPerformance</td>
      <td>Hawk&amp;nbsp;Performance&amp;nbsp;是高性能街车和专业赛车高性能刹车片的供应商。</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435861809</td>
    </tr>
    <tr>
      <th>1061802</th>
      <td>1171093</td>
      <td>MzAwODE4ODczOQ==</td>
      <td>BelleBelleBeautySalon</td>
      <td>NaN</td>
      <td>让更多的爱美人士通过我们发布的内容可以了解更多美容方面的信息。</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435916968</td>
    </tr>
    <tr>
      <th>1062588</th>
      <td>1174237</td>
      <td>MzAxNDE1NDE0OQ==</td>
      <td>ColorMePottery创意陶瓷彩绘馆</td>
      <td>ColorMePottery</td>
      <td>秉承“人人皆可艺术”的理念，引领全新的休闲风潮。超过300种素坯供您选择，无论有无画功，均可...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435941382</td>
    </tr>
    <tr>
      <th>1084806</th>
      <td>1263109</td>
      <td>MzAwMDE1NTY1NQ==</td>
      <td>myoyo麦幼优海绵宝宝主题儿童乐园烟台园</td>
      <td>ytmyoyo</td>
      <td>myoyo海绵宝宝主题儿童乐园，这里有海绵宝宝、朵拉等知名的卡通人物，有充满冒险精神的游乐设...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436359476</td>
    </tr>
    <tr>
      <th>1067680</th>
      <td>1194605</td>
      <td>MzA4NzczMjYyNA==</td>
      <td>dreamtripslife梦幻之旅俱乐部</td>
      <td>KXF0012</td>
      <td>行者无疆&amp;nbsp;放眼世界</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436057109</td>
    </tr>
    <tr>
      <th>1072439</th>
      <td>1213641</td>
      <td>MzA4NzMxODcwNg==</td>
      <td>晶瑜荟萃Crystal&amp;nbsp;Jade</td>
      <td>NaN</td>
      <td>健康的生活、积极的态度、乐观的心态、甜蜜的幸福，这里只有正能量。</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436164307</td>
    </tr>
    <tr>
      <th>1074922</th>
      <td>1223573</td>
      <td>MzA3MDU1MzYwNA==</td>
      <td>代写essay论文代写assignment</td>
      <td>daixieessaylunwendai</td>
      <td>英国论文代写,,欢迎关注交流。</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436208458</td>
    </tr>
    <tr>
      <th>1075347</th>
      <td>1225273</td>
      <td>MzI2MDAwMzI4Mw==</td>
      <td>shaklee美国christy的快乐人生</td>
      <td>NaN</td>
      <td>shaklee美国christy的快乐人生</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436215497</td>
    </tr>
    <tr>
      <th>1077344</th>
      <td>1233261</td>
      <td>MzA3OTAzMzMzNA==</td>
      <td>Automechanika&amp;nbsp;SH</td>
      <td>Automechanika_SH</td>
      <td>上海国际汽车零配件、维修检测诊断设备及服务用品展览会（Automechanika&amp;nbsp;...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436249865</td>
    </tr>
    <tr>
      <th>1080017</th>
      <td>1243953</td>
      <td>MzAxMDI3OTA3Mw==</td>
      <td>IFilmProductionStudio</td>
      <td>I_FilmStudio</td>
      <td>I&amp;nbsp;Film&amp;nbsp;Production&amp;nbsp;Studio专注于为华人顶...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436289034</td>
    </tr>
    <tr>
      <th>1082018</th>
      <td>1251957</td>
      <td>MzAwMTAyMzI4OA==</td>
      <td>WORLDVENTURES世界环球旅游集团</td>
      <td>NaN</td>
      <td>要么旅行、要么读书、身体和灵魂，必须有一个在路上</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436319345</td>
    </tr>
    <tr>
      <th>1082580</th>
      <td>1254205</td>
      <td>MjM5ODAwMTU2OQ==</td>
      <td>where&amp;nbsp;to&amp;nbsp;go</td>
      <td>enjoylife78</td>
      <td>一起去旅游，享受生命中的惊喜</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436329494</td>
    </tr>
    <tr>
      <th>945</th>
      <td>2398440</td>
      <td>MzAwNTMyMjY1MQ==</td>
      <td>BreakTheCodeAustralia</td>
      <td>breakthecode_aus</td>
      <td>BreakTheCodeAustralia官方账号</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1459760311</td>
    </tr>
  </tbody>
</table>
<p>3712 rows × 7 columns</p>
</div>



Looks like those long names have a lot of html-escaped "hard spaces" (`&nbsp;`) in them. There are also some other HTML encoded characters like `&amp;`. Some rows have even more broken encodings like `&amp;nbsp` which probably result from "double" html excaping. We should probably try to clean it.


```python
(biz_long_names.biz_name.str.count('&nbsp;') >= 1).sum()
```




    1607



Looks like ~ 50% of the records have at least a single 'nbsp'

Let's clean it


```python
import html

def double_unescape_html_text(text):
    if not pd.isnull(text):
        return html.unescape(html.unescape(text)).replace('\xa0', ' ')
    else:
        return text
```


```python
biz['clean_biz_name'] = biz.biz_name.map(double_unescape_html_text)
```


```python
clean_biz_name_len = biz.clean_biz_name.str.len()
clean_biz_name_len.describe(percentiles=[0.5, 0.75, 0.9, 0.95, 0.999]).astype(int)
```




    count    2542556
    mean           6
    std            2
    min            1
    50%            6
    75%            8
    90%           10
    95%           12
    99.9%         20
    max           47
    Name: clean_biz_name, dtype: int64



Looks more reasonable now


```python
biz.loc[clean_biz_name_len[clean_biz_name_len > 20].sort_values(ascending=False).index].head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>biz_id</th>
      <th>biz_name</th>
      <th>biz_code</th>
      <th>biz_description</th>
      <th>qr_code</th>
      <th>timestamp</th>
      <th>clean_biz_name</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1483349</th>
      <td>487794</td>
      <td>MzA4NDg1OTAzNA==</td>
      <td>爱&amp;nbsp;sm8336&amp;nbsp;sm8337&amp;nbsp;sm8755&amp;nbsp;sm8...</td>
      <td>nvrenwzc</td>
      <td>小妹每天会在这里陪你爽，陪你嗨哦！&amp;nbsp;记得要关注小妹，小妹有好多爽爽的精彩等着你哦！</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>爱 sm8336 sm8337 sm8755 sm8875 vip88375 hehe5966</td>
    </tr>
    <tr>
      <th>998422</th>
      <td>917573</td>
      <td>MzAxMDEyMjQyMg==</td>
      <td>脸红auv8899那iilian6些aaashy8事xiaoshimei992</td>
      <td>vip8784</td>
      <td>每天晚上更新，等你来哦</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434920490</td>
      <td>脸红auv8899那iilian6些aaashy8事xiaoshimei992</td>
    </tr>
    <tr>
      <th>785035</th>
      <td>64025</td>
      <td>MjM5NDY0NTk2MA==</td>
      <td>夜遇nvr369美zvip521丽xvip555和zvipxo和nrnr36</td>
      <td>tianshengmei</td>
      <td>都市是繁华的，惟其繁华，所以喧嚣，人在其中，最易迷失自己，我是夜遇女人！火速点击下面关注哦！</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434742403</td>
      <td>夜遇nvr369美zvip521丽xvip555和zvipxo和nrnr36</td>
    </tr>
    <tr>
      <th>205221</th>
      <td>114532</td>
      <td>MzAwMzEyMjUwMg==</td>
      <td>搞笑女神simi2015&amp;nbsp;ysh5511&amp;nbsp;rrniubi&amp;nbsp;yi...</td>
      <td>VIPdud</td>
      <td>男人的终极禁地，女人的私密花园，欢淫关注哦！</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434752704</td>
      <td>搞笑女神simi2015 ysh5511 rrniubi yishen233</td>
    </tr>
    <tr>
      <th>908953</th>
      <td>559697</td>
      <td>MzA5MjExMTY5NA==</td>
      <td>坏丫头vipwysmh小nvren238丫simi365头simi8090</td>
      <td>NTF000</td>
      <td>我是坏丫头，有胆你就来！</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>坏丫头vipwysmh小nvren238丫simi365头simi8090</td>
    </tr>
  </tbody>
</table>
</div>



## Checking `biz_code`

I'm not going to check biz_code in details, because it's not required in the output schema.


```python
biz.biz_code.sample(n=10)
```




    310559     LianshengMarket
    1054186          TD-PHOTOS
    194580                 NaN
    71886                  NaN
    2013362            xhlyszx
    61402                  NaN
    865419           SunBasket
    1010622    meiguozhilv1980
    455125             hdsptyd
    135171         qy993562871
    Name: biz_code, dtype: object




```python
missing_biz_code = biz[biz.biz_code.isnull() | (biz.biz_code.str.len() == 0)]
missing_biz_code.shape
```




    (568171, 8)



## Checking `biz_description`


```python
biz.biz_description.sample(n=10)
```




    2230154                                                中央电视台
    1611438                       vmsky.com是国内最早、全球最大的中文虚拟化技术社区。
    1843178    关注莞工文学院志愿服务站，了解最新志愿动态。蒲公英，让爱随风传递！趣味涂鸦、福利院学习班辅导...
    190172                                热帖头条，八卦资讯，奇闻异事，网罗天下新鲜事
    676708     女人的美丽不仅仅存在于外表，而应该源自内在的品质——&nbsp;充满自信、充满激情、从容的平...
    992831                                                   NaN
    1045219    致力于终身教育（学历教育、会计培训、职业技能培训、高校毕业生创业和职业综合能力培训），是您培...
    222930             冠世网是提供枣庄本地优质生活服务信息的社交平台。分享点滴，品质生活，欢迎加入冠世网
    1988671                                            牵手心搜，商机无限
    442902                      发布酒店的最新动态（包括酒店新闻、创新菜肴推出、促销优惠活动等）
    Name: biz_description, dtype: object




```python
missing_biz_description = biz[biz.biz_description.isnull() | (biz.biz_description.str.len() == 0)]
missing_biz_description.shape
```




    (18803, 8)




```python
biz_description_len = biz.biz_description.str.len()
biz_description_len.describe(percentiles=[0.5, 0.75, 0.9, 0.95, 0.999]).astype(int)
```




    count    2541002
    mean          51
    std           87
    min            1
    50%           40
    75%           77
    90%          112
    95%          120
    99.9%        263
    max        65527
    Name: biz_description, dtype: int64



Looks like in description there are also some outliers


```python
biz_long_descriptions = biz.loc[biz_description_len[biz_description_len > 200].sort_values(ascending=False).index]
print(biz_long_descriptions.shape)
biz_long_descriptions
```

    (7346, 8)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>biz_id</th>
      <th>biz_name</th>
      <th>biz_code</th>
      <th>biz_description</th>
      <th>qr_code</th>
      <th>timestamp</th>
      <th>clean_biz_name</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>394570</th>
      <td>871928</td>
      <td>MzAxMjM2MTczNQ==</td>
      <td>MaShaolong的</td>
      <td>mashaolong-716</td>
      <td>无副作用&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434788529</td>
      <td>MaShaolong的</td>
    </tr>
    <tr>
      <th>1791596</th>
      <td>1720782</td>
      <td>MzA5NzYzNTQ4Mw==</td>
      <td>逗知</td>
      <td>NaN</td>
      <td>令人脑洞大开的创意集锦&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1439888962</td>
      <td>逗知</td>
    </tr>
    <tr>
      <th>320402</th>
      <td>575256</td>
      <td>MzA5Mzc5MTI2Mg==</td>
      <td>璐成教育</td>
      <td>lcjy-365</td>
      <td>&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbs...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>璐成教育</td>
    </tr>
    <tr>
      <th>1450601</th>
      <td>356802</td>
      <td>MzA3MTQ1MzI3OA==</td>
      <td>成就你</td>
      <td>CJNF2014</td>
      <td>培训、教育事业&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nb...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>成就你</td>
    </tr>
    <tr>
      <th>1536382</th>
      <td>699926</td>
      <td>MzAxMDIxMzUzMw==</td>
      <td>美若似水年华</td>
      <td>mei18age</td>
      <td>我们花容月貌，我们倾国倾城。但若不美心美志，独立自强，则只配拥有“落日的皮囊”！我爱“永远的...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>美若似水年华</td>
    </tr>
    <tr>
      <th>2369088</th>
      <td>1660939</td>
      <td>MzA4MzE4NDY1MA==</td>
      <td>卓越领尚Gone简一制作</td>
      <td>NaN</td>
      <td>卓越领尚Gone简一制作官方微信，店铺地址：http://eonesimplelife.ta...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1439309273</td>
      <td>卓越领尚Gone简一制作</td>
    </tr>
    <tr>
      <th>646362</th>
      <td>1879096</td>
      <td>MzI4MjA0OTA3MQ==</td>
      <td>点滴阅读</td>
      <td>diandidushu</td>
      <td>开阔文学视野，触动写作灵感，陶冶思想情操，提升人生品味。&amp;nbsp;&amp;nbsp;&amp;nbsp;...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1441897486</td>
      <td>点滴阅读</td>
    </tr>
    <tr>
      <th>1827877</th>
      <td>1865906</td>
      <td>MzA3MjI5Mzk1MQ==</td>
      <td>一起学美搭</td>
      <td>wlykelly1984</td>
      <td>工厂一手货女装，诚招代理加盟，支持一件代发，欢迎选购。&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1441627621</td>
      <td>一起学美搭</td>
    </tr>
    <tr>
      <th>193647</th>
      <td>68236</td>
      <td>MzAwMTU4MjgxNQ==</td>
      <td>天狮事业网上运作</td>
      <td>TSSY666</td>
      <td>欢迎关注，人以梦想而伟大！思维决定行动，行动产生结果。在我们生命中，被什么样的语言打动，就会...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434743038</td>
      <td>天狮事业网上运作</td>
    </tr>
    <tr>
      <th>1532608</th>
      <td>684830</td>
      <td>MzAwNzUzMzIxMg==</td>
      <td>汤圆</td>
      <td>NaN</td>
      <td>&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbs...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>汤圆</td>
    </tr>
    <tr>
      <th>1747284</th>
      <td>1543534</td>
      <td>MzA3NTQ3NjM3Mw==</td>
      <td>南阳欧普橱柜</td>
      <td>oupuchugui</td>
      <td>南阳欧普橱柜公众信息交流平台&lt;br/&gt;您可以在此了解全面的装修、橱柜、厨房电器等方面的专业知...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1438149185</td>
      <td>南阳欧普橱柜</td>
    </tr>
    <tr>
      <th>1066567</th>
      <td>1190153</td>
      <td>MzI3MTAwMjY5NA==</td>
      <td>周易算卦</td>
      <td>suyunfeng8899</td>
      <td>&amp;nbsp;&amp;nbsp;&amp;nbsp;中国最优秀的周易预测平台，四柱推命，六爻占卜，起名策划&amp;...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436038731</td>
      <td>周易算卦</td>
    </tr>
    <tr>
      <th>686272</th>
      <td>2038736</td>
      <td>MzI2NTAyMTA3NQ==</td>
      <td>芊芊</td>
      <td>qianqian2015612</td>
      <td>一朵花，一棵树，一块石头&amp;nbsp;&amp;nbsp;一双发现的眼睛，用一颗柔软的心读懂生活的一字...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1446427584</td>
      <td>芊芊</td>
    </tr>
    <tr>
      <th>2254769</th>
      <td>1203663</td>
      <td>MzA3MzA1NTY0NA==</td>
      <td>群众群特通告</td>
      <td>ENA1st</td>
      <td>群众群特的通告平台！&lt;br/&gt;片酬在150元/天以内！&lt;br/&gt;&lt;br/&gt;&lt;br/&gt;&lt;br/...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436114524</td>
      <td>群众群特通告</td>
    </tr>
    <tr>
      <th>2080773</th>
      <td>507679</td>
      <td>MzA4NjgxNjk4OA==</td>
      <td>玛丽风尚</td>
      <td>NaN</td>
      <td>最走心的女人微生活平台。&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbs...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>玛丽风尚</td>
    </tr>
    <tr>
      <th>670045</th>
      <td>1973828</td>
      <td>MzIwNjA0MDExOQ==</td>
      <td>龙都生活好帮手</td>
      <td>ldshhbs</td>
      <td>生活好帮手，衣食住行游购娱，柴米油盐酱醋茶！&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1445137248</td>
      <td>龙都生活好帮手</td>
    </tr>
    <tr>
      <th>1464243</th>
      <td>411370</td>
      <td>MzA3NzA0MjI3Nw==</td>
      <td>重庆科技学院学生会</td>
      <td>cqust-xsh</td>
      <td>为了加强组织建设，促进与学生的交流与联系，让全校学生都能了解学校发生的各项活动，增进友谊。&amp;...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>重庆科技学院学生会</td>
    </tr>
    <tr>
      <th>936775</th>
      <td>670985</td>
      <td>MzAwNTQ3MDAzMw==</td>
      <td>GSGforex外汇平台</td>
      <td>GSGforex</td>
      <td>GSGforex致力于为澳洲和亚洲个人以及机构客户提供良好的外汇、黄金、差价合约交易环境。&amp;...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>GSGforex外汇平台</td>
    </tr>
    <tr>
      <th>1782305</th>
      <td>1683618</td>
      <td>MzA4MTEzOTU1OQ==</td>
      <td>汉诺威自酿啤酒屋</td>
      <td>NaN</td>
      <td>喝德国鲜酿啤酒，品时尚健康生活!我们严格按照德国啤酒坊的传统酿造工艺，只用麦芽、啤酒花、酵母...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1439577519</td>
      <td>汉诺威自酿啤酒屋</td>
    </tr>
    <tr>
      <th>1782420</th>
      <td>1684078</td>
      <td>MzI4MDAyMzkxNg==</td>
      <td>鲅鱼圈共青团</td>
      <td>byq_gqt</td>
      <td>共青团营口市经济技术开发区委员会，营口市鲅鱼圈共青团官方微信。&amp;nbsp;&amp;nbsp;&amp;nb...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1439581286</td>
      <td>鲅鱼圈共青团</td>
    </tr>
    <tr>
      <th>1755515</th>
      <td>1576458</td>
      <td>MzIwNTAzMjE5Mw==</td>
      <td>舒伟健康园</td>
      <td>shuwei8848</td>
      <td>&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbs...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1438420036</td>
      <td>舒伟健康园</td>
    </tr>
    <tr>
      <th>770584</th>
      <td>2375984</td>
      <td>MzI1MDA0MTk4Ng==</td>
      <td>北京非凡生活</td>
      <td>ffshpt2016</td>
      <td>运用互联网思维，让好产品到消费者的路径最短，打破传统的流通方式，创建以互联网技术为基础的新的...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1459303885</td>
      <td>北京非凡生活</td>
    </tr>
    <tr>
      <th>1765212</th>
      <td>1615246</td>
      <td>MzIyMTAzMTY5Ng==</td>
      <td>歪not择业</td>
      <td>whynotzeye</td>
      <td>兼职信息发布&lt;br/&gt;职业困惑交流&lt;br/&gt;职业选择规划&lt;br/&gt;就业指导培训&lt;br/&gt;创业...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1438774330</td>
      <td>歪not择业</td>
    </tr>
    <tr>
      <th>1056614</th>
      <td>1150341</td>
      <td>MzAwMjY0MzI4Nw==</td>
      <td>莫明堂</td>
      <td>mmingtang</td>
      <td>古琴文化&amp;nbsp;&amp;nbsp;国风传统&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;n...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435806734</td>
      <td>莫明堂</td>
    </tr>
    <tr>
      <th>1071349</th>
      <td>1209281</td>
      <td>MzA4NzAzMTM0NA==</td>
      <td>莆田报关行</td>
      <td>NaN</td>
      <td>专业报关、报检、国际货运、海运、集装箱、海外拓展、海外企业运营、注册各类证件、代理进出口货物...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436142987</td>
      <td>莆田报关行</td>
    </tr>
    <tr>
      <th>2478148</th>
      <td>2097179</td>
      <td>MzA4MDE2NzI0NQ==</td>
      <td>瑜伽慢生活</td>
      <td>YogaFL888666</td>
      <td>瑜伽是一种生活方式，自由呼吸，自然自在，生活是一场体验的过程，生命是一场旅行。&lt;br/&gt;分享...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1448350410</td>
      <td>瑜伽慢生活</td>
    </tr>
    <tr>
      <th>2120219</th>
      <td>665463</td>
      <td>MzAwNDQxNzI4Nw==</td>
      <td>紅雨庭流行坊</td>
      <td>hytlxf520</td>
      <td>&lt;br/&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>紅雨庭流行坊</td>
    </tr>
    <tr>
      <th>2046950</th>
      <td>372387</td>
      <td>MzA3MzA2Nzg5Nw==</td>
      <td>皇室工匠3D艺术软包</td>
      <td>HSGJRB</td>
      <td>软包第一品牌，专业生产3D艺术软包背景墙、电视背景墙、床头背景墙、沙发背景墙。全国连锁招商加...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>皇室工匠3D艺术软包</td>
    </tr>
    <tr>
      <th>690728</th>
      <td>2056560</td>
      <td>MzA3OTI3NDg1NQ==</td>
      <td>仙女花园</td>
      <td>NaN</td>
      <td>小仙女的秘密花园，&amp;nbsp;星座、美容、彩妆、护肤，一切能让你变美变好的，都是我们所追求的...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1446738151</td>
      <td>仙女花园</td>
    </tr>
    <tr>
      <th>2444851</th>
      <td>1963991</td>
      <td>MzA4NDg0Njk4OQ==</td>
      <td>ysmart</td>
      <td>NaN</td>
      <td>纯美式顶级私人影院&amp;nbsp;，为你打造属于你的智慧豪宅！&amp;nbsp;&amp;nbsp;&amp;nbsp...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1444903302</td>
      <td>ysmart</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>780067</th>
      <td>44153</td>
      <td>MzAwODM4MjAxOA==</td>
      <td>NewsTurbo</td>
      <td>NewsTurbo</td>
      <td>One-stop&amp;nbsp;English&amp;nbsp;platform&amp;nbsp;for&amp;n...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434739587</td>
      <td>NewsTurbo</td>
    </tr>
    <tr>
      <th>2203352</th>
      <td>997995</td>
      <td>MzIwMTIzODg4MA==</td>
      <td>梁巧</td>
      <td>NaN</td>
      <td>你散发独有的香味，闻的心都醉！每个女孩都是落如凡间的天使，美丽是我们与生俱来的！&amp;nbsp;...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435172358</td>
      <td>梁巧</td>
    </tr>
    <tr>
      <th>2384270</th>
      <td>1721667</td>
      <td>MzAxODMxMjc5Mg==</td>
      <td>南阳尾货服装</td>
      <td>whfz99</td>
      <td>南阳同城网购，送货上门，满意后再付款。主营&amp;nbsp;海澜之家&amp;nbsp;利郎&amp;nbsp;七...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1439897299</td>
      <td>南阳尾货服装</td>
    </tr>
    <tr>
      <th>1466456</th>
      <td>420222</td>
      <td>MzA3NzgzMTgxOA==</td>
      <td>音乐房子livehouse</td>
      <td>NaN</td>
      <td>音乐房子livehouse小酒吧&amp;nbsp;&amp;nbsp;&amp;nbsp;是太原大学城首家live...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>音乐房子livehouse</td>
    </tr>
    <tr>
      <th>2220069</th>
      <td>1064863</td>
      <td>MzA5MTYxODIyNQ==</td>
      <td>关爱肾脏同舟共济</td>
      <td>KidneyCare_TongJi</td>
      <td>关爱肾脏健康，为您提供肾脏疾病相关知识、解答疑惑，与您一起同舟共济，重拾生活信心，重塑身体健...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435416091</td>
      <td>关爱肾脏同舟共济</td>
    </tr>
    <tr>
      <th>774303</th>
      <td>2390860</td>
      <td>MzI3MzIwMzUwNw==</td>
      <td>正品NB运动鞋AJ篮球鞋外贸批发</td>
      <td>NaN</td>
      <td>正品耐克&amp;nbsp;新百伦&amp;nbsp;阿迪达斯&amp;nbsp;乔丹&amp;nbsp;彪马&amp;nbsp;亚...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1459600321</td>
      <td>正品NB运动鞋AJ篮球鞋外贸批发</td>
    </tr>
    <tr>
      <th>613199</th>
      <td>1746444</td>
      <td>MzIzNTA0NDE4MQ==</td>
      <td>弥陀之爱</td>
      <td>fyin18</td>
      <td>南無阿彌陀佛！净土宗宗旨&lt;br/&gt;信受彌陀救度，专称彌陀佛名。&lt;br/&gt;愿生彌陀净土，广度十...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1440135069</td>
      <td>弥陀之爱</td>
    </tr>
    <tr>
      <th>357184</th>
      <td>722384</td>
      <td>MzAxNDAxMjI1NQ==</td>
      <td>银河之星幼儿园</td>
      <td>yhzxyey</td>
      <td>&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;青岛城阳...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>银河之星幼儿园</td>
    </tr>
    <tr>
      <th>1497112</th>
      <td>542846</td>
      <td>MzA5MDQ2NDU5OA==</td>
      <td>奥工装饰</td>
      <td>ag2930816898</td>
      <td>&amp;nbsp;&amp;nbsp;室内装饰&amp;nbsp;&amp;nbsp;设计&amp;nbsp;&amp;nbsp;施工服务...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>奥工装饰</td>
    </tr>
    <tr>
      <th>1106070</th>
      <td>1348165</td>
      <td>MzIwNTAxMjM2MQ==</td>
      <td>悦香台湾美食原料</td>
      <td>NaN</td>
      <td>主营：台湾炸鸡原料&amp;nbsp;台湾正宗鸡排粗（细）粉&amp;nbsp;&amp;nbsp;卡啦粉&amp;nbsp...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436805934</td>
      <td>悦香台湾美食原料</td>
    </tr>
    <tr>
      <th>48037</th>
      <td>2457742</td>
      <td>MzI0NjI3MzQ1Mw==</td>
      <td>青县便民圈</td>
      <td>qingxianquan798</td>
      <td>青县便民圈&amp;nbsp;&amp;nbsp;可以在里面&amp;nbsp;&amp;nbsp;&amp;nbsp;征婚&amp;nbs...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1461077546</td>
      <td>青县便民圈</td>
    </tr>
    <tr>
      <th>1905190</th>
      <td>2175158</td>
      <td>MzIwMTUxMDcwNg==</td>
      <td>陆河推广传媒</td>
      <td>NaN</td>
      <td>陆河一条龙推广服务&amp;nbsp;&amp;nbsp;&amp;nbsp;微商推广&amp;nbsp;&amp;nbsp;&amp;nb...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1451383371</td>
      <td>陆河推广传媒</td>
    </tr>
    <tr>
      <th>931208</th>
      <td>648717</td>
      <td>MzAwMjAyMDcxMQ==</td>
      <td>娇颜平价洗化超市</td>
      <td>JYPJXH13839802776</td>
      <td>娇颜洗化风雨十年！&amp;nbsp;本店主营：名牌护肤/精品洗化/家用日常等&amp;nbsp;护肤品牌：...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>娇颜平价洗化超市</td>
    </tr>
    <tr>
      <th>427911</th>
      <td>1005292</td>
      <td>MzA5MTMwNTMwMA==</td>
      <td>八一彩印</td>
      <td>QQ815063118</td>
      <td>特价名片&amp;nbsp;专业承接：劳务&amp;nbsp;家政&amp;nbsp;快递&amp;nbsp;房介&amp;nbsp...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435194470</td>
      <td>八一彩印</td>
    </tr>
    <tr>
      <th>1114646</th>
      <td>1382469</td>
      <td>MzA5NTkwNDIyMQ==</td>
      <td>青岛良木家具寿光旗舰店</td>
      <td>qingdaoliangmu</td>
      <td>&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbs...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1437065814</td>
      <td>青岛良木家具寿光旗舰店</td>
    </tr>
    <tr>
      <th>422497</th>
      <td>983636</td>
      <td>MjM5MjU3ODczOA==</td>
      <td>SICC</td>
      <td>SICCOFFICE</td>
      <td>SICC--an&amp;nbsp;Ideal&amp;nbsp;Place&amp;nbsp;for&amp;nbsp;L...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435128551</td>
      <td>SICC</td>
    </tr>
    <tr>
      <th>576073</th>
      <td>1597940</td>
      <td>MzA5MzU4MDQ2Ng==</td>
      <td>贞子漫画</td>
      <td>NaN</td>
      <td>贞子的脑子里有个黑洞～&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1438608658</td>
      <td>贞子漫画</td>
    </tr>
    <tr>
      <th>419150</th>
      <td>970248</td>
      <td>MzAxNDM3NjUzMg==</td>
      <td>桃花苑土鸡</td>
      <td>thy616888</td>
      <td>天然&amp;nbsp;&amp;nbsp;&amp;nbsp;绿色&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435074979</td>
      <td>桃花苑土鸡</td>
    </tr>
    <tr>
      <th>950679</th>
      <td>726601</td>
      <td>MzAxNDQxNjk3NQ==</td>
      <td>CulinaryCapers</td>
      <td>CulinaryCapersChina</td>
      <td>China&amp;#39;s&amp;nbsp;first&amp;nbsp;choice&amp;nbsp;for&amp;nb...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>CulinaryCapers</td>
    </tr>
    <tr>
      <th>951125</th>
      <td>728385</td>
      <td>MzAxNTAwNjEwOA==</td>
      <td>齐市自行车运动俱乐部</td>
      <td>TREK0452</td>
      <td>回复“00”&amp;nbsp;&amp;nbsp;世界十大自行车品牌排行榜&amp;nbsp;回复“01”&amp;nbs...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>齐市自行车运动俱乐部</td>
    </tr>
    <tr>
      <th>815449</th>
      <td>185681</td>
      <td>MzA3NTQ0OTE5NA==</td>
      <td>海盐时代旅游</td>
      <td>NaN</td>
      <td>【游天下找时代】：承接国内旅游/出境旅游&amp;nbsp;/疗休养/会务接待/商务考察/户外拓展/...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434773343</td>
      <td>海盐时代旅游</td>
    </tr>
    <tr>
      <th>1508575</th>
      <td>588698</td>
      <td>MzA5NTE3NzMxMQ==</td>
      <td>康乐鑫健康家园</td>
      <td>aaa2434715824</td>
      <td>均衡营养&amp;nbsp;享瘦人生！&amp;nbsp;&amp;nbsp;&amp;nbsp;健康生活&amp;nbsp;享受人...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>康乐鑫健康家园</td>
    </tr>
    <tr>
      <th>956791</th>
      <td>751049</td>
      <td>MzAxODU0ODE1NQ==</td>
      <td>北京飞影文化传播有限公司</td>
      <td>NaN</td>
      <td>&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;北京飞影...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>北京飞影文化传播有限公司</td>
    </tr>
    <tr>
      <th>810609</th>
      <td>166321</td>
      <td>MjM5ODA4Mzc2MA==</td>
      <td>北京天王星娱乐有限公司</td>
      <td>bjtianwangstar928</td>
      <td>北京天王星娱乐有限公司&amp;nbsp;地址：北京市东城区珠市口东大街12号&amp;nbsp;电话：01...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434766796</td>
      <td>北京天王星娱乐有限公司</td>
    </tr>
    <tr>
      <th>1919221</th>
      <td>2231282</td>
      <td>MzA3NTQwNTQ3Mg==</td>
      <td>菩提茶缘阁</td>
      <td>PTCYG-2310328</td>
      <td>菩提茶缘阁是一个集素菜、品茶、闻香、修禅为一体的静心养生妙境。&amp;nbsp;&amp;nbsp;&amp;nb...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1453537482</td>
      <td>菩提茶缘阁</td>
    </tr>
    <tr>
      <th>2502462</th>
      <td>2194435</td>
      <td>MjM5MDQ2NjQ0Mw==</td>
      <td>FilmNeverDie膠卷不死</td>
      <td>FilmNeverDie84</td>
      <td>WE&amp;nbsp;ARE&amp;nbsp;FILM&amp;nbsp;LOVER,&amp;nbsp;GEEK,&amp;n...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1452089177</td>
      <td>FilmNeverDie膠卷不死</td>
    </tr>
    <tr>
      <th>962670</th>
      <td>774565</td>
      <td>MjM5NzkwOTgzNg==</td>
      <td>凌恒书法工作室</td>
      <td>tianchao0275</td>
      <td>凌恒书法工作室是由太原师范书法系的优秀毕业生和首师大研究生共同创办的书法艺术高考培训班。&amp;n...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434695699</td>
      <td>凌恒书法工作室</td>
    </tr>
    <tr>
      <th>2400165</th>
      <td>1785247</td>
      <td>MzI5MjAyMjQyMQ==</td>
      <td>梦想话筒少儿主持口才</td>
      <td>NaN</td>
      <td>3岁以上&amp;nbsp;&amp;nbsp;形体训练语言发声儿歌练习开心识字。&amp;nbsp;绕口令练习，古...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1440505679</td>
      <td>梦想话筒少儿主持口才</td>
    </tr>
    <tr>
      <th>1905461</th>
      <td>2176242</td>
      <td>MzIwNDA5OTUzMA==</td>
      <td>壹T街舞</td>
      <td>NaN</td>
      <td>龙岩专业流行舞蹈培训和少儿舞蹈培训的知名机构，壹T舞蹈培训机构主营：&amp;nbsp;成人街舞&amp;n...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1451443793</td>
      <td>壹T街舞</td>
    </tr>
    <tr>
      <th>1744820</th>
      <td>1533678</td>
      <td>MzA5MTUwMzEwMQ==</td>
      <td>祥瑞艺术馆</td>
      <td>xiangruiyishuguan</td>
      <td>祥瑞沉香檀香&amp;nbsp;&amp;nbsp;&amp;nbsp;收藏热线：&amp;nbsp;400-1177-66...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1438074394</td>
      <td>祥瑞艺术馆</td>
    </tr>
  </tbody>
</table>
<p>7346 rows × 8 columns</p>
</div>




```python
biz_long_descriptions.iloc[0].biz_description[:100]
```




    '无副作用&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'



Looks like the same problem as with `biz_name`. However, there is also an issue with excessive space. I'm going to remove it, because likely it doesn't bring much value.


```python
biz['clean_biz_description'] = biz.biz_description.map(double_unescape_html_text).str.replace('\s+', ' ')
```


```python
clean_biz_description_len = biz.clean_biz_description.str.len()
clean_biz_description_len.describe(percentiles=[0.5, 0.75, 0.9, 0.95, 0.999]).astype(int)
```




    count    2541002
    mean          47
    std           35
    min            1
    50%           38
    75%           73
    90%          106
    95%          116
    99.9%        135
    max         5937
    Name: clean_biz_description, dtype: int64




```python
clean_biz_long_descriptions = biz.loc[clean_biz_description_len[clean_biz_description_len > 150].sort_values(ascending=False).index]
print(clean_biz_long_descriptions.shape)
clean_biz_long_descriptions
```

    (329, 9)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>biz_id</th>
      <th>biz_name</th>
      <th>biz_code</th>
      <th>biz_description</th>
      <th>qr_code</th>
      <th>timestamp</th>
      <th>clean_biz_name</th>
      <th>clean_biz_description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2254769</th>
      <td>1203663</td>
      <td>MzA3MzA1NTY0NA==</td>
      <td>群众群特通告</td>
      <td>ENA1st</td>
      <td>群众群特的通告平台！&lt;br/&gt;片酬在150元/天以内！&lt;br/&gt;&lt;br/&gt;&lt;br/&gt;&lt;br/...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436114524</td>
      <td>群众群特通告</td>
      <td>群众群特的通告平台！&lt;br/&gt;片酬在150元/天以内！&lt;br/&gt;&lt;br/&gt;&lt;br/&gt;&lt;br/...</td>
    </tr>
    <tr>
      <th>909155</th>
      <td>560505</td>
      <td>MzA5MjI5MzI2MQ==</td>
      <td>合肥氧宜多生态硅藻</td>
      <td>HF_Odour</td>
      <td>硅藻泥&amp;nbsp;:&amp;nbsp;有效吸收、分解装修过程中产生的甲醛、苯等有害物质，调节空气湿...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>合肥氧宜多生态硅藻</td>
      <td>硅藻泥 : 有效吸收、分解装修过程中产生的甲醛、苯等有害物质，调节空气湿度。 &lt;br/&gt;白炭...</td>
    </tr>
    <tr>
      <th>510529</th>
      <td>1335764</td>
      <td>MzA3ODc2NzI5Ng==</td>
      <td>北京皮克布克烟台站</td>
      <td>pikebukeyantaizhan</td>
      <td>看听结合&amp;nbsp;亲子共读&lt;br/&gt;网上选书&amp;nbsp;轻松快捷&lt;br/&gt;培养阅读习惯&amp;n...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436709571</td>
      <td>北京皮克布克烟台站</td>
      <td>看听结合 亲子共读&lt;br/&gt;网上选书 轻松快捷&lt;br/&gt;培养阅读习惯 享受阅读乐趣&lt;br/&gt;...</td>
    </tr>
    <tr>
      <th>344533</th>
      <td>671780</td>
      <td>MzAwNTU0NDM1MQ==</td>
      <td>黄岐巨圣女鞋</td>
      <td>juusnn888</td>
      <td>关注黄岐巨圣女鞋，接收了解黄岐最具性价比的时尚女鞋，优惠活动，打造最美丽的你。地址：佛山市南...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>黄岐巨圣女鞋</td>
      <td>关注黄岐巨圣女鞋，接收了解黄岐最具性价比的时尚女鞋，优惠活动，打造最美丽的你。地址：佛山市南...</td>
    </tr>
    <tr>
      <th>1803904</th>
      <td>1770014</td>
      <td>MzIzODA0ODM0MA==</td>
      <td>秀容微生活</td>
      <td>NaN</td>
      <td>为所有忻州微友提供生活资讯、美食推荐、休闲娱乐、消费指南、便民服务、招聘求职、二手信息……&lt;...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1440362333</td>
      <td>秀容微生活</td>
      <td>为所有忻州微友提供生活资讯、美食推荐、休闲娱乐、消费指南、便民服务、招聘求职、二手信息……&lt;...</td>
    </tr>
    <tr>
      <th>1817355</th>
      <td>1823818</td>
      <td>MzA4OTcyOTcxMw==</td>
      <td>华隆泰便利连锁超市</td>
      <td>lht19781016</td>
      <td>便民服务：&lt;br/&gt;一、主营:&lt;br/&gt;烟酒，小吃、粮油、百货、妇婴用品、日化...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1440999579</td>
      <td>华隆泰便利连锁超市</td>
      <td>便民服务：&lt;br/&gt;一、主营:&lt;br/&gt;烟酒，小吃、粮油、百货、妇婴用品、日化洗护用品。&lt;...</td>
    </tr>
    <tr>
      <th>105388</th>
      <td>2515093</td>
      <td>MzIyOTAxMzY5Nw==</td>
      <td>加宝地板金昌服务中心</td>
      <td>jiabaojc15214174568</td>
      <td>本店坚持以人为本，质量第一，追求卓越的经营理念。着力打造‘’加宝“品牌形象。引进德国技术高端...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1461558873</td>
      <td>加宝地板金昌服务中心</td>
      <td>本店坚持以人为本，质量第一，追求卓越的经营理念。着力打造‘’加宝“品牌形象。引进德国技术高端...</td>
    </tr>
    <tr>
      <th>2253244</th>
      <td>1197563</td>
      <td>MzA4NDA0NjM1MA==</td>
      <td>六一护肤</td>
      <td>Ddjh9690</td>
      <td>你若高贵，一切都便宜！你若便宜，一切都高贵！女人，一定要好好减肥，好好护肤，好好赚钱，当你又...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436073361</td>
      <td>六一护肤</td>
      <td>你若高贵，一切都便宜！你若便宜，一切都高贵！女人，一定要好好减肥，好好护肤，好好赚钱，当你又...</td>
    </tr>
    <tr>
      <th>632169</th>
      <td>1822324</td>
      <td>MzAxNjY3MDA4Mw==</td>
      <td>台湾帮女郎</td>
      <td>an15725482433</td>
      <td>&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;帮女郎凝聚的功效&lt;br/&gt;一、消...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1440987765</td>
      <td>台湾帮女郎</td>
      <td>帮女郎凝聚的功效&lt;br/&gt;一、消炎杀菌&lt;br/&gt;二、修复损伤&lt;br/&gt;三、味消毒除&lt;br/...</td>
    </tr>
    <tr>
      <th>917256</th>
      <td>592909</td>
      <td>MzA5NTU4NDEwMA==</td>
      <td>闺蜜</td>
      <td>gm8111</td>
      <td>如果不是你&lt;br/&gt;&lt;br/&gt;我不会相信&lt;br/&gt;&lt;br/&gt;朋友比情人还死心塌地&lt;br/&gt;&lt;...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>闺蜜</td>
      <td>如果不是你&lt;br/&gt;&lt;br/&gt;我不会相信&lt;br/&gt;&lt;br/&gt;朋友比情人还死心塌地&lt;br/&gt;&lt;...</td>
    </tr>
    <tr>
      <th>1065033</th>
      <td>1184017</td>
      <td>MzAwODY1NDYxMQ==</td>
      <td>古钱币私下交易免费鉴定中心</td>
      <td>NaN</td>
      <td>咸平元宝&lt;br/&gt;乾道元宝&lt;br/&gt;宣和重宝&lt;br/&gt;圣宋通宝&lt;br/&gt;靖康元宝&lt;br/&gt;宣...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435999893</td>
      <td>古钱币私下交易免费鉴定中心</td>
      <td>咸平元宝&lt;br/&gt;乾道元宝&lt;br/&gt;宣和重宝&lt;br/&gt;圣宋通宝&lt;br/&gt;靖康元宝&lt;br/&gt;宣...</td>
    </tr>
    <tr>
      <th>2162859</th>
      <td>836023</td>
      <td>MzAwMDUxNjYzNA==</td>
      <td>星艺装饰丰顺分公司</td>
      <td>NaN</td>
      <td>&lt;br/&gt;&lt;br/&gt;广东星艺装饰集团股份有限公司于1991年在广州创立，属建筑装修装饰工程专...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434583373</td>
      <td>星艺装饰丰顺分公司</td>
      <td>&lt;br/&gt;&lt;br/&gt;广东星艺装饰集团股份有限公司于1991年在广州创立，属建筑装修装饰工程专...</td>
    </tr>
    <tr>
      <th>948689</th>
      <td>718641</td>
      <td>MzAxMzM1MjExNg==</td>
      <td>尘封</td>
      <td>V270085423</td>
      <td>你见，或者不见我&lt;br/&gt;我就在那里&lt;br/&gt;不悲不喜&lt;br/&gt;你念，或者不念我&lt;br/&gt;情...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>尘封</td>
      <td>你见，或者不见我&lt;br/&gt;我就在那里&lt;br/&gt;不悲不喜&lt;br/&gt;你念，或者不念我&lt;br/&gt;情...</td>
    </tr>
    <tr>
      <th>1166918</th>
      <td>1591557</td>
      <td>MzAwOTY2NTU0MQ==</td>
      <td>睿美徒手精雕</td>
      <td>RM13780629401</td>
      <td>徒手整形&amp;nbsp;以中医学&amp;nbsp;解剖学为基础&amp;nbsp;以整形师的各种手法&amp;nbsp...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1438553800</td>
      <td>睿美徒手精雕</td>
      <td>徒手整形 以中医学 解剖学为基础 以整形师的各种手法 加上人体的艺术审美学等多门学科的综合运...</td>
    </tr>
    <tr>
      <th>1065959</th>
      <td>1187721</td>
      <td>MzA3OTc4MDE4MQ==</td>
      <td>伊世茗尚</td>
      <td>tb985611163</td>
      <td>伊世茗尚专注于微商发展事业，成立于2015年，注册资金200万，以独特的护肤配方引领着护肤界...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436026073</td>
      <td>伊世茗尚</td>
      <td>伊世茗尚专注于微商发展事业，成立于2015年，注册资金200万，以独特的护肤配方引领着护肤界...</td>
    </tr>
    <tr>
      <th>379698</th>
      <td>812440</td>
      <td>MzA4NzU2MDQ5Mg==</td>
      <td>澳门四季酒店</td>
      <td>FSMacau</td>
      <td>矗立於連接氹仔島及路環島的金光大道上，澳門四季酒店位置優越、交通四通八達。酒店樓高20層，設...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434592195</td>
      <td>澳门四季酒店</td>
      <td>矗立於連接氹仔島及路環島的金光大道上，澳門四季酒店位置優越、交通四通八達。酒店樓高20層，設...</td>
    </tr>
    <tr>
      <th>1755809</th>
      <td>1577634</td>
      <td>MzI1NjAyMTcwOQ==</td>
      <td>新注册公众号</td>
      <td>QJMIFYS</td>
      <td>一、权健火疗通络开背&lt;br/&gt;&lt;br/&gt;&lt;br/&gt;二、权健火疗强肾壮腰&lt;br/&gt;&lt;br/&gt;三...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1438431599</td>
      <td>新注册公众号</td>
      <td>一、权健火疗通络开背&lt;br/&gt;&lt;br/&gt;&lt;br/&gt;二、权健火疗强肾壮腰&lt;br/&gt;&lt;br/&gt;三...</td>
    </tr>
    <tr>
      <th>2166769</th>
      <td>851663</td>
      <td>MzAxMTA0MjY2NA==</td>
      <td>阳谷户外运动俱乐部</td>
      <td>yangguhuwaijvlebu</td>
      <td>&lt;br/&gt;阳谷户外运动俱乐部，是一个以自助式AA户外活动、驴行为主的自发组织。以推动全民健身...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434573737</td>
      <td>阳谷户外运动俱乐部</td>
      <td>&lt;br/&gt;阳谷户外运动俱乐部，是一个以自助式AA户外活动、驴行为主的自发组织。以推动全民健身...</td>
    </tr>
    <tr>
      <th>662589</th>
      <td>1944004</td>
      <td>MzA4OTUzNTkzMQ==</td>
      <td>神农谷祛痘</td>
      <td>shennongguqudou</td>
      <td>“神农谷”系列产品是宏森科技与广州杨森生物联合研发的一款针对于青春痘，痤疮、粉刺、美白、爽肤...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1444182466</td>
      <td>神农谷祛痘</td>
      <td>“神农谷”系列产品是宏森科技与广州杨森生物联合研发的一款针对于青春痘，痤疮、粉刺、美白、爽肤...</td>
    </tr>
    <tr>
      <th>1205265</th>
      <td>1744945</td>
      <td>MzIxODA0NTM1Nw==</td>
      <td>古玩古董私下交易免费鉴定平台</td>
      <td>NaN</td>
      <td>宋元通宝&lt;br/&gt;太平通宝&lt;br/&gt;天禧通宝&lt;br/&gt;皇宋通宝&lt;br/&gt;大德通宝&lt;br/&gt;淳...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1440115611</td>
      <td>古玩古董私下交易免费鉴定平台</td>
      <td>宋元通宝&lt;br/&gt;太平通宝&lt;br/&gt;天禧通宝&lt;br/&gt;皇宋通宝&lt;br/&gt;大德通宝&lt;br/&gt;淳...</td>
    </tr>
    <tr>
      <th>1568689</th>
      <td>829154</td>
      <td>MzA5NTY2MjQzMQ==</td>
      <td>九职院学生会女生部</td>
      <td>NaN</td>
      <td>&lt;br/&gt;女生部是校学生会的一个重要组成部分，并以&lt;br/&gt;&lt;br/&gt;“&lt;br/&gt;&lt;br/&gt;...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434612737</td>
      <td>九职院学生会女生部</td>
      <td>&lt;br/&gt;女生部是校学生会的一个重要组成部分，并以&lt;br/&gt;&lt;br/&gt;“&lt;br/&gt;&lt;br/&gt;...</td>
    </tr>
    <tr>
      <th>1084952</th>
      <td>1263693</td>
      <td>MzI2MTAwNDUyNA==</td>
      <td>古钱币私下交易免费鉴定平台</td>
      <td>NaN</td>
      <td>宋元通宝&lt;br/&gt;太平通宝&lt;br/&gt;天禧通宝&lt;br/&gt;皇宋通宝&lt;br/&gt;大德通宝&lt;br/&gt;淳...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436361213</td>
      <td>古钱币私下交易免费鉴定平台</td>
      <td>宋元通宝&lt;br/&gt;太平通宝&lt;br/&gt;天禧通宝&lt;br/&gt;皇宋通宝&lt;br/&gt;大德通宝&lt;br/&gt;淳...</td>
    </tr>
    <tr>
      <th>2242480</th>
      <td>1154507</td>
      <td>MzAxMjYzOTk5NQ==</td>
      <td>Ammy的心情日记</td>
      <td>Ammy-U</td>
      <td>天上掉下了一片乌云&lt;br/&gt;好想对一个人说：我想你了&lt;br/&gt;咖啡不小心溅到了白色外套上&lt;b...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435828325</td>
      <td>Ammy的心情日记</td>
      <td>天上掉下了一片乌云&lt;br/&gt;好想对一个人说：我想你了&lt;br/&gt;咖啡不小心溅到了白色外套上&lt;b...</td>
    </tr>
    <tr>
      <th>1027720</th>
      <td>1034765</td>
      <td>MzA4OTg4MTExNQ==</td>
      <td>沁园净水寒亭专卖</td>
      <td>hantingqinyuan</td>
      <td>沁园集团是一家专业从事净水设备、饮水设备、工业水处理的国家高新企业，国家净水器及系统化工作组...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435304503</td>
      <td>沁园净水寒亭专卖</td>
      <td>沁园集团是一家专业从事净水设备、饮水设备、工业水处理的国家高新企业，国家净水器及系统化工作组...</td>
    </tr>
    <tr>
      <th>1718422</th>
      <td>1428086</td>
      <td>MzA3NDU2MjMyMQ==</td>
      <td>一秒文化培训团队</td>
      <td>wanjiayimiao</td>
      <td>◎&amp;nbsp;培训是企业发展的需要&lt;br/&gt;&amp;nbsp;&lt;br/&gt;◎&amp;nbsp;人才是培训出...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1437353568</td>
      <td>一秒文化培训团队</td>
      <td>◎ 培训是企业发展的需要&lt;br/&gt; &lt;br/&gt;◎ 人才是培训出来的&lt;br/&gt; &lt;br/&gt;◎ ...</td>
    </tr>
    <tr>
      <th>2078404</th>
      <td>498203</td>
      <td>MzA4NTgyMTMwMg==</td>
      <td>名言</td>
      <td>HOT0092</td>
      <td>&lt;br/&gt;100句精选的人生哲理名言&lt;br/&gt;&lt;br/&gt;100句精选的励志名言名句&lt;br/&gt;...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>名言</td>
      <td>&lt;br/&gt;100句精选的人生哲理名言&lt;br/&gt;&lt;br/&gt;100句精选的励志名言名句&lt;br/&gt;...</td>
    </tr>
    <tr>
      <th>1584187</th>
      <td>891146</td>
      <td>MzA3Nzc2ODU2OA==</td>
      <td>小丸子和她的卡农</td>
      <td>aimarch1996</td>
      <td>诗意的&lt;br/&gt;安静的&lt;br/&gt;疯狂的&lt;br/&gt;喧闹的&lt;br/&gt;况是青春日将暮&lt;br/&gt;桃花...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434829788</td>
      <td>小丸子和她的卡农</td>
      <td>诗意的&lt;br/&gt;安静的&lt;br/&gt;疯狂的&lt;br/&gt;喧闹的&lt;br/&gt;况是青春日将暮&lt;br/&gt;桃花...</td>
    </tr>
    <tr>
      <th>1806949</th>
      <td>1782194</td>
      <td>MzA5MzE1ODQ3NQ==</td>
      <td>美丽小铺华阳秀水三号店</td>
      <td>NaN</td>
      <td>&amp;nbsp;&amp;nbsp;美丽，健康，品质生活是爱美女性的追求和梦想。澳洲美丽小铺Beauty...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1440481920</td>
      <td>美丽小铺华阳秀水三号店</td>
      <td>美丽，健康，品质生活是爱美女性的追求和梦想。澳洲美丽小铺Beauty shop,顺应现代人...</td>
    </tr>
    <tr>
      <th>1760785</th>
      <td>1597538</td>
      <td>MzAxNDQ3NTcyMg==</td>
      <td>道成禅修</td>
      <td>daochengchanxiu007</td>
      <td>道成禅修养生中心是大庆市首家以禅修炼养为核心的养生会馆，中心倡导性命双修，以辟谷禅为契机，以...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1438604519</td>
      <td>道成禅修</td>
      <td>道成禅修养生中心是大庆市首家以禅修炼养为核心的养生会馆，中心倡导性命双修，以辟谷禅为契机，以...</td>
    </tr>
    <tr>
      <th>2370698</th>
      <td>1667379</td>
      <td>MzI5MjAyNzU1OA==</td>
      <td>张辉弟子侃营销</td>
      <td>zhanghuidizi</td>
      <td>哈喽！&lt;br/&gt;欢迎你入《张辉弟子侃营销》&lt;br/&gt;&lt;br/&gt;我是中国微营销创始人张辉第98...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1439393748</td>
      <td>张辉弟子侃营销</td>
      <td>哈喽！&lt;br/&gt;欢迎你入《张辉弟子侃营销》&lt;br/&gt;&lt;br/&gt;我是中国微营销创始人张辉第98...</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>1594239</th>
      <td>931354</td>
      <td>MzAwODE2OTI3Nw==</td>
      <td>临清口腔</td>
      <td>lqkqyy</td>
      <td>口腔医院主要开展项目：&lt;br/&gt;一、各种口腔外伤，肿瘤、畸形手术&lt;br/&gt;二、各种牙体病、口...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434955060</td>
      <td>临清口腔</td>
      <td>口腔医院主要开展项目：&lt;br/&gt;一、各种口腔外伤，肿瘤、畸形手术&lt;br/&gt;二、各种牙体病、口...</td>
    </tr>
    <tr>
      <th>1103182</th>
      <td>1336613</td>
      <td>MjM5NTQ1OTYyMA==</td>
      <td>北镇青岩寺（歪脖老母）公众平台</td>
      <td>qingyansiyangming</td>
      <td>郑重声明&lt;br/&gt;近几年以来，一些团队打着歪脖老母的旗号以牟取暴利为目的，将到青岩寺拜歪脖老...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436714600</td>
      <td>北镇青岩寺（歪脖老母）公众平台</td>
      <td>郑重声明&lt;br/&gt;近几年以来，一些团队打着歪脖老母的旗号以牟取暴利为目的，将到青岩寺拜歪脖老...</td>
    </tr>
    <tr>
      <th>2396947</th>
      <td>1772375</td>
      <td>MzI2NzAzODY3Mg==</td>
      <td>盛通汽车俱乐部</td>
      <td>hnstqcjlb</td>
      <td>俱乐部会员可享全年全城多家：&lt;br/&gt;汽车美容店不限次免费洗车！&lt;br/&gt;大型洗浴中心&amp;nb...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1440389944</td>
      <td>盛通汽车俱乐部</td>
      <td>俱乐部会员可享全年全城多家：&lt;br/&gt;汽车美容店不限次免费洗车！&lt;br/&gt;大型洗浴中心 不限...</td>
    </tr>
    <tr>
      <th>1670441</th>
      <td>1236162</td>
      <td>MzA4OTkwNDE5OQ==</td>
      <td>盐城市小荷花舞蹈艺术中心</td>
      <td>yc-xhh</td>
      <td>&amp;nbsp;&amp;nbsp;小荷花舞蹈艺术中心成立十多年来，专注少儿舞蹈艺术教育，教授北京舞蹈学...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436259450</td>
      <td>盐城市小荷花舞蹈艺术中心</td>
      <td>小荷花舞蹈艺术中心成立十多年来，专注少儿舞蹈艺术教育，教授北京舞蹈学院舞蹈教材:芭蕾舞、民...</td>
    </tr>
    <tr>
      <th>1119437</th>
      <td>1401633</td>
      <td>MzAwMTI3NzEwOA==</td>
      <td>澳尚置业</td>
      <td>AUSUNCITY</td>
      <td>关注澳尚置业公众平台，你将获得最专业的房地产资讯和澳洲最具潜力投资物业的优先购买权利。&lt;br...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1437195424</td>
      <td>澳尚置业</td>
      <td>关注澳尚置业公众平台，你将获得最专业的房地产资讯和澳洲最具潜力投资物业的优先购买权利。&lt;br...</td>
    </tr>
    <tr>
      <th>1590237</th>
      <td>915346</td>
      <td>MzAxNTYwNTU2Ng==</td>
      <td>田龙</td>
      <td>NaN</td>
      <td>山东枣庄旭峰实业主营&lt;br/&gt;果汁饮料，功能饮料，&lt;br/&gt;乳酸菌、蛋白、利乐砖等系列产品，...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434915707</td>
      <td>田龙</td>
      <td>山东枣庄旭峰实业主营&lt;br/&gt;果汁饮料，功能饮料，&lt;br/&gt;乳酸菌、蛋白、利乐砖等系列产品，...</td>
    </tr>
    <tr>
      <th>1571995</th>
      <td>842378</td>
      <td>MzAwNjM4MzcyNQ==</td>
      <td>邑展地产有限公司</td>
      <td>qq2453513389</td>
      <td>以台湾优质地产销售，物业管理服务团队整团移植到广西。&lt;br/&gt;服務信念：●專業\u000B●...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434601790</td>
      <td>邑展地产有限公司</td>
      <td>以台湾优质地产销售，物业管理服务团队整团移植到广西。&lt;br/&gt;服務信念：●專業\u000B●...</td>
    </tr>
    <tr>
      <th>1538733</th>
      <td>709330</td>
      <td>MzAxMjA0ODE3Ng==</td>
      <td>小投资零风险教你开网店微店田艳芳</td>
      <td>tyf1403</td>
      <td>♤&amp;nbsp;加盟服务流程：&lt;br/&gt;♤&amp;nbsp;签订协议，支付加盟费，公司安排老师&lt;br...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>小投资零风险教你开网店微店田艳芳</td>
      <td>♤ 加盟服务流程：&lt;br/&gt;♤ 签订协议，支付加盟费，公司安排老师&lt;br/&gt;♤ 教您认证、提...</td>
    </tr>
    <tr>
      <th>1254003</th>
      <td>1939897</td>
      <td>MjM5Mzc1NDg3MQ==</td>
      <td>潘俊贤官号</td>
      <td>panjunxianguanhao</td>
      <td>潘俊贤老师&lt;br/&gt;&lt;br/&gt;亚洲顶级领导力研究大师&lt;br/&gt;华人顶尖战略运营大师&lt;br/&gt;...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1444039745</td>
      <td>潘俊贤官号</td>
      <td>潘俊贤老师&lt;br/&gt;&lt;br/&gt;亚洲顶级领导力研究大师&lt;br/&gt;华人顶尖战略运营大师&lt;br/&gt;...</td>
    </tr>
    <tr>
      <th>1648840</th>
      <td>1149758</td>
      <td>MzAwODUzMDczOA==</td>
      <td>天华建筑四所</td>
      <td>NaN</td>
      <td>上海天华建筑四所&lt;br/&gt;&lt;br/&gt;马&amp;nbsp;亮&amp;nbsp;|&amp;nbsp;四所所长&lt;br...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435804080</td>
      <td>天华建筑四所</td>
      <td>上海天华建筑四所&lt;br/&gt;&lt;br/&gt;马 亮 | 四所所长&lt;br/&gt;手机：135 6431 6...</td>
    </tr>
    <tr>
      <th>2271895</th>
      <td>1272167</td>
      <td>MzIzMzAxMDkyMw==</td>
      <td>8点整体训</td>
      <td>PEstart8</td>
      <td>致力于青少年和少儿的体能训练，按学员身体素质分层次&lt;br/&gt;训练围绕两条主线：&lt;br/&gt;1、...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436394036</td>
      <td>8点整体训</td>
      <td>致力于青少年和少儿的体能训练，按学员身体素质分层次&lt;br/&gt;训练围绕两条主线：&lt;br/&gt;1、...</td>
    </tr>
    <tr>
      <th>1684599</th>
      <td>1292794</td>
      <td>MzA5OTIxNzU2Mg==</td>
      <td>韩妆购美丽</td>
      <td>A15845486706</td>
      <td>感谢关注韩国美妆达人&lt;br/&gt;本人&amp;nbsp;对护肤彩妆有着独有的钟爱&lt;br/&gt;希望把这份爱...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436498369</td>
      <td>韩妆购美丽</td>
      <td>感谢关注韩国美妆达人&lt;br/&gt;本人 对护肤彩妆有着独有的钟爱&lt;br/&gt;希望把这份爱分享给所有...</td>
    </tr>
    <tr>
      <th>639504</th>
      <td>1851664</td>
      <td>MzI1ODA0MTYzOQ==</td>
      <td>金童jintongbaobei</td>
      <td>BJjintongbaobei</td>
      <td>北京金童宝贝母婴培训中心主营：&lt;br/&gt;（母婴护理师，催乳师、育婴师）培训及外派服务。&lt;br...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1441365537</td>
      <td>金童jintongbaobei</td>
      <td>北京金童宝贝母婴培训中心主营：&lt;br/&gt;（母婴护理师，催乳师、育婴师）培训及外派服务。&lt;br...</td>
    </tr>
    <tr>
      <th>2125737</th>
      <td>687535</td>
      <td>MzAwODI5NDMzNA==</td>
      <td>喜维丽商贸</td>
      <td>NaN</td>
      <td>吧台设备供应&lt;br/&gt;意式咖啡设备、精品咖啡器具、软饮酒水器具&lt;br/&gt;&lt;br/&gt;咖啡物料供...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>喜维丽商贸</td>
      <td>吧台设备供应&lt;br/&gt;意式咖啡设备、精品咖啡器具、软饮酒水器具&lt;br/&gt;&lt;br/&gt;咖啡物料供...</td>
    </tr>
    <tr>
      <th>532042</th>
      <td>1421816</td>
      <td>MzA4NTkyNDc3NA==</td>
      <td>好孩子好妈妈俱乐部</td>
      <td>hhzhmmjlb</td>
      <td>&amp;nbsp;&amp;nbsp;&amp;nbsp;“好孩子好妈妈俱乐部”是妈妈们基于培养孩子们健康、茁壮成...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1437310425</td>
      <td>好孩子好妈妈俱乐部</td>
      <td>“好孩子好妈妈俱乐部”是妈妈们基于培养孩子们健康、茁壮成长的一份责任心，自愿结成的公益性互...</td>
    </tr>
    <tr>
      <th>2110637</th>
      <td>627135</td>
      <td>MzA5OTAyNTY2MA==</td>
      <td>温州九龙生态园旅游有限公司</td>
      <td>WZJL64892222</td>
      <td>温州九龙生态园新疆一龙&lt;br/&gt;投资集团公司建设的集生&lt;br/&gt;态、文化、旅游、陵园为一&lt;b...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>温州九龙生态园旅游有限公司</td>
      <td>温州九龙生态园新疆一龙&lt;br/&gt;投资集团公司建设的集生&lt;br/&gt;态、文化、旅游、陵园为一&lt;b...</td>
    </tr>
    <tr>
      <th>2086654</th>
      <td>531203</td>
      <td>MzA4OTIwMDUzNA==</td>
      <td>震不倒的房子</td>
      <td>zbddfz</td>
      <td>依托地震安全示范社区建设&lt;br/&gt;打造地产销售亮点；&lt;br/&gt;采用先进减震隔震技术措施&lt;br...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>震不倒的房子</td>
      <td>依托地震安全示范社区建设&lt;br/&gt;打造地产销售亮点；&lt;br/&gt;采用先进减震隔震技术措施&lt;br...</td>
    </tr>
    <tr>
      <th>555751</th>
      <td>1516652</td>
      <td>MzI2OTAxOTI0NA==</td>
      <td>婕斯环球商机</td>
      <td>JNS1078</td>
      <td>婕斯Y.E.S.细胞优化管理系統加盟&lt;br/&gt;&lt;br/&gt;国际互联网交互式网购的电子商务公司&lt;...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1437960378</td>
      <td>婕斯环球商机</td>
      <td>婕斯Y.E.S.细胞优化管理系統加盟&lt;br/&gt;&lt;br/&gt;国际互联网交互式网购的电子商务公司&lt;...</td>
    </tr>
    <tr>
      <th>2166585</th>
      <td>850927</td>
      <td>MzAxMjMyMTYyMQ==</td>
      <td>临沂财鑫钢铁集团有限公司</td>
      <td>NaN</td>
      <td>河北敬业钢铁集团&lt;br/&gt;山东泰山钢铁集团&lt;br/&gt;山东鑫华特钢集团&lt;br/&gt;江苏镔钢集团苏...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434583358</td>
      <td>临沂财鑫钢铁集团有限公司</td>
      <td>河北敬业钢铁集团&lt;br/&gt;山东泰山钢铁集团&lt;br/&gt;山东鑫华特钢集团&lt;br/&gt;江苏镔钢集团苏...</td>
    </tr>
    <tr>
      <th>620873</th>
      <td>1777140</td>
      <td>MzIxOTA0NTYzNg==</td>
      <td>小月巴</td>
      <td>NaN</td>
      <td>对于我来说&amp;nbsp;&lt;br/&gt;最幸福的事就是&lt;br/&gt;因为拍照爱上一个女孩&lt;br/&gt;拍着拍...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1440434599</td>
      <td>小月巴</td>
      <td>对于我来说 &lt;br/&gt;最幸福的事就是&lt;br/&gt;因为拍照爱上一个女孩&lt;br/&gt;拍着拍着 她成了...</td>
    </tr>
    <tr>
      <th>373095</th>
      <td>786028</td>
      <td>MzA3Mzg3NDQ4MQ==</td>
      <td>三一英语考点JFC金芳程教育</td>
      <td>jfc_trinity001</td>
      <td>家庭教育校帮手，在家也能享受英语环境，&lt;br/&gt;分享英语教育国际新思维，提高孩子的英语口语水...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434694372</td>
      <td>三一英语考点JFC金芳程教育</td>
      <td>家庭教育校帮手，在家也能享受英语环境，&lt;br/&gt;分享英语教育国际新思维，提高孩子的英语口语水...</td>
    </tr>
    <tr>
      <th>941183</th>
      <td>688617</td>
      <td>MzAwODM4Njk0Nw==</td>
      <td>SU素时尚服饰高级定制</td>
      <td>SU_JUDY</td>
      <td>北京S*U素品牌服饰有限公司，感谢您的关注。&lt;br/&gt;传递素生活的慢态度；慢下来，感受我们用...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>SU素时尚服饰高级定制</td>
      <td>北京S*U素品牌服饰有限公司，感谢您的关注。&lt;br/&gt;传递素生活的慢态度；慢下来，感受我们用...</td>
    </tr>
    <tr>
      <th>786989</th>
      <td>71841</td>
      <td>MzAxODUzMTk5MQ==</td>
      <td>雨卉美乐生活</td>
      <td>NaN</td>
      <td>纯净的心，纯净的家：&lt;br/&gt;美乐家：全球顶级环保产品，国际认证。&lt;br/&gt;有日用，营养，美...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434743633</td>
      <td>雨卉美乐生活</td>
      <td>纯净的心，纯净的家：&lt;br/&gt;美乐家：全球顶级环保产品，国际认证。&lt;br/&gt;有日用，营养，美...</td>
    </tr>
    <tr>
      <th>1868649</th>
      <td>2028994</td>
      <td>MzAxOTU1ODMwMQ==</td>
      <td>漫MoreCafe</td>
      <td>NaN</td>
      <td>more.cafe-&lt;br/&gt;所有的理由多很单纯&lt;br/&gt;这就是真我的随性&lt;br/&gt;落地的门...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1446265064</td>
      <td>漫MoreCafe</td>
      <td>more.cafe-&lt;br/&gt;所有的理由多很单纯&lt;br/&gt;这就是真我的随性&lt;br/&gt;落地的门...</td>
    </tr>
    <tr>
      <th>807663</th>
      <td>154537</td>
      <td>MzIwMTExNTk0MQ==</td>
      <td>玫丽点靓精彩人生</td>
      <td>NaN</td>
      <td>&lt;br/&gt;玫琳凯让女人展现出多面的美。&lt;br/&gt;玫琳凯相信&lt;br/&gt;女人的美丽不仅仅存在于外...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434763367</td>
      <td>玫丽点靓精彩人生</td>
      <td>&lt;br/&gt;玫琳凯让女人展现出多面的美。&lt;br/&gt;玫琳凯相信&lt;br/&gt;女人的美丽不仅仅存在于外...</td>
    </tr>
    <tr>
      <th>1822412</th>
      <td>1844046</td>
      <td>MzIyMzA1NzkzNA==</td>
      <td>咔么电影工业网</td>
      <td>kamedianying</td>
      <td>一个专业从事电影器材产品的&lt;br/&gt;对比和测评、分享制作电影中的方&lt;br/&gt;法和技巧；网站汇...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1441259663</td>
      <td>咔么电影工业网</td>
      <td>一个专业从事电影器材产品的&lt;br/&gt;对比和测评、分享制作电影中的方&lt;br/&gt;法和技巧；网站汇...</td>
    </tr>
    <tr>
      <th>852927</th>
      <td>335593</td>
      <td>MjM5OTU0ODk1MQ==</td>
      <td>Ps设计教学部</td>
      <td>ZSLX6688</td>
      <td>开设科目如下：&lt;br/&gt;平面：PS、CDR、AI&lt;br/&gt;三维：3dmax、CAD、MAYA...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>Ps设计教学部</td>
      <td>开设科目如下：&lt;br/&gt;平面：PS、CDR、AI&lt;br/&gt;三维：3dmax、CAD、MAYA...</td>
    </tr>
    <tr>
      <th>878150</th>
      <td>436485</td>
      <td>MzA3OTU5MDg4Nw==</td>
      <td>诗意生活</td>
      <td>purelife123456</td>
      <td>城市白天的喧嚣声，&lt;br/&gt;夜晚望着流离灯光。&lt;br/&gt;我们似蝼蚁般活着，&lt;br/&gt;在这钢铁...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434730000</td>
      <td>诗意生活</td>
      <td>城市白天的喧嚣声，&lt;br/&gt;夜晚望着流离灯光。&lt;br/&gt;我们似蝼蚁般活着，&lt;br/&gt;在这钢铁...</td>
    </tr>
    <tr>
      <th>2291473</th>
      <td>1350479</td>
      <td>MzAwMzU5NTAyMw==</td>
      <td>台湾安沛优天然水果青梅酵素原液</td>
      <td>anpeiyoujiaosu</td>
      <td>适用人群：&lt;br/&gt;1.经常应酬，爱吃辛辣食物人群&lt;br/&gt;2&amp;nbsp;爱吃垃圾食品，熬夜...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1436833088</td>
      <td>台湾安沛优天然水果青梅酵素原液</td>
      <td>适用人群：&lt;br/&gt;1.经常应酬，爱吃辛辣食物人群&lt;br/&gt;2 爱吃垃圾食品，熬夜，失眠人群...</td>
    </tr>
    <tr>
      <th>627585</th>
      <td>1803988</td>
      <td>MzI4MTAxNzMwMQ==</td>
      <td>城市建设杂志社</td>
      <td>NaN</td>
      <td>社内经营四本国家级权威性期刊，是广大聘职称作者们的首选：&lt;br/&gt;《城市建设理论研究》&lt;br...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1440813004</td>
      <td>城市建设杂志社</td>
      <td>社内经营四本国家级权威性期刊，是广大聘职称作者们的首选：&lt;br/&gt;《城市建设理论研究》&lt;br...</td>
    </tr>
  </tbody>
</table>
<p>329 rows × 9 columns</p>
</div>




```python
clean_biz_long_descriptions.clean_biz_description.iloc[0][:100]
```




    '群众群特的通告平台！<br/>片酬在150元/天以内！<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br'



Looks like there is still a problem with `<br>` html tags. Let's replace them with simple newlines, and combine combine subsequent newlines.


```python
biz['clean_biz_description'] = biz.clean_biz_description.str.replace('(<br/>\s*)+', '\n')
clean_biz_description_len = biz.clean_biz_description.str.len()
clean_biz_description_len.describe(percentiles=[0.5, 0.75, 0.9, 0.95, 0.999]).astype(int)
```




    count    2541002
    mean          47
    std           34
    min            1
    50%           38
    75%           72
    90%          105
    95%          115
    99.9%        121
    max          200
    Name: clean_biz_description, dtype: int64



Looks more reasonable now!


```python
clean_biz_long_descriptions = biz.loc[clean_biz_description_len[clean_biz_description_len > 150].sort_values(ascending=False).index]
print(clean_biz_long_descriptions.shape)
clean_biz_long_descriptions
```

    (5, 9)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>biz_id</th>
      <th>biz_name</th>
      <th>biz_code</th>
      <th>biz_description</th>
      <th>qr_code</th>
      <th>timestamp</th>
      <th>clean_biz_name</th>
      <th>clean_biz_description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>379698</th>
      <td>812440</td>
      <td>MzA4NzU2MDQ5Mg==</td>
      <td>澳门四季酒店</td>
      <td>FSMacau</td>
      <td>矗立於連接氹仔島及路環島的金光大道上，澳門四季酒店位置優越、交通四通八達。酒店樓高20層，設...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1434592195</td>
      <td>澳门四季酒店</td>
      <td>矗立於連接氹仔島及路環島的金光大道上，澳門四季酒店位置優越、交通四通八達。酒店樓高20層，設...</td>
    </tr>
    <tr>
      <th>662589</th>
      <td>1944004</td>
      <td>MzA4OTUzNTkzMQ==</td>
      <td>神农谷祛痘</td>
      <td>shennongguqudou</td>
      <td>“神农谷”系列产品是宏森科技与广州杨森生物联合研发的一款针对于青春痘，痤疮、粉刺、美白、爽肤...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1444182466</td>
      <td>神农谷祛痘</td>
      <td>“神农谷”系列产品是宏森科技与广州杨森生物联合研发的一款针对于青春痘，痤疮、粉刺、美白、爽肤...</td>
    </tr>
    <tr>
      <th>1231945</th>
      <td>1851665</td>
      <td>MzIyNTAyODcxMQ==</td>
      <td>平安保险黄彩凤</td>
      <td>hcf18624225955</td>
      <td>黄&amp;nbsp;彩&amp;nbsp;凤\u000B1997----加盟平安\u000B2000---...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1441365558</td>
      <td>平安保险黄彩凤</td>
      <td>黄 彩 凤\u000B1997----加盟平安\u000B2000----营业部经理\u00...</td>
    </tr>
    <tr>
      <th>2236811</th>
      <td>1131831</td>
      <td>MzA4MDgyNDg5Ng==</td>
      <td>HereProperty</td>
      <td>NaN</td>
      <td>HERE&amp;nbsp;property(恒远国际地产)是昆士兰房地产投资开发里最具潜力和诚信的...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1435715274</td>
      <td>HereProperty</td>
      <td>HERE property(恒远国际地产)是昆士兰房地产投资开发里最具潜力和诚信的地产销售及...</td>
    </tr>
    <tr>
      <th>1776794</th>
      <td>1661574</td>
      <td>MzAwMzQ5MTU5Nw==</td>
      <td>独立民谣唱作人茶季杨</td>
      <td>NaN</td>
      <td>&amp;nbsp;独立原创民谣歌者。\u000B小时候梦想着当飞行员、篮球明星，可最后却阴错阳差的...</td>
      <td>http://mp.weixin.qq.com/mp/qrcode?scene=100000...</td>
      <td>1439319483</td>
      <td>独立民谣唱作人茶季杨</td>
      <td>独立原创民谣歌者。\u000B小时候梦想着当飞行员、篮球明星，可最后却阴错阳差的抱起了吉他...</td>
    </tr>
  </tbody>
</table>
</div>




```python
clean_biz_long_descriptions.clean_biz_description.values
```




    array(['矗立於連接氹仔島及路環島的金光大道上，澳門四季酒店位置優越、交通四通八達。酒店樓高20層，設360間優雅舒適的客房包括84間套房、5所高級食府、5個室外游泳池、2,000平方米的水療及健身設施、2,500平方米的會議及宴會設施，並與樓高3層薈萃頂級名牌的四季名店及澳門威尼斯人會展中心相連。酒店之中餐廳「紫逸軒」自2009年起被米芝蓮港澳飲食指南評選為兩星米芝蓮餐廳，吸引不少喜愛粵菜的客人慕名而來。',
           '“神农谷”系列产品是宏森科技与广州杨森生物联合研发的一款针对于青春痘，痤疮、粉刺、美白、爽肤五位一体的纯中草药产品套装。“神农谷”享有中国祛痘领先品牌美誉是中国祛痘产品十强企业。产品秉承了中华传统文化中最神奇最悠久的中医药理。原材料全部来自于神秘的神农架大峡谷。“神农谷”品牌借助了神农架的天然资源，是在一个致力于改变肌肤问题，从根本上调理肌肤的全面健康，兼具安全与效果的药妆品牌',
           '黄 彩 凤\\u000B1997----加盟平安\\u000B2000----营业部经理\\u000B所获荣誉：\\u000B全球华人寿险美国MDRT\\u000B百万圆桌荣誉会员\\u000B中国平安人寿辽宁分公司\\u000B钻石年会总会长 \\u000B名人堂总堂主\\u000B鞍山市保险界功勋\\u000B拥有客户600多位\\u000B标准保费780多万\\u000B',
           'HERE property(恒远国际地产)是昆士兰房地产投资开发里最具潜力和诚信的地产销售及投资为一体的公司。我们已经与当地知名开发商、律师所及会计行等组成专家顾问团队, 为您提供高水准和优质房源, 从而让您买房买得放心、安心、舒心。 </signature>\n<reward_wording><![CDATA[',
           ' 独立原创民谣歌者。\\u000B小时候梦想着当飞行员、篮球明星，可最后却阴错阳差的抱起了吉他。\\u000B长大后喜欢读诗，却发现诗里的世界很遥远。\\u000B人，看不见自己的脸，听不清自己的声音。\\u000B别人告诉我，你的歌，让人感动，我不确定。\\u000B烛光摇曳，香炉熏烟，你听得到吗？\\u000B'],
          dtype=object)



There are still some encoding problems with unicode characters like `\u000B`, but I'm going to skip it for now.


```python
biz_description_value_counts = biz.clean_biz_description.value_counts()
biz_description_value_counts.head(10)
```




    服务大众       9057
    中国电信       6937
    感谢关注       6305
    功能介绍       4890
    欢迎您我的客人    3411
    品牌宣传       3086
    欢迎你们       2774
    中国中国       2128
    欢迎光临       1859
    个人理财       1763
    Name: clean_biz_description, dtype: int64



Again, there are some duplicated descriptions


```python
description_translations = [translator.translate(text).text for text in biz_description_value_counts.head(n=10).index]
description_translations
```




    ['to serve the public',
     'China Telecom',
     'Thanks for attention',
     'Features',
     'Welcome to my guest',
     'Branding',
     'welcome',
     'China China',
     'welcome',
     'Personal finance']



Seems like probably some parsing issues.

## `qr_code` and `timestamp`

It seems that I'm not going to need qr_code and timestamp, so I'll skip those.


```python
biz.to_pickle('./biz-parsed.pickle')
```

# Pages Dataset

Let's analyze pages dataset now


```python
!head -n1000 ../wechat_data_medium/weixin_page_test
```

    <URL>http://mp.weixin.qq.com/s?__biz=MjM5OTE5MzE4Mg==&mid=403624174&idx=4&sn=1ba7d9edfd708ae64df7b31b662b1b59&scene=6#rd</URL>
    <TITLE>������&nbsp;|&nbsp;������������������������������������������&nbsp;������������������</TITLE>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        
    
    
    
    
    
    
    
    
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
                
                
                
    
    
    
    
    
    
    
    
    
    
    
    
    
    </BODY>
    <TIME>1454169600</TIME>
    <DATE>2016-01-31</DATE>
    <ID>79020759</ID>
    
    <URL>http://mp.weixin.qq.com/s?__biz=MjM5ODE2Mjk1Nw==&mid=401867851&idx=2&sn=1a7d4f2ea9bf962229c702dfe664522c&scene=6#rd</URL>
    <TITLE>���������������������������������������������������������</TITLE>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        
    
    
    
    
    
    
    
    
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
                
                
                
    
    
    
    
    
    
    
    
    
    
    
    
    
    </BODY>
    <TIME>1454169600</TIME>
    <DATE>2016-01-31</DATE>
    <ID>79020760</ID>
    
    <URL>http://mp.weixin.qq.com/s?__biz=MjM5MTUyMjQ2MA==&mid=402603428&idx=3&sn=affbe2b5c4777ff505ca2a014769f21c&scene=6#rd</URL>
    <TITLE>������������������������������������������������������������������</TITLE>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        
    
    
    
    
    
    
    
    
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    


Looks like this dataset has quite not a standard format, where each "column" is encoded in new line as XML tag, and there is a newline between each record. I'm going to try to parse it by looking at <URL> tag.


```python
with open("../wechat_data_medium/weixin_page_test", encoding='UTF-8') as f:
    pages = f.read()
```


```python
pages = list(map(lambda p: '<URL>' + p, pages.split("<URL>")[1:]))
```


```python
import re

DOCUMENT_PATTERN = re.compile("^<URL>(?P<url>.*)</URL>\n" +
         "<TITLE>(?P<title>.*)</TITLE>\n" +
         "<BODY>(?P<body>.*)</BODY>\n" +
         "<TIME>(?P<time>.*)</TIME>\n" + 
         "<DATE>(?P<date>.*)</DATE>\n" +
         "<ID>(?P<id>.*)</ID>\n+$", flags=re.DOTALL)
```


```python
def parse_document(page):
    m = re.match(DOCUMENT_PATTERN, page)
    if m:
        return m.groupdict()
    else:
        raise Exception("Couldn't parse!")
```


```python
import pandas as pd

pages_df = pd.DataFrame(list(map(parse_document, pages)))
pages_df['date'] = pd.to_datetime(pages_df.date)
pages_df['time'] = pd.to_datetime(pages_df.time, unit='s')
pages_df.sample(10)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>body</th>
      <th>date</th>
      <th>id</th>
      <th>time</th>
      <th>title</th>
      <th>url</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>11373</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79032132</td>
      <td>2016-01-30 19:23:17</td>
      <td>城区划定四处烟花爆竹临时经营区</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzA5MzM5Mjg5MA...</td>
    </tr>
    <tr>
      <th>2442</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79023201</td>
      <td>2016-01-30 16:14:42</td>
      <td>人人都有痛苦，宽容就是忘却</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5OTEyODA2Mw...</td>
    </tr>
    <tr>
      <th>8710</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79029469</td>
      <td>2016-01-30 18:22:53</td>
      <td>小学应用题宝典！类型归纳+解题思路+例题整理，收藏！</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5Mjg1MDQ0OQ...</td>
    </tr>
    <tr>
      <th>628</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79021387</td>
      <td>2016-01-30 16:02:33</td>
      <td>一分钟读心术，让你一秒成为心理专家！</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzA3NjQ0Njk0MA...</td>
    </tr>
    <tr>
      <th>192</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020951</td>
      <td>2016-01-30 16:00:47</td>
      <td>临近年关各行涨声四起，看看大嵊州哪些行业必涨价！</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzA5NzA3OTQzMA...</td>
    </tr>
    <tr>
      <th>9341</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79030100</td>
      <td>2016-01-30 18:37:34</td>
      <td>重庆方言&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;...</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzAxNTUzOTU3Nw...</td>
    </tr>
    <tr>
      <th>13604</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79034363</td>
      <td>2016-01-30 20:17:28</td>
      <td>春节出游、回家不堵车，可以走这条高速！</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzA3MzI5NzQyNQ...</td>
    </tr>
    <tr>
      <th>1827</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79022586</td>
      <td>2016-01-30 16:09:38</td>
      <td>见过豪车，见过那么豪华的摩托车吗？！</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzI5NzA5ODIzOQ...</td>
    </tr>
    <tr>
      <th>9153</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79029912</td>
      <td>2016-01-30 18:33:28</td>
      <td>好校长必须具备的管理意识</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzAxMTAzNTEzNA...</td>
    </tr>
    <tr>
      <th>12632</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79033391</td>
      <td>2016-01-30 19:52:18</td>
      <td>上海名媛，她如何美了一世！</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5ODAzODgwMg...</td>
    </tr>
  </tbody>
</table>
</div>



Let's run a quick EDA on body, title, date and url


```python
pages_df.title.str.len().describe()
```




    count    13796.000000
    mean        22.369092
    std         10.584357
    min          2.000000
    25%         16.000000
    50%         21.000000
    75%         26.000000
    max        292.000000
    Name: title, dtype: float64




```python
pages_df[pages_df.title.str.len() > 50].head(5)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>body</th>
      <th>date</th>
      <th>id</th>
      <th>time</th>
      <th>title</th>
      <th>url</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>45</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020804</td>
      <td>2016-01-30 16:00:11</td>
      <td>【福利】招财狗：0撸无限，填写邀请码得2元，邀请一个2元，每天签到2元，可充电话费、提现银行...</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzA3NTI3OTA4Mw...</td>
    </tr>
    <tr>
      <th>49</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020808</td>
      <td>2016-01-30 16:00:11</td>
      <td>【居家首选：民丰小区】&amp;nbsp;110㎡&amp;nbsp;&amp;nbsp;三室二厅&amp;nbsp;精装修...</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzAxNTYxNjE1Nw...</td>
    </tr>
    <tr>
      <th>60</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020819</td>
      <td>2016-01-30 16:00:14</td>
      <td>西班牙鞋履设计大师&amp;nbsp;Manolo&amp;nbsp;Blahnik&amp;nbsp;纪录片《Ma...</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5MTYwNjA0MQ...</td>
    </tr>
    <tr>
      <th>146</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020905</td>
      <td>2016-01-30 16:00:36</td>
      <td>湘乡时政︱市政府党组召开“三严三实”专题民主生活会&amp;nbsp;&amp;nbsp;;市关工委受助学生...</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzA4MjYyNzYzNg...</td>
    </tr>
    <tr>
      <th>206</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020965</td>
      <td>2016-01-30 16:00:50</td>
      <td>【V-BAR】V-BAR&amp;nbsp;2周年庆典盛宴香港明星袁洁仪小姐、婚纱主题Party现场...</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzA4NjIxNDkzOQ...</td>
    </tr>
  </tbody>
</table>
</div>




```python
pages_df['clean_title'] = pages_df.title.map(double_unescape_html_text)
```


```python
pages_df.clean_title.str.len().describe()
```




    count    13796.000000
    mean        21.078718
    std          8.381656
    min          2.000000
    25%         15.000000
    50%         20.000000
    75%         25.000000
    max         70.000000
    Name: clean_title, dtype: float64




```python
pages_df.clean_title.isnull().sum()
```




    0




```python
%matplotlib inline

pages_df.time.describe()
```




    count                   13796
    unique                   4575
    top       2016-01-30 18:13:38
    freq                       26
    first     2016-01-30 16:00:00
    last      2016-01-30 20:21:40
    Name: time, dtype: object




```python
pages_df.date.describe()
```




    count                   13796
    unique                      1
    top       2016-01-31 00:00:00
    freq                    13796
    first     2016-01-31 00:00:00
    last      2016-01-31 00:00:00
    Name: date, dtype: object




```python
pages_df.url.sample(10).values
```




    array(['http://mp.weixin.qq.com/s?__biz=MzA4NDU5Nzg3NQ==&mid=401914571&idx=7&sn=bf3e1ef6cd4be1edfc7d3cd16ffc50f9&scene=6#rd',
           'http://mp.weixin.qq.com/s?__biz=MzA4NTMxODg5Mg==&mid=402467678&idx=2&sn=831c15deba1b4cbbf65ad3dbde14e650&scene=6#rd',
           'http://mp.weixin.qq.com/s?__biz=MzA4MDYyNzQyNg==&mid=401291030&idx=4&sn=ab0c70a1d9107fd98e67cd975e393d66&scene=6#rd',
           'http://mp.weixin.qq.com/s?__biz=MzAwOTEwMjA4Nw==&mid=401635961&idx=6&sn=4645f37cdca14399deb5d1f3ee38a235&scene=6#rd',
           'http://mp.weixin.qq.com/s?__biz=MzAxOTY5OTEyMg==&mid=417691203&idx=6&sn=7073a8099f3e3966064e7ce37e10da84&scene=6#rd',
           'http://mp.weixin.qq.com/s?__biz=MzAwMDI2MjU1Ng==&mid=401890372&idx=3&sn=f5a29703cfb88f35f57decd641c4665b&scene=6#rd',
           'http://mp.weixin.qq.com/s?__biz=MzA4MzcxNTcwMg==&mid=403280062&idx=1&sn=367345f872f8a98dea7d2942aa49f8e8&scene=6#rd',
           'http://mp.weixin.qq.com/s?__biz=MzAxNzE2MzAyMg==&mid=401182824&idx=3&sn=d65f89170dba204126c2df12f560b462&scene=6#rd',
           'http://mp.weixin.qq.com/s?__biz=MjM5OTk5MzY0Mw==&mid=401850843&idx=4&sn=7aa6a180f58904188ddeb0ed402c2cd7&scene=6#rd',
           'http://mp.weixin.qq.com/s?__biz=MjM5ODgyMTk1Nw==&mid=401733005&idx=3&sn=38ad1f312196c1354c21a7fe17f71322&scene=6#rd'],
          dtype=object)



Let's extract biz from URL


```python
ARTICLE_URL_PATTERN = r"^http://.*?__biz=(?P<biz>.*)&mid.*$"
```


```python
def extract_biz_from_url(url):
    m = re.match(ARTICLE_URL_PATTERN, url)
    if m:
        return m.groupdict()['biz']
    else:
        raise Exception("Incorrect URL!")
        

```


```python
pages_df['biz_id'] = pages_df.url.map(extract_biz_from_url)
pages_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>body</th>
      <th>date</th>
      <th>id</th>
      <th>time</th>
      <th>title</th>
      <th>url</th>
      <th>clean_title</th>
      <th>biz_id</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020759</td>
      <td>2016-01-30 16:00:00</td>
      <td>关注&amp;nbsp;|&amp;nbsp;“寨卡”病毒中南美洲迅速蔓延&amp;nbsp;巴西蒙上阴影</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5OTE5MzE4Mg...</td>
      <td>关注 | “寨卡”病毒中南美洲迅速蔓延 巴西蒙上阴影</td>
      <td>MjM5OTE5MzE4Mg==</td>
    </tr>
    <tr>
      <th>1</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020760</td>
      <td>2016-01-30 16:00:00</td>
      <td>㊙身为一个小姐，不能有感觉了再接客……</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5ODE2Mjk1Nw...</td>
      <td>㊙身为一个小姐，不能有感觉了再接客……</td>
      <td>MjM5ODE2Mjk1Nw==</td>
    </tr>
    <tr>
      <th>2</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020761</td>
      <td>2016-01-30 16:00:00</td>
      <td>两次战败，被揍的稀巴烂的德国为何能迅速崛起？</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5MTUyMjQ2MA...</td>
      <td>两次战败，被揍的稀巴烂的德国为何能迅速崛起？</td>
      <td>MjM5MTUyMjQ2MA==</td>
    </tr>
    <tr>
      <th>3</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020762</td>
      <td>2016-01-30 16:00:00</td>
      <td>一个老炮儿的无奈抗争</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzA3MDU3MDc2Mw...</td>
      <td>一个老炮儿的无奈抗争</td>
      <td>MzA3MDU3MDc2Mw==</td>
    </tr>
    <tr>
      <th>4</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020763</td>
      <td>2016-01-30 16:00:00</td>
      <td>秘一个女子该有的精致生活</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzA3MzUwOTQwNg...</td>
      <td>秘一个女子该有的精致生活</td>
      <td>MzA3MzUwOTQwNg==</td>
    </tr>
  </tbody>
</table>
</div>



Let's check the distribution of extracted `biz_id` to understand if there is any skeweness


```python
pages_df.biz_id.value_counts().head(10)
```




    MjM5NjE5MTY1Mg==    8
    MzAxNDA2MDQxMw==    7
    MzA4ODMwMjEzNg==    7
    MzAwNDAzNDI3OA==    7
    MzA4ODI4OTczNQ==    7
    MjM5NjcyNTc2NA==    7
    MzA3NDkxNDIyMQ==    6
    MjM5Njk0OTUyMg==    6
    MjM5MzYwNDY4MQ==    6
    MzA5Njc2MzEzNw==    6
    Name: biz_id, dtype: int64



It looks like there is little skeweness. That's good.

Let's check if every biz_id is in the biz dataset


```python
(~pages_df.biz_id.isin(biz.biz_id)).sum()
```




    961



Looks like 961 records don't match. Why is that?


```python
pages_missing_biz_ids = pages_df[~pages_df.biz_id.isin(biz.biz_id)]
pages_missing_biz_ids.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>body</th>
      <th>date</th>
      <th>id</th>
      <th>time</th>
      <th>title</th>
      <th>url</th>
      <th>clean_title</th>
      <th>biz_id</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>6</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020765</td>
      <td>2016-01-30 16:00:01</td>
      <td>说说德国教育牛在哪里？（好文推荐）</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5OTc1NTU5OQ...</td>
      <td>说说德国教育牛在哪里？（好文推荐）</td>
      <td>MjM5OTc1NTU5OQ==</td>
    </tr>
    <tr>
      <th>13</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020772</td>
      <td>2016-01-30 16:00:03</td>
      <td>【大话青县·精选】出售帝豪小区楼房</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5NTM5NTIxMQ...</td>
      <td>【大话青县·精选】出售帝豪小区楼房</td>
      <td>MjM5NTM5NTIxMQ==</td>
    </tr>
    <tr>
      <th>30</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020789</td>
      <td>2016-01-30 16:00:07</td>
      <td>【关注】呼市一小区居民家中着火</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5NDY5MDgyOQ...</td>
      <td>【关注】呼市一小区居民家中着火</td>
      <td>MjM5NDY5MDgyOQ==</td>
    </tr>
    <tr>
      <th>46</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020805</td>
      <td>2016-01-30 16:00:11</td>
      <td>?马云大军出动，迅速占领日本！</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzA3MTY3OTgzOA...</td>
      <td>?马云大军出动，迅速占领日本！</td>
      <td>MzA3MTY3OTgzOA==</td>
    </tr>
    <tr>
      <th>56</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020815</td>
      <td>2016-01-30 16:00:14</td>
      <td>红木行业一片惨烈&amp;nbsp;为何她却快速开店增长？</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5ODU0Njc2MQ...</td>
      <td>红木行业一片惨烈 为何她却快速开店增长？</td>
      <td>MjM5ODU0Njc2MQ==</td>
    </tr>
  </tbody>
</table>
</div>




```python
pages_missing_biz_ids.biz_id.value_counts().head()
```




    MjM5NDMzNTk2MA==    6
    MjM5NzI3MTM3OA==    6
    MzA3MzMzNDk2NA==    6
    MjM5MzYwNDY4MQ==    6
    MjM5Njc3MzU2MQ==    5
    Name: biz_id, dtype: int64




```python
biz[biz.biz_id.fillna('').str.contains('MjM5OTc1NTU5OQ==')]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>biz_id</th>
      <th>biz_name</th>
      <th>biz_code</th>
      <th>biz_description</th>
      <th>qr_code</th>
      <th>timestamp</th>
      <th>clean_biz_name</th>
      <th>clean_biz_description</th>
    </tr>
  </thead>
  <tbody>
  </tbody>
</table>
</div>



Let's leave it for now

> Within the content is a javascript variable “ct” that denotes publish time

Let's check it


```python
def show_text_around_var_ct(text):
    i = text.index("var ct")
    return text[i-10:i+50]

for i in range(10):
    print(show_text_around_var_ct(pages_df.body.sample(10).iloc[i]))
```

    9";
          var ct = "1454158835";
          var user_name = "gh_2
    9";
          var ct = "1454162330";
          var user_name = "gh_6
    9";
          var ct = "1454137078";
          var user_name = "gh_2
    9";
          var ct = "1454141418";
          var user_name = "gh_c
    9";
          var ct = "1454169632";
          var user_name = "gh_7
    9";
          var ct = "1454170097";
          var user_name = "gh_2
    9";
          var ct = "1454161089";
          var user_name = "gh_6
    9";
          var ct = "1454169693";
          var user_name = "gh_f
    9";
          var ct = "1454167188";
          var user_name = "gh_f
    9";
          var ct = "1454170632";
          var user_name = "gh_3


Looks like we can create a regex for extracting this timestamp


```python
PUBLISH_TIMESTAMP_PATTERN = r"var ct = \"(?P<ct>\d+)\";"

def extract_publish_time_from_body(body):
    cts = re.findall(PUBLISH_TIMESTAMP_PATTERN, body)
    if len(cts) != 1:
        raise Exception("Incorrect number of publish times in body!")
    return int(cts[0])
```


```python
pages_df['publish_time'] = pd.to_datetime(pages_df.body.map(extract_publish_time_from_body), unit='s')
```


```python
pages_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>body</th>
      <th>date</th>
      <th>id</th>
      <th>time</th>
      <th>title</th>
      <th>url</th>
      <th>clean_title</th>
      <th>biz_id</th>
      <th>publish_time</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020759</td>
      <td>2016-01-30 16:00:00</td>
      <td>关注&amp;nbsp;|&amp;nbsp;“寨卡”病毒中南美洲迅速蔓延&amp;nbsp;巴西蒙上阴影</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5OTE5MzE4Mg...</td>
      <td>关注 | “寨卡”病毒中南美洲迅速蔓延 巴西蒙上阴影</td>
      <td>MjM5OTE5MzE4Mg==</td>
      <td>2016-01-30 15:01:21</td>
    </tr>
    <tr>
      <th>1</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020760</td>
      <td>2016-01-30 16:00:00</td>
      <td>㊙身为一个小姐，不能有感觉了再接客……</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5ODE2Mjk1Nw...</td>
      <td>㊙身为一个小姐，不能有感觉了再接客……</td>
      <td>MjM5ODE2Mjk1Nw==</td>
      <td>2016-01-30 15:11:35</td>
    </tr>
    <tr>
      <th>2</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020761</td>
      <td>2016-01-30 16:00:00</td>
      <td>两次战败，被揍的稀巴烂的德国为何能迅速崛起？</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5MTUyMjQ2MA...</td>
      <td>两次战败，被揍的稀巴烂的德国为何能迅速崛起？</td>
      <td>MjM5MTUyMjQ2MA==</td>
      <td>2016-01-30 15:12:37</td>
    </tr>
    <tr>
      <th>3</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020762</td>
      <td>2016-01-30 16:00:00</td>
      <td>一个老炮儿的无奈抗争</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzA3MDU3MDc2Mw...</td>
      <td>一个老炮儿的无奈抗争</td>
      <td>MzA3MDU3MDc2Mw==</td>
      <td>2016-01-30 15:12:25</td>
    </tr>
    <tr>
      <th>4</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020763</td>
      <td>2016-01-30 16:00:00</td>
      <td>秘一个女子该有的精致生活</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzA3MzUwOTQwNg...</td>
      <td>秘一个女子该有的精致生活</td>
      <td>MzA3MzUwOTQwNg==</td>
      <td>2016-01-30 15:11:05</td>
    </tr>
  </tbody>
</table>
</div>




```python
pages_df.publish_time.describe()
```




    count                   13796
    unique                   7774
    top       2016-01-30 16:00:32
    freq                       28
    first     2016-01-29 16:20:30
    last      2016-01-30 19:51:46
    Name: publish_time, dtype: object




```python
pages_df.url.value_counts().head()
```




    http://mp.weixin.qq.com/s?__biz=MzAwNDM1MTY3OA==&mid=402063769&idx=1&sn=66bb4c4a4b0c3587f5b6bfd6d11f8433&scene=6#rd    1
    http://mp.weixin.qq.com/s?__biz=MzAwMjI0NTM0Mg==&mid=403058454&idx=2&sn=4c23f8ec05a900f44e21df013bc1e05e&scene=6#rd    1
    http://mp.weixin.qq.com/s?__biz=MzA5OTA0NDc2MA==&mid=401920276&idx=5&sn=045491f7317bca20c7ae86774dc0e6b2&scene=6#rd    1
    http://mp.weixin.qq.com/s?__biz=MzA5NTk3NzczMQ==&mid=401425375&idx=2&sn=1a7c54f766496785f0a6d938f38727c9&scene=6#rd    1
    http://mp.weixin.qq.com/s?__biz=MzA5NTEyMjIzOA==&mid=401144404&idx=1&sn=5b6458d74cea8f870880adc791267337&scene=6#rd    1
    Name: url, dtype: int64



### Extract content without HTML


```python
from bs4 import BeautifulSoup
soup = BeautifulSoup(pages_df.body.iloc[0])
soup
```

    /Users/mateusz/.virtualenvs/deep-learning-with-python/lib/python3.6/site-packages/bs4/__init__.py:181: UserWarning: No parser was explicitly specified, so I'm using the best available HTML parser for this system ("lxml"). This usually isn't a problem, but if you run this code on another system, or in a different virtual environment, it may use a different parser and behave differently.
    
    The code that caused this warning is on line 193 of the file /usr/local/Cellar/python3/3.6.1/Frameworks/Python.framework/Versions/3.6/lib/python3.6/runpy.py. To get rid of this warning, change code that looks like this:
    
     BeautifulSoup(YOUR_MARKUP})
    
    to this:
    
     BeautifulSoup(YOUR_MARKUP, "lxml")
    
      markup_type=markup_type))





    <!DOCTYPE html>
    <html>
    <head>
    <script type="text/javascript">
                window.logs = {
                    pagetime: {}
                };
                window.logs.pagetime['html_begin'] = (+new Date());
            </script>
    <script type="text/javascript"> 
        var page_begintime = (+new Date());
    
        var biz = "MjM5OTE5MzE4Mg==";
        var sn = "1ba7d9edfd708ae64df7b31b662b1b59" || "";
        var mid = "403624174" || "";
        var idx = "4" || "" ;
        
        
        var is_rumor = ""*1;
        var norumor = ""*1;
        if (!!is_rumor&&!norumor){
          if (!document.referrer || document.referrer.indexOf("mp.weixin.qq.com/mp/rumor") == -1){
            location.href = "http://mp.weixin.qq.com/mp/rumor?action=info&__biz=" + biz + "&mid=" + mid + "&idx=" + idx + "&sn=" + sn + "#wechat_redirect";
          }
        }
    
        
        
    </script>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" name="viewport"/>
    <link href="//res.wx.qq.com" rel="dns-prefetch"/>
    <link href="//mmbiz.qpic.cn" rel="dns-prefetch"/>
    <link href="http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/images/icon/common/favicon22c41b.ico" rel="shortcut icon" type="image/x-icon"/>
    <meta content="yes" name="apple-mobile-web-app-capable"/>
    <meta content="black" name="apple-mobile-web-app-status-bar-style"/>
    <meta content="telephone=no" name="format-detection"/>
    <script type="text/javascript">
        String.prototype.html = function(encode) {
            var replace =["&#39;", "'", "&quot;", '"', "&nbsp;", " ", "&gt;", ">", "&lt;", "<", "&amp;", "&", "&yen;", "¥"];
            if (encode) {
                replace.reverse();
            }
            for (var i=0,str=this;i< replace.length;i+= 2) {
                 str=str.replace(new RegExp(replace[i],'g'),replace[i+1]);
            }
            return str;
        };
    
        window.isInWeixinApp = function() {
            return /MicroMessenger/.test(navigator.userAgent);
        };
    
        window.getQueryFromURL = function(url) {
            url = url || 'http://qq.com/s?a=b#rd'; // 做一层保护，保证URL是合法的
            var query = url.split('?')[1].split('#')[0].split('&'),
                params = {};
            for (var i=0; i<query.length; i++) {
                var arg = query[i].split('=');
                params[arg[0]] = arg[1];
            }
            if (params['pass_ticket']) {
            	params['pass_ticket'] = encodeURIComponent(params['pass_ticket'].html(false).html(false).replace(/\s/g,"+"));
            }
            return params;
        };
    
        (function() {
    	    var params = getQueryFromURL(location.href);
            window.uin = params['uin'] || '';
            window.key = params['key'] || '';
            window.wxtoken = params['wxtoken'] || '';
            window.pass_ticket = params['pass_ticket'] || '';
        })();
    
    </script>
    <title>关注 | “寨卡”病毒中南美洲迅速蔓延 巴西蒙上阴影</title>
    <link href="http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/style/page/appmsg/page_mp_article_improve2a7a3f.css" rel="stylesheet" type="text/css"/>
    <style>
    </style>
    <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/style/page/appmsg/page_mp_article_improve_pc2a7a3f.css">
    <![endif]-->
    <script type="text/javascript">
        document.domain = "qq.com";
    </script>
    </head>
    <body class="zh_CN mm_appmsg" id="activity-detail" ontouchstart="">
    <script type="text/javascript">
            var write_sceen_time = (+new Date());
        </script>
    <div class="discuss_container editing access" id="js_cmt_mine" style="display:none;">
    <div class="discuss_container_inner">
    <h2 class="rich_media_title">关注 | “寨卡”病毒中南美洲迅速蔓延 巴西蒙上阴影</h2>
    <span id="log"></span>
    <div class="frm_textarea_box_wrp">
    <span class="frm_textarea_box">
    <textarea class="frm_textarea" id="js_cmt_input" placeholder="留言将由公众号筛选后显示，对所有人可见。"></textarea>
    <div class="emotion_tool">
    <span class="emotion_switch" style="display:none;"></span>
    <span class="pic_emotion_switch_wrp" id="js_emotion_switch">
    <img alt="" class="pic_default" src="http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/images/icon/appmsg/emotion/icon_emotion_switch.2x278965.png"/>
    <img alt="" class="pic_active" src="http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/images/icon/appmsg/emotion/icon_emotion_switch_active.2x278965.png"/>
    </span>
    <div class="emotion_panel" id="js_emotion_panel">
    <span class="emotion_panel_arrow_wrp" id="js_emotion_panel_arrow_wrp">
    <i class="emotion_panel_arrow arrow_out"></i>
    <i class="emotion_panel_arrow arrow_in"></i>
    </span>
    <div class="emotion_list_wrp" id="js_slide_wrapper">
    </div>
    <ul class="emotion_navs" id="js_navbar">
    </ul>
    </div>
    </div>
    </span>
    </div>
    <div class="discuss_btn_wrp"><a class="btn btn_primary btn_discuss btn_disabled" href="javascript:;" id="js_cmt_submit">提交</a></div>
    <div class="discuss_list_wrp" style="display:none">
    <div class="rich_tips with_line title_tips discuss_title_line">
    <span class="tips">我的留言</span>
    </div>
    <ul class="discuss_list" id="js_cmt_mylist"></ul>
    </div>
    <div class="rich_tips tips_global loading_tips" id="js_mycmt_loading">
    <img alt="" class="rich_icon icon_loading_white" src="http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/images/icon/common/icon_loading_white2805ea.gif"/>
    <span class="tips">加载中</span>
    </div>
    <div class="wx_poptips" id="js_cmt_toast" style="display:none;">
    <img alt="" class="icon_toast" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGoAAABqCAYAAABUIcSXAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3NpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDoyMTUxMzkxZS1jYWVhLTRmZTMtYTY2NS0xNTRkNDJiOGQyMWIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MTA3QzM2RTg3N0UwMTFFNEIzQURGMTQzNzQzMDAxQTUiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MTA3QzM2RTc3N0UwMTFFNEIzQURGMTQzNzQzMDAxQTUiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NWMyOGVjZTMtNzllZS00ODlhLWIxZTYtYzNmM2RjNzg2YjI2IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjIxNTEzOTFlLWNhZWEtNGZlMy1hNjY1LTE1NGQ0MmI4ZDIxYiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pmvxj1gAAAVrSURBVHja7J15rF1TFMbXk74q1ZKHGlMkJVIhIgg1FH+YEpEQJCKmGBpThRoSs5jVVNrSQUvEEENIhGiiNf9BiERICCFIRbUiDa2qvudbOetF3Tzv7XWGffa55/uS7593977n3vO7e5+199p7v56BgQGh0tcmvAUERREUQVEERREUQVEERREUQVEERREUQVEERREUQVEERREUQVEERVAUQVEERVAUQbVYk+HdvZVG8b5F0xj4RvhouB+eCy8KrdzDJc1RtAX8ILxvx98V1GyCSkN98Cx4z/95/Wn4fj6j6tUEeN4wkFSnw1MJqj5NhBfAuwaUHREUg4lqNMmePVsHll/HFhVfe1t3FwpJI8DXCCquDrCWNN4B6Tb4M3Z98aTPmTvh0YHl18PXw29yZiKejoPvcUD6E74yFBJbVDk6Bb7K8aP/Hb4c/tRzEYIqprPhSxzlf4Uvhb/0Xoig8qnHAJ3lqPMzfDH8XZ4LEpRf2sVdA5/sqPO9Qfop70UJyn+/boaPddT5yrq7VUUvTIVJI7q74MMddXR8NB1eXcYvhBpZm0s2w72/o86HFoKvLau/pYaXzjLMdUJ6y0LwtWV9CIIaXtvA8+G9HHV03u5q+K+yH47U0NoRngPv7KjzHDwTLj0bS1BDazfJJlcnOOostC6ysnCT+q80G/sIvFVgeW09D8FPVT0uoP7VfvAD8NjA8pqmuAN+OcYAjso0RbIZ8DGB5TVNcRO8JMaHY9SXSdfa3eeANJimWBLrA7JFiZwIXye+NMUV8CcxP2SRFjXefok7NRjSGZJlWUPvw2/wtNiQirSoXWyMsR28wR7AzzYM0oXw+Y7yK+CLJGeaoqjyrJSdZJD6Ov4+z5y6NJc0Az7NUecHydIUy+v60KNyQHoM3nKI1y7YCFiq0i7uBvgER52vDdKqWn9djhY1Dn4G3n6Ecqm2rF74dvgoR53S0hQxW9RJAZAGW5bSn58QJA27dQ7uIEedjywEX5NKVxCqsY6y+qA+LxFI4+yZ6oH0trWkNan80jygtIUsc5SflgAsDXgehfdx1KkkTRE76tN+Xue2jnTU0Ru1oIbvpt30bBtKhOp5yaaRkts0lic8V1i6dPcIRx2d/l8Y8XtNNEg7OOo8bl1kmmOKnDsO88CaYzejau0hWZqiL7C83oCH4SeTHvwV2BqqsHRVztSEYOmWF80NeXZT6Hd4KflResE9vCnBOlCyGfDNAstHTVPUDWoQ1t3iW+9WNizvlhfd4aerXd+ThqiMfNR6+9LvOOro5OY5JX2H4+F7HZD+kGzlamMgldWiirQsjcwWFbjmqZJteekJLK9pisvgL6RhKvuciZiwzrWWGapfrPy30kBVcSBIrw0aD3PU0XB6cehntq7rTMf7/2iQlktDVdXJLXlg6VjmiYBn6rWSTRCH6hvJ0hQrpcGq8oidsmHpTP8t8DGO9/vcWt9qabiqPgup1yKyQwvC2tSefZ73SSpNkUJ4PlLorlHZ+446nc8f3fIyywlJhwrTuwVSjBa1ccvSxN0hjjoK5xVrYZMd9V6XbFfgBukixTwGLg8sDam3dZR/wZ6L/dJlin1en8LS+bgpFbz3Ygvzu1J1HKxYNqxGpCmaCEo12rrBorD6LRp8UbpcdR5VWhTW35KlKd6QFqjuM2XzwlpnMxTvSkuUwuG/Xlg6NtPjbT6WFimF/VG6LEvXgn8QGDjMbBukVECFwhpoS+CQatfX2Q1q6H7wENHdrfCr0lKleEB9JyxNneus+VJpsVL9TwI6W65LovWIGl3KtVJaLv7LBwYTFEERFEVQFEERFEVQFEERFEVQFEERFEVQFEERFEVQFEERFFWq/hFgADUMN4RzT6/OAAAAAElFTkSuQmCC"/>
    <p class="toast_content">已留言</p>
    </div>
    </div>
    </div>
    <div class="rich_media" id="js_article">
    <div class="top_banner" id="js_top_ad_area">
    </div>
    <div class="rich_media_inner">
    <div id="page-content">
    <div class="rich_media_area_primary" id="img-content">
    <h2 class="rich_media_title" id="activity-name">
                            关注 | “寨卡”病毒中南美洲迅速蔓延 巴西蒙上阴影 
                        </h2>
    <div class="rich_media_meta_list">
    <em class="rich_media_meta rich_media_meta_text" id="post-date">2016-01-30</em>
    <a class="rich_media_meta rich_media_meta_link rich_media_meta_nickname" href="javascript:void(0);" id="post-user">法国侨报</a>
    <span class="rich_media_meta rich_media_meta_text rich_media_meta_nickname">法国侨报</span>
    <div class="profile_container" id="js_profile_qrcode" style="display:none;">
    <div class="profile_inner">
    <strong class="profile_nickname">法国侨报</strong>
    <img alt="" class="profile_avatar" id="js_profile_qrcode_img" src=""/>
    <p class="profile_meta">
    <label class="profile_meta_label">微信号</label>
    <span class="profile_meta_value">ecmdpress</span>
    </p>
    <p class="profile_meta">
    <label class="profile_meta_label">功能介绍</label>
    <span class="profile_meta_value">关注《法国侨报》微信平台，即时获取您身边最新、最快的新闻时事，抢先知晓华侨华人动态。侨报竭力为您服务，关注世界，关注法国，关注华侨华人，关注《法国侨报》。</span>
    </p>
    </div>
    <span class="profile_arrow_wrp" id="js_profile_arrow_wrp">
    <i class="profile_arrow arrow_out"></i>
    <i class="profile_arrow arrow_in"></i>
    </span>
    </div>
    </div>
    <div class="rich_media_content " id="js_content">
    <p><strong style="max-width: 100%; font-size: 18px; line-height: 25.6px; text-align: justify; box-sizing: border-box !important; word-wrap: break-word !important; background-color: rgb(255, 255, 255);"><span style="max-width: 100%; text-align: center; color: rgb(84, 141, 212); font-size: 14px; box-sizing: border-box !important; word-wrap: break-word !important;">请点右上方“</span><strong style="max-width: 100%; text-align: center; color: rgb(84, 141, 212); font-size: 14px; box-sizing: border-box !important; word-wrap: break-word !important;"><span style="max-width: 100%; color: rgb(0, 0, 0); box-sizing: border-box !important; word-wrap: break-word !important;">…”，</span></strong><span style="max-width: 100%; text-align: center; color: rgb(84, 141, 212); font-size: 14px; box-sizing: border-box !important; word-wrap: break-word !important;">点击“</span><strong style="max-width: 100%; text-align: center; color: rgb(84, 141, 212); font-size: 14px; box-sizing: border-box !important; word-wrap: break-word !important;"><span style="max-width: 100%; color: rgb(0, 0, 0); box-sizing: border-box !important; word-wrap: break-word !important;">发送给朋友</span></strong><span style="max-width: 100%; text-align: center; color: rgb(84, 141, 212); font-size: 14px; box-sizing: border-box !important; word-wrap: break-word !important;">”或“</span><strong style="max-width: 100%; text-align: center; color: rgb(84, 141, 212); font-size: 14px; box-sizing: border-box !important; word-wrap: break-word !important;"><span style="max-width: 100%; color: rgb(0, 0, 0); box-sizing: border-box !important; word-wrap: break-word !important;">分享到朋友圈</span></strong><span style="max-width: 100%; text-align: center; color: rgb(84, 141, 212); font-size: 14px; box-sizing: border-box !important; word-wrap: break-word !important;">”，分享法国生活、巴黎时尚</span></strong></p><p style="max-width: 100%; min-height: 1em; line-height: 23.2727px; box-sizing: border-box !important; word-wrap: break-word !important;"><strong style="max-width: 100%; font-size: 18px; line-height: 25.6px; text-align: justify; box-sizing: border-box !important; word-wrap: break-word !important; background-color: rgb(255, 255, 255);"><span style="max-width: 100%; text-align: center; color: rgb(84, 141, 212); font-size: 14px; box-sizing: border-box !important; word-wrap: break-word !important;"><br/></span></strong></p><p style="max-width: 100%; min-height: 1em; line-height: 23.2727px; box-sizing: border-box !important; word-wrap: break-word !important; text-align: center;"><strong style="max-width: 100%; font-size: 18px; line-height: 25.6px; text-align: justify; box-sizing: border-box !important; word-wrap: break-word !important; background-color: rgb(255, 255, 255);"><span style="max-width: 100%; text-align: center; color: rgb(84, 141, 212); font-size: 14px; box-sizing: border-box !important; word-wrap: break-word !important;"><img data-ratio="0.8007968127490039" data-s="300,640" data-src="http://mmbiz.qpic.cn/mmbiz/S7rG6oF8SF6npbiazs3SSicyWEhgM0HGUaK6MiaaCjNby1jibNvFVsialM2YR6Po9g3JsYr7To6o2k4Y6MLR5Y8XT8Q/0?wx_fmt=jpeg" data-type="jpeg" data-w="251"/><br/></span></strong></p><p style="max-width: 100%; min-height: 1em; line-height: 23.2727px; box-sizing: border-box !important; word-wrap: break-word !important; text-align: center;"><strong style="max-width: 100%; font-size: 18px; line-height: 25.6px; text-align: justify; box-sizing: border-box !important; word-wrap: break-word !important; background-color: rgb(255, 255, 255);"><span style="max-width: 100%; text-align: center; color: rgb(84, 141, 212); font-size: 14px; box-sizing: border-box !important; word-wrap: break-word !important;"><br/></span></strong></p><p style="border: 0px; font-family: 宋体; font-size: 13px; line-height: 26px; text-indent: 26px; white-space: normal; background-color: rgb(255, 255, 255);"><span style="font-size: 18px;"></span></p><p>1月29日消息，“寨卡热”疫情在中南美洲迅速蔓延，欧洲多国也相继出现确诊个案。世界卫生组织(WHO)28日举行特别会议，总干事陈冯富珍指，寨卡病毒正在“爆炸性扩散”，可能会在美洲感染300万至400万人。她表示，尽管未确切证明寨卡热与初生婴儿小头症有关，但“风险水平极高”。</p><p><br/></p><p><img data-ratio="0.5683453237410072" data-s="300,640" data-src="http://mmbiz.qpic.cn/mmbiz/S7rG6oF8SF6npbiazs3SSicyWEhgM0HGUaYnQZdxNSl6bF5LUu5knIPrhUN9YsWbpO547xhUyxtt2M4wtSqtA8aA/0?wx_fmt=jpeg" data-type="jpeg" data-w="" style="line-height: 1.6;"/><span style="line-height: 1.6;">                                                                                               </span></p><p style="text-align: right;"><strong><span style="line-height: 1.6;">据中新网报道</span></strong><br/></p><p style="max-width: 100%; min-height: 1em; white-space: pre-wrap; box-sizing: border-box !important; word-wrap: break-word !important;"><br/></p><section class="tn-Powered-by-XIUMI" style="max-width: 100%; line-height: 25.6px; font-size: 18px; border: 0px; box-sizing: border-box !important; word-wrap: break-word !important; background-color: rgb(255, 255, 255);"><section class="tn-Powered-by-XIUMI" style="margin-top: 2px; margin-bottom: 0.5em; max-width: 100%; border-top-style: solid; border-top-width: 2px; border-color: rgb(249, 110, 87); text-align: center; box-sizing: border-box !important; word-wrap: break-word !important;"><span style="max-width: 100%; color: rgb(127, 127, 127); font-size: 14px; line-height: 25.6px; box-sizing: border-box !important; word-wrap: break-word !important;">法国侨网推广</span></section></section><p><img data-ratio="0.6438848920863309" data-s="300,640" data-src="http://mmbiz.qpic.cn/mmbiz/S7rG6oF8SF5E6pzW3oO53UZ2ib1ibDaIfgksw2jKiaY8KDZdtgjthVXBhcuzI7qv6iaXnygBdtlvrJg2OIDGCPtA9A/640?wx_fmt=jpeg" data-type="jpeg" data-w="" style="color: rgb(62, 62, 62); font-size: 18px; line-height: 23.2727px; text-align: center; box-sizing: border-box !important; word-wrap: break-word !important; width: auto !important; visibility: visible !important; background-color: rgb(255, 255, 255);"/><br/></p><p style="max-width: 100%; min-height: 1em; line-height: 25.6px; font-size: 18px; box-sizing: border-box !important; word-wrap: break-word !important;"><img data-ratio="1.2517985611510791" data-s="300,640" data-src="http://mmbiz.qpic.cn/mmbiz/S7rG6oF8SF5E6pzW3oO53UZ2ib1ibDaIfgSKPMBLsC6ib8Y8RHRmqSouWPSicqpIxiaSpyTZOqicO8YVMEia0owWagfcw/640?wx_fmt=jpeg" data-type="jpeg" data-w="" style="line-height: 25.6px; text-align: center; box-sizing: border-box !important; word-wrap: break-word !important; width: auto !important; visibility: visible !important; background-color: rgb(255, 255, 255);"/></p><p style="max-width: 100%; min-height: 1em; line-height: 25.6px; font-size: 18px; box-sizing: border-box !important; word-wrap: break-word !important;"><br/></p><p style="max-width: 100%; min-height: 1em; line-height: 25.6px; font-size: 18px; text-align: center; box-sizing: border-box !important; word-wrap: break-word !important; background-color: rgb(255, 255, 255);"><span style="max-width: 100%; font-size: 14px; color: rgb(127, 127, 127); box-sizing: border-box !important; word-wrap: break-word !important;">本期责任编辑：<span style="color: rgb(127, 127, 127); font-size: 14px; line-height: 25.6px; text-align: center;  background-color: rgb(255, 255, 255);">壹贰叁</span></span></p><p style="max-width: 100%; min-height: 1em; line-height: 25.6px; font-size: 18px; box-sizing: border-box !important; word-wrap: break-word !important; background-color: rgb(255, 255, 255);"><br style="max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important;"/></p><p style="max-width: 100%; min-height: 1em; line-height: 23.2727px; box-sizing: border-box !important; word-wrap: break-word !important;">您的喜欢是我们最大的动力，如果觉得还不错，请点一下下方大拇指<strong style="max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important;"><span style="max-width: 100%; color: rgb(255, 76, 0); box-sizing: border-box !important; word-wrap: break-word !important;">点赞</span></strong>，也欢迎在下方“<strong style="max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important;"><span style="max-width: 100%; color: rgb(255, 76, 0); box-sizing: border-box !important; word-wrap: break-word !important;">写留言</span></strong>”处发表您的看法！谢谢！<span style="max-width: 100%; color: rgb(255, 76, 0); box-sizing: border-box !important; word-wrap: break-word !important;"><strong style="max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important;">您的支持和鼓励是我们前进的动力！</strong></span></p><section class="tn-Powered-by-weipaiban" style="max-width: 100%; line-height: 25.6px; font-size: 18px; height: 0.1em; box-sizing: border-box !important; word-wrap: break-word !important; background-color: rgb(212, 43, 43);"></section><section class="tn-Powered-by-weipaiban" style="margin-top: 0.3em; margin-bottom: 0.3em; max-width: 100%; line-height: 25.6px; font-size: 18px; height: 0.2em; box-sizing: border-box !important; word-wrap: break-word !important; background-color: rgb(212, 43, 43);"></section><section class="tn-Powered-by-weipaiban" style="padding: 0.5em; max-width: 100%; font-size: 1em; line-height: 1.6em; border: 1px solid rgb(226, 226, 226); box-shadow: rgb(226, 226, 226) 0em 1em 0.1em -0.8em; text-align: center; box-sizing: border-box !important; word-wrap: break-word !important; background-color: white;"><span style="max-width: 100%; font-size: 14px; box-sizing: border-box !important; word-wrap: break-word !important;"><span class="tn-Powered-by-weipaiban" style="max-width: 100%; display: inline-block; color: inherit; font-family: inherit; text-align: inherit; text-decoration: inherit; box-sizing: border-box !important; word-wrap: break-word !important;">更多内容请点击左下角</span><span class="tn-Powered-by-weipaiban" style="max-width: 100%; display: inline-block; color: rgb(64, 84, 115); font-family: inherit; text-align: inherit; text-decoration: inherit; box-sizing: border-box !important; word-wrap: break-word !important;">“阅读原文”，<span style="max-width: 100%; display: inline-block; color: rgb(0, 0, 0); font-family: inherit; text-align: inherit; text-decoration: inherit; box-sizing: border-box !important; word-wrap: break-word !important;">访问“</span><strong style="max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important;"><span style="max-width: 100%; display: inline-block; color: rgb(192, 0, 0); font-family: inherit; font-weight: inherit; text-align: inherit; text-decoration: inherit; box-sizing: border-box !important; word-wrap: break-word !important;">法国侨网</span></strong><span style="max-width: 100%; display: inline-block; color: rgb(0, 0, 0); font-family: inherit; text-align: inherit; text-decoration: inherit; box-sizing: border-box !important; word-wrap: break-word !important;">”</span></span></span> <span class="tn-Powered-by-weipaiban" style="max-width: 100%; display: inline-block; color: inherit; font-size: 1em; font-family: inherit; text-align: inherit; text-decoration: inherit; box-sizing: border-box !important; word-wrap: break-word !important;"></span></section><section class="tn-Powered-by-weipaiban" style="max-width: 100%; color: rgb(212, 43, 43); font-size: 2em; box-sizing: border-box !important; word-wrap: break-word !important; background-color: rgb(255, 255, 255);">↓↓↓</section>
    </div>
    <script type="text/javascript">
                            var first_sceen__time = (+new Date());
    
                            if ("" == 1 && document.getElementById('js_content'))
                                document.getElementById('js_content').addEventListener("selectstart",function(e){ e.preventDefault(); });
    
                                            (function(){
                                if (navigator.userAgent.indexOf("WindowsWechat") != -1){
                                    var link = document.createElement('link');
                                    var head = document.getElementsByTagName('head')[0];
                                    link.rel = 'stylesheet';
                                    link.type = 'text/css';
                                    link.href = "http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/style/page/appmsg/page_mp_article_improve_winwx2b1291.css";
                                    head.appendChild(link);
                                }
                            })();
                        </script>
    <link href="http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/style/page/appmsg/page_mp_article_improve_combo2a7a3f.css" rel="stylesheet" type="text/css"/>
    <div class="rich_media_tool" id="js_toobar3">
    <div class="media_tool_meta tips_global meta_primary" id="js_read_area3" style="display:none;">阅读 <span id="readNum3"></span></div>
    <span class="media_tool_meta meta_primary tips_global meta_praise" id="like3" style="display:none;">
    <i class="icon_praise_gray"></i><span class="praise_num" id="likeNum3"></span>
    </span>
    <a class="media_tool_meta tips_global meta_extra" href="javascript:void(0);" id="js_report_article3" style="display:none;">举报</a>
    </div>
    </div>
    <div class="rich_media_area_extra">
    <div class="mpda_bottom_container" id="js_bottom_ad_area">
    </div>
    <div id="js_iframetest" style="display:none;"></div>
    <div class="rich_media_extra" id="js_cmt_area" style="display:none">
    <div class="discuss_container" id="js_cmt_main" style="display:none">
    <div class="rich_tips with_line title_tips discuss_title_line">
    <span class="tips">精选留言</span>
    </div>
    <p class="tips_global tc title_bottom_tips" id="js_cmt_nofans1" style="display:none;">关注该公众号可参与留言</p>
    <p class="discuss_icon_tips title_bottom_tips tr" id="js_cmt_addbtn1" style="display:none">
    <a href="#comment">写留言<img alt="" class="icon_edit" src="http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/images/icon/appmsg/icon_edit25ded2.png"/></a>
    </p>
    <ul class="discuss_list" id="js_cmt_list"></ul>
    </div>
    <div class="tips_global rich_split_tips tc" id="js_cmt_nofans2" style="display:none;">
                                关注该公众号可参与留言
                            </div>
    <p class="discuss_icon_tips rich_split_tips tr" id="js_cmt_addbtn2" style="display:none">
    <a href="#comment">写留言<img alt="" class="icon_edit" src="http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/images/icon/appmsg/icon_edit25ded2.png"/></a>
    </p>
    <p class="rich_split_tips tc tips_global" id="js_cmt_tips" style="display:none;"></p>
    <div class="rich_tips tips_global loading_tips" id="js_cmt_loading">
    <img alt="" class="rich_icon icon_loading_white" src="http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/images/icon/common/icon_loading_white2805ea.gif"/>
    <span class="tips">加载中</span>
    </div>
    <div class="rich_tips with_line tips_global" id="js_cmt_statement" style="display:none">
    <span class="tips">以上留言由公众号筛选后显示</span>
    </div>
    <p class="rich_split_tips tc" id="js_cmt_qa" style="display:none;">
    <a href="http://kf.qq.com/touch/sappfaq/150211YfyMVj150313qmMbyi.html?scene_id=kf264">
                                    了解留言功能详情
                                </a>
    </p>
    </div>
    </div>
    </div>
    <div class="qr_code_pc_outer" id="js_pc_qr_code" style="display:none;">
    <div class="qr_code_pc_inner">
    <div class="qr_code_pc">
    <img class="qr_code_pc_img" id="js_pc_qr_code_img"/>
    <p>微信扫一扫<br/>关注该公众号</p>
    </div>
    </div>
    </div>
    </div>
    </div>
    <script>window.moon_map = {"appmsg/emotion/caret.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/emotion/caret278965.js","appmsg/emotion/map.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/emotion/map278965.js","appmsg/emotion/textarea.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/emotion/textarea27cdc5.js","appmsg/emotion/nav.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/emotion/nav278965.js","appmsg/emotion/common.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/emotion/common278965.js","appmsg/emotion/slide.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/emotion/slide2a9cd9.js","biz_wap/jsapi/cardticket.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_wap/jsapi/cardticket275627.js","pages/report.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/pages/report2a26bd.js","pages/music_player.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/pages/music_player2b674b.js","pages/loadscript.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/pages/loadscript292ed8.js","appmsg/emotion/dom.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/emotion/dom278965.js","appmsg/emotion/emotion.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/emotion/emotion2a9cd9.js","biz_wap/utils/hashrouter.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_wap/utils/hashrouter2805ea.js","appmsg/cmt_tpl.html.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/cmt_tpl.html2a2c13.js","a/gotoappdetail.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/a/gotoappdetail2a2c13.js","a/ios.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/a/ios275627.js","a/android.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/a/android275627.js","a/profile.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/a/profile29b1f8.js","a/card.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/a/card29b1f8.js","biz_wap/utils/position.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_wap/utils/position29b1f8.js","appmsg/a_report.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/a_report2b6c15.js","biz_common/utils/report.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_common/utils/report275627.js","biz_common/utils/huatuo.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_common/utils/huatuo293afc.js","biz_common/utils/cookie.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_common/utils/cookie275627.js","pages/voice_component.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/pages/voice_component2b674b.js","new_video/ctl.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/new_video/ctl292ed8.js","biz_common/utils/monitor.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_common/utils/monitor2a30ee.js","biz_common/utils/spin.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_common/utils/spin275627.js","biz_wap/jsapi/pay.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_wap/jsapi/pay275627.js","appmsg/reward_entry.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/reward_entry2b1291.js","appmsg/comment.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/comment2a9cd9.js","appmsg/like.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/like2b5583.js","appmsg/a.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/a2b6c8f.js","pages/version4video.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/pages/version4video2a6afa.js","biz_wap/utils/storage.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_wap/utils/storage2a74ac.js","biz_common/tmpl.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_common/tmpl2b3578.js","appmsg/img_copyright_tpl.html.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/img_copyright_tpl.html2a2c13.js","appmsg/a_tpl.html.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/a_tpl.html2a4dd8.js","biz_common/ui/imgonepx.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_common/ui/imgonepx275627.js","biz_common/dom/attr.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_common/dom/attr275627.js","biz_wap/utils/ajax.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_wap/utils/ajax2b6c15.js","biz_common/utils/string/html.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_common/utils/string/html29f4e9.js","appmsg/report.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/report2a30ee.js","biz_common/dom/class.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_common/dom/class275627.js","appmsg/report_and_source.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/report_and_source2b6c8f.js","appmsg/page_pos.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/page_pos2b6c15.js","appmsg/cdn_speed_report.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/cdn_speed_report275627.js","appmsg/voice.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/voice2ab8bd.js","appmsg/qqmusic.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/qqmusic2ab8bd.js","appmsg/iframe.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/iframe2b39ee.js","appmsg/review_image.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/review_image2a5394.js","appmsg/outer_link.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/outer_link275627.js","biz_wap/jsapi/core.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_wap/jsapi/core2b0d03.js","biz_common/dom/event.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_common/dom/event275627.js","appmsg/copyright_report.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/copyright_report2a2c13.js","appmsg/cache.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/cache2a74ac.js","appmsg/pay_for_reading.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/pay_for_reading28f721.js","appmsg/async.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/async2ae6ea.js","biz_wap/ui/lazyload_img.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_wap/ui/lazyload_img2b18f6.js","biz_common/log/jserr.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_common/log/jserr2805ea.js","appmsg/share.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/share2b5583.js","biz_wap/utils/mmversion.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_wap/utils/mmversion275627.js","appmsg/cdn_img_lib.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/cdn_img_lib275627.js","biz_common/utils/url/parse.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_common/utils/url/parse275627.js","biz_wap/utils/device.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_wap/utils/device2b3aae.js","biz_wap/jsapi/a8key.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_wap/jsapi/a8key2a30ee.js","appmsg/index.js":"http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/appmsg/index2a9baf.js"};window.moon_crossorigin = true;</script><script crossorigin="" src="http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/js/biz_wap/moon2b5731.js" type="text/javascript"></script>
    <script id="voice_tpl" type="text/html">        
            <span id="voice_main_<#=voiceid#>_<#=posIndex#>" class="db audio_area <#if(!musicSupport){#> unsupport<#}#>">
                <span class="tc tips_global unsupport_tips" <#if(show_not_support!==true){#>style="display:none;"<#}#>>
                当前浏览器不支持播放音乐或语音，请在微信或其他浏览器中播放            </span>
                <span class="audio_wrp db">
                    <span id="voice_play_<#=voiceid#>_<#=posIndex#>" class="audio_play_area">
                        <i class="icon_audio_default"></i>
                        <i class="icon_audio_playing"></i>
                        <img src="http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/images/icon/appmsg/audio/icon_audio_unread26f1f1.png" alt="" class="pic_audio_default">
                    </span>
                    <span class="audio_length tips_global"><#=duration_str#></span>
                    <span class="db audio_info_area">
                        <strong class="db audio_title"><#=title#></strong>
                        <span class="audio_source tips_global"><#if(window.nickname){#>来自<#=window.nickname#><#}#></span>
                    </span>
                    <span id="voice_progress_<#=voiceid#>_<#=posIndex#>" class="progress_bar" style="width:0px;"></span>
                </span>
            </span>
        </script>
    <script id="qqmusic_tpl" type="text/html">        
            <span id="qqmusic_main_<#=comment_id#>_<#=posIndex#>" class="db qqmusic_area <#if(!musicSupport){#> unsupport<#}#>">
                <span class="tc tips_global unsupport_tips" <#if(show_not_support!==true){#>style="display:none;"<#}#>>
                当前浏览器不支持播放音乐或语音，请在微信或其他浏览器中播放            </span>
                <span class="db qqmusic_wrp">
                    <span class="db qqmusic_bd">
                        <span id="qqmusic_play_<#=musicid#>_<#=posIndex#>" class="play_area">
                            <i class="icon_qqmusic_switch"></i>
                            <img src="http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/images/icon/appmsg/qqmusic/icon_qqmusic_default.2x26f1f1.png" alt="" class="pic_qqmusic_default">
                            <img src="<#=music_img#>" data-autourl="<#=audiourl#>" data-musicid="<#=musicid#>" class="qqmusic_thumb" alt="">
                        </span>
                                            <a id="qqmusic_home_<#=musicid#>_<#=posIndex#>" href="javascript:void(0);" class="access_area">
                            <span class="qqmusic_songname"><#=music_name#></span>
                            <span class="qqmusic_singername"><#=singer#></span>
                            <span class="qqmusic_source"><img src="http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/images/icon/appmsg/qqmusic/icon_qqmusic_source263724.png" alt=""></span>
                        </a>
                    </span>
                </span>       
            </span>
        </script>
    <script type="text/javascript">
          var not_in_mm_css = "http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/style/page/appmsg/not_in_mm2a7a3f.css";
          var windowwx_css = "http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/style/page/appmsg/page_mp_article_improve_winwx2b1291.css";
          var tid = "";
          var aid = "";
          var clientversion = "0";
          var appuin = "MjM5OTE5MzE4Mg==";
    
          var source = "6";
          var scene = 75;
          
          var itemidx = "";
    
          var _copyright_stat = "0";
          var _ori_article_type = "";
    
          var nickname = "法国侨报";
          var appmsg_type = "9";
          var ct = "1454166081";
          var user_name = "gh_045b6f6d854d";
          var user_name_new = "";
          var fakeid   = "";
          var version   = "";
          var is_limit_user   = "0";
          var msg_title = "关注&nbsp;|&nbsp;“寨卡”病毒中南美洲迅速蔓延&nbsp;巴西蒙上阴影";
          var msg_desc = "2016-01-30&nbsp;法国侨报请点右上方“…”，点击“发送给朋友”或“分享到朋友圈”，分享法国生活、巴黎时尚";
          var msg_cdn_url = "http://mmbiz.qpic.cn/mmbiz/S7rG6oF8SF6npbiazs3SSicyWEhgM0HGUaHngtf5QzF5XZ4hzcIEAIEfdThrBb7Nd9l25hptnVR1uDbZtkTXPCBw/0?wx_fmt=jpeg";
          var msg_link = "http://mp.weixin.qq.com/s?__biz=MjM5OTE5MzE4Mg==&amp;mid=403624174&amp;idx=4&amp;sn=1ba7d9edfd708ae64df7b31b662b1b59#rd";
          var user_uin = "0"*1;
          var msg_source_url = '';
          var img_format = 'jpeg';
          var srcid = '';
          var networkType;
          var appmsgid = '' || '403624174';
          var comment_id = "1472488771" * 1;
          var comment_enabled = "" * 1;
          var is_need_reward = "0" * 1;
          var is_https_res = ("" * 1) && (location.protocol == "https:");
    
          var devicetype = "";
          var source_username = "";  
          var profile_ext_signature = "" || "";
          var reprint_ticket = "";
          var source_mid = "";
          var source_idx = "";
    
          var show_comment = "";
          var __appmsgCgiData = {
                can_use_page : "0"*1,
                is_wxg_stuff_uin : "0"*1,
                card_pos : "",
                copyright_stat : "0",
                source_biz : "",
                hd_head_img : "http://wx.qlogo.cn/mmhead/Q3auHgzwzM78hHhbb3dlm6vXlR88aRXY7ianTicmcicicRhg01lpg1q5iaw/0"||(window.location.protocol+"//"+window.location.host + "http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/images/pic/appmsg/pic_rumor_link.2x264e76.jpg")
          };
          var _empty_v = "http://res.wx.qq.com/mmbizwap/zh_CN/htmledition/images/pic/pages/voice/empty26f1f1.mp3";
    
          var copyright_stat = "0" * 1;
    
          var pay_fee = "" * 1;
          var pay_timestamp = "";
          var need_pay = "" * 1;
    
          var need_report_cost = "0" * 1;
          
                window.wxtoken = "";
              if(!!window.__initCatch){
            window.__initCatch({
                idkey : 27613,
                startKey : 0,
                limit : 9,
                reportOpt : {
                    uin : uin,
                    biz : biz,
                    mid : mid,
                    idx : idx,
                    sn  : sn
                }
                
                
                
            });
        }
      </script>
    <script type="text/javascript">
          seajs.use('appmsg/index.js');
      </script>
    </body>
    </html>



Depending on how we define "raw conent, we might want to extract different things. I'm going to assume that content is raw text inside whole body. Alternative would be to extract div with class `rich_media_content`.


```python
def extract_text_content(body):
    soup = BeautifulSoup(body)
    [s.extract() for s in soup('script')]
    return soup.body.text
```


```python
extract_text_content(pages_df.body.iloc[0])
```

    /Users/mateusz/.virtualenvs/deep-learning-with-python/lib/python3.6/site-packages/bs4/__init__.py:181: UserWarning: No parser was explicitly specified, so I'm using the best available HTML parser for this system ("lxml"). This usually isn't a problem, but if you run this code on another system, or in a different virtual environment, it may use a different parser and behave differently.
    
    The code that caused this warning is on line 193 of the file /usr/local/Cellar/python3/3.6.1/Frameworks/Python.framework/Versions/3.6/lib/python3.6/runpy.py. To get rid of this warning, change code that looks like this:
    
     BeautifulSoup(YOUR_MARKUP})
    
    to this:
    
     BeautifulSoup(YOUR_MARKUP, "lxml")
    
      markup_type=markup_type))





    '\n\n\n\n关注 | “寨卡”病毒中南美洲迅速蔓延\xa0巴西蒙上阴影\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n提交\n\n\n我的留言\n\n\n\n\n\n加载中\n\n\n\n已留言\n\n\n\n\n\n\n\n\n\n\n                        关注 | “寨卡”病毒中南美洲迅速蔓延\xa0巴西蒙上阴影 \n                    \n\n2016-01-30\n法国侨报\n法国侨报\n\n\n法国侨报\n\n\n微信号\necmdpress\n\n\n功能介绍\n关注《法国侨报》微信平台，即时获取您身边最新、最快的新闻时事，抢先知晓华侨华人动态。侨报竭力为您服务，关注世界，关注法国，关注华侨华人，关注《法国侨报》。\n\n\n\n\n\n\n\n\n\n请点右上方“…”，点击“发送给朋友”或“分享到朋友圈”，分享法国生活、巴黎时尚1月29日消息，“寨卡热”疫情在中南美洲迅速蔓延，欧洲多国也相继出现确诊个案。世界卫生组织(WHO)28日举行特别会议，总干事陈冯富珍指，寨卡病毒正在“爆炸性扩散”，可能会在美洲感染300万至400万人。她表示，尽管未确切证明寨卡热与初生婴儿小头症有关，但“风险水平极高”。\xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0 \xa0据中新网报道法国侨网推广本期责任编辑：壹贰叁您的喜欢是我们最大的动力，如果觉得还不错，请点一下下方大拇指点赞，也欢迎在下方“写留言”处发表您的看法！谢谢！您的支持和鼓励是我们前进的动力！更多内容请点击左下角“阅读原文”，访问“法国侨网”\xa0↓↓↓\n\n\n\n\n阅读 \n\n\n\n举报\n\n\n\n\n\n\n\n\n\n精选留言\n\n关注该公众号可参与留言\n\n写留言\n\n\n\n\n                            关注该公众号可参与留言\n                        \n\n写留言\n\n\n\n\n加载中\n\n\n以上留言由公众号筛选后显示\n\n\n\n                                了解留言功能详情\n                            \n\n\n\n\n\n\n\n\n微信扫一扫关注该公众号\n\n\n\n\n\n\n\n\n\n\n'




```python
extract_text_content(pages_df.body.iloc[1])
```

    /Users/mateusz/.virtualenvs/deep-learning-with-python/lib/python3.6/site-packages/bs4/__init__.py:181: UserWarning: No parser was explicitly specified, so I'm using the best available HTML parser for this system ("lxml"). This usually isn't a problem, but if you run this code on another system, or in a different virtual environment, it may use a different parser and behave differently.
    
    The code that caused this warning is on line 193 of the file /usr/local/Cellar/python3/3.6.1/Frameworks/Python.framework/Versions/3.6/lib/python3.6/runpy.py. To get rid of this warning, change code that looks like this:
    
     BeautifulSoup(YOUR_MARKUP})
    
    to this:
    
     BeautifulSoup(YOUR_MARKUP, "lxml")
    
      markup_type=markup_type))





    '\n\n\n\n㊙身为一个小姐，不能有感觉了再接客……\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n提交\n\n\n我的留言\n\n\n\n\n\n加载中\n\n\n\n已留言\n\n\n\n\n\n\n\n\n\n\n                        ㊙身为一个小姐，不能有感觉了再接客…… \n                    \n\n2016-01-30\n我爱服装搭配\n我爱服装搭配\n\n\n我爱服装搭配\n\n\n微信号\ndapei522\n\n\n功能介绍\n服装搭配小知识，提升自身魅力哦\n\n\n\n\n\n\n\n\n\n作为一个小姐，你不能有了感觉才开门接客。作为一个作家，你不能有了灵感才动笔写作。作为一个员工，你不能有心情了才开始工作。人生如妓，努力达到角色与体验的高度统一，才是正道。骑自行车，再努力也追不上路虎。说明：平台很重要!男人，再优秀，没女人也生不下孩子。说明：合作很重要！一个人，再有能力，也干不过一群人。说明：团队很重要！想有保障，买再大的水桶都不如挖一口井。说明：渠道很重要！两只青蛙相爱，婚后生一癞蛤蟆。公青蛙见状大怒：怎么回事？母青蛙哭着说：他爹，认识你之前我整过容。说明：了解很重要！小驴问老驴：为啥咱们天天吃草，而奶牛顿顿精饲料？老驴叹道：咱爷们靠腿吃饭，人家靠胸脯吃饭。说明：心态很重要！鸭子与螃蟹赛跑难分胜负，裁判说：你们划拳确定吧！鸭子大怒：我出的全是布，他总是剪刀。说明：先天很重要！狗对熊说：嫁给我吧，你会幸福的。熊说：嫁你生狗熊，我要嫁给猫，生熊猫才尊贵。说明：选择很重要！跟着苍蝇找厕所，跟着蜜蜂找花朵，跟着富翁挣百万，跟着乞丐会要饭！现实生活中，你和谁在一起的确很重要，甚至能改变你的成长轨迹，决定你的人生成败。和什么样的人在一起，就会有什么样的人生。和勤奋的人在一起，你不会懒惰；和积极的人在一起，你不会消沉；与智者同行，你会不同凡响；与高人为伍，你能登上巅峰。科学家研究认为：人是唯一能接受暗示的动物。积极的暗示，会对人的情绪和生理状态产生良好的影响，激发人的内在潜能，发挥人的超常水平，使人进取，催人奋进。远离消极的人吧!否则，他们会在不知不觉中偷走你的梦想，使你渐渐颓废，变得平庸。积极的人像太阳，照到哪里哪里亮；消极的人像月亮，初一十五不一样。态度决定一切。有什么态度，就有什么样的未来：性格决定命运。有怎样的性格，就有怎样的人生。有人说，人生有三大幸运：上学时遇到一位好老师；工作时遇到一位好师傅；成家时遇到一个好伴侣。有时他们一个甜美的笑容，一句温馨的问候，就能使你的人生与众不同，光彩照人。生活中最不幸的是：由于你身边缺乏积极进取的人，缺少远见卓识的人，使你的人生变得平平庸庸，黯然无光！有句话说得好：你是谁并不重要,重要的是你和谁在一起。\n\n\n\n\n阅读 \n\n\n\n举报\n\n\n\n\n\n\n\n\n\n\n\n\n微信扫一扫关注该公众号\n\n\n\n\n\n\n\n\n\n\n'




```python
pages_df['raw_content'] = pages_df.body.map(extract_text_content)
```

    /Users/mateusz/.virtualenvs/deep-learning-with-python/lib/python3.6/site-packages/bs4/__init__.py:181: UserWarning: No parser was explicitly specified, so I'm using the best available HTML parser for this system ("lxml"). This usually isn't a problem, but if you run this code on another system, or in a different virtual environment, it may use a different parser and behave differently.
    
    The code that caused this warning is on line 193 of the file /usr/local/Cellar/python3/3.6.1/Frameworks/Python.framework/Versions/3.6/lib/python3.6/runpy.py. To get rid of this warning, change code that looks like this:
    
     BeautifulSoup(YOUR_MARKUP})
    
    to this:
    
     BeautifulSoup(YOUR_MARKUP, "lxml")
    
      markup_type=markup_type))



```python
pages_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>body</th>
      <th>date</th>
      <th>id</th>
      <th>time</th>
      <th>title</th>
      <th>url</th>
      <th>clean_title</th>
      <th>biz_id</th>
      <th>publish_time</th>
      <th>raw_content</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020759</td>
      <td>2016-01-30 16:00:00</td>
      <td>关注&amp;nbsp;|&amp;nbsp;“寨卡”病毒中南美洲迅速蔓延&amp;nbsp;巴西蒙上阴影</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5OTE5MzE4Mg...</td>
      <td>关注 | “寨卡”病毒中南美洲迅速蔓延 巴西蒙上阴影</td>
      <td>MjM5OTE5MzE4Mg==</td>
      <td>2016-01-30 15:01:21</td>
      <td>\n\n\n\n关注 | “寨卡”病毒中南美洲迅速蔓延 巴西蒙上阴影\n\n\n\n\n\n...</td>
    </tr>
    <tr>
      <th>1</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020760</td>
      <td>2016-01-30 16:00:00</td>
      <td>㊙身为一个小姐，不能有感觉了再接客……</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5ODE2Mjk1Nw...</td>
      <td>㊙身为一个小姐，不能有感觉了再接客……</td>
      <td>MjM5ODE2Mjk1Nw==</td>
      <td>2016-01-30 15:11:35</td>
      <td>\n\n\n\n㊙身为一个小姐，不能有感觉了再接客……\n\n\n\n\n\n\n\n\n\...</td>
    </tr>
    <tr>
      <th>2</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020761</td>
      <td>2016-01-30 16:00:00</td>
      <td>两次战败，被揍的稀巴烂的德国为何能迅速崛起？</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5MTUyMjQ2MA...</td>
      <td>两次战败，被揍的稀巴烂的德国为何能迅速崛起？</td>
      <td>MjM5MTUyMjQ2MA==</td>
      <td>2016-01-30 15:12:37</td>
      <td>\n\n\n\n两次战败，被揍的稀巴烂的德国为何能迅速崛起？\n\n\n\n\n\n\n\n...</td>
    </tr>
    <tr>
      <th>3</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020762</td>
      <td>2016-01-30 16:00:00</td>
      <td>一个老炮儿的无奈抗争</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzA3MDU3MDc2Mw...</td>
      <td>一个老炮儿的无奈抗争</td>
      <td>MzA3MDU3MDc2Mw==</td>
      <td>2016-01-30 15:12:25</td>
      <td>\n\n\n\n一个老炮儿的无奈抗争\n\n\n\n\n\n\n\n\n\n\n\n\n\n...</td>
    </tr>
    <tr>
      <th>4</th>
      <td>&lt;!DOCTYPE html&gt;\n&lt;html&gt;\n    &lt;head&gt;\n        &lt;...</td>
      <td>2016-01-31</td>
      <td>79020763</td>
      <td>2016-01-30 16:00:00</td>
      <td>秘一个女子该有的精致生活</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzA3MzUwOTQwNg...</td>
      <td>秘一个女子该有的精致生活</td>
      <td>MzA3MzUwOTQwNg==</td>
      <td>2016-01-30 15:11:05</td>
      <td>\n\n\n\n秘一个女子该有的精致生活\n\n\n\n\n\n\n\n\n\n\n\n\n...</td>
    </tr>
  </tbody>
</table>
</div>




```python
pages_df.to_pickle('pages-parsed.pickle')
```

## Checking clicks


```python
!head -n100  ../wechat_data_medium/weixin_click
```

    54305043	http://mp.weixin.qq.com/s?__biz=MjM5OTk0NTMwMA==&mid=401622306&idx=1&sn=c61368facc3e0aee69175a50bf5c8c25&scene=6#rd	������������������������������������������������������������	0	0	1454169600	2016-01-31
    54305044	http://mp.weixin.qq.com/s?__biz=MzA3MzMxMzkwOA==&mid=402795157&idx=6&sn=46df1ae59c5db25aa15fd71bcf28caa0&scene=6#rd	������180���������������������������������������������������������������������������������������������	0	0	1454169600	2016-01-31
    54305045	http://mp.weixin.qq.com/s?__biz=MjM5OTE5MzE4Mg==&mid=403624174&idx=4&sn=1ba7d9edfd708ae64df7b31b662b1b59&scene=6#rd	������&nbsp;|&nbsp;������������������������������������������&nbsp;������������������	33	0	1454169600	2016-01-31
    54305046	http://mp.weixin.qq.com/s?__biz=MjM5ODE2Mjk1Nw==&mid=401867851&idx=2&sn=1a7d4f2ea9bf962229c702dfe664522c&scene=6#rd	���������������������������������������������������������	910	4	1454169600	2016-01-31
    54305047	http://mp.weixin.qq.com/s?__biz=MjM5MTUyMjQ2MA==&mid=402603428&idx=3&sn=affbe2b5c4777ff505ca2a014769f21c&scene=6#rd	������������������������������������������������������������������	426	6	1454169600	2016-01-31
    54305048	http://mp.weixin.qq.com/s?__biz=MzA3MDU3MDc2Mw==&mid=406918977&idx=1&sn=0df6864258ecdadb346c8dbcb900b462&scene=6#rd	������������������������������	88	7	1454169600	2016-01-31
    54305049	http://mp.weixin.qq.com/s?__biz=MzA3MzUwOTQwNg==&mid=404359766&idx=5&sn=08f3003d5d74963793b9c4ecee30fb95&scene=6#rd	������������������������������������	0	0	1454169601	2016-01-31
    54305050	http://mp.weixin.qq.com/s?__biz=MzAwNDgwNzYxMA==&mid=401256232&idx=1&sn=903519a688176bc7e40100d60681713a&scene=6#rd	������������������������������������������������������������������������	9	3	1454169601	2016-01-31
    54305051	http://mp.weixin.qq.com/s?__biz=MjM5OTc1NTU5OQ==&mid=401607566&idx=1&sn=551f9c2bef51a6a85b2a762387af585b&scene=6#rd	���������������������������������������������������	29	0	1454169601	2016-01-31
    54305052	http://mp.weixin.qq.com/s?__biz=MjM5MzY4MDU0MA==&mid=401284147&idx=3&sn=a259e92949448664ae385056d1aa0ab4&scene=6#rd	������������������������������������	0	0	1454169601	2016-01-31
    54305053	http://mp.weixin.qq.com/s?__biz=MjM5NjY3Nzg0MA==&mid=403972475&idx=4&sn=65b34e9beab6ca3fcd2b8796213dadc5&scene=6#rd	���������������������������������������������������	0	0	1454169601	2016-01-31
    54305054	http://mp.weixin.qq.com/s?__biz=MzA5ODQ0ODEyNA==&mid=402020373&idx=6&sn=349d421b8e9f4a01a335197cfc5d1a0f&scene=6#rd	������������������������������������������������������������	0	0	1454169602	2016-01-31
    54305055	http://mp.weixin.qq.com/s?__biz=MzA4OTU2MjIwNA==&mid=401871358&idx=4&sn=a6c77690fc918afea666e4ba93789d80&scene=6#rd	���������������������������������������������������������	0	0	1454169602	2016-01-31
    54305056	http://mp.weixin.qq.com/s?__biz=MzAwNDAxMDY3Mg==&mid=402865425&idx=4&sn=95778aa9b80edd98e2583d938dd98def&scene=6#rd	������������������������������������������������������������������	416	3	1454169602	2016-01-31
    54305057	http://mp.weixin.qq.com/s?__biz=MjM5MDYzOTE2MA==&mid=402236716&idx=2&sn=2ad20664cc73bde4947945d96d7843d0&scene=6#rd	���������������������������������������.	16	0	1454169603	2016-01-31
    54305058	http://mp.weixin.qq.com/s?__biz=MjM5NTM5NTIxMQ==&mid=401306281&idx=5&sn=a9edf2bd5a7390ecb22c436c8ab22585&scene=6#rd	��������������������������������������������������	282	0	1454169603	2016-01-31
    54305059	http://mp.weixin.qq.com/s?__biz=MjM5MjU4MjU2MA==&mid=402843478&idx=5&sn=93ca763eeaa63c984279b8f768cb38a5&scene=6#rd	������������������������������������������������������	2	0	1454169603	2016-01-31
    54305060	http://mp.weixin.qq.com/s?__biz=MzA5MDQ3MjEyNg==&mid=401457947&idx=1&sn=f81c2f9f43aa46d5776952a87a4400b6&scene=6#rd	���������������������������������������������������������	5	0	1454169603	2016-01-31
    54305061	http://mp.weixin.qq.com/s?__biz=MjM5MTY3NTg2MA==&mid=402703985&idx=5&sn=7d40e270404848d473d05f0f166bf681&scene=6#rd	���������������������������������������������������	0	0	1454169603	2016-01-31
    54305062	http://mp.weixin.qq.com/s?__biz=MzA3NTU2MTA1MQ==&mid=404067140&idx=2&sn=f1a01363e4a461e477142742e772e9df&scene=6#rd	������������������������5���������	21	0	1454169604	2016-01-31
    54305063	http://mp.weixin.qq.com/s?__biz=MzAxMjQ5OTUwOA==&mid=402003100&idx=7&sn=d4dbe0628bf7dc9a4e14f4772ab7f348&scene=6#rd	������������������������������������������	0	0	1454169604	2016-01-31
    54305064	http://mp.weixin.qq.com/s?__biz=MzA4NDEyOTAzMw==&mid=412603368&idx=8&sn=928cc38c918eb0ca8e8d17b88500d258&scene=6#rd	������������������������������������������������������������������	9	0	1454169604	2016-01-31
    54305065	http://mp.weixin.qq.com/s?__biz=MzIzNTE1ODMxNg==&mid=404131437&idx=2&sn=6470a2157b7836a022356cbe194e0960&scene=6#rd	���������������������������������������������������������	2	0	1454169605	2016-01-31
    54305066	http://mp.weixin.qq.com/s?__biz=MzA3MjQ4NzMxNg==&mid=401858498&idx=4&sn=4b72e5d68d2d1cec22455f319b23b6e4&scene=6#rd	������������������������������	0	0	1454169605	2016-01-31
    54305067	http://mp.weixin.qq.com/s?__biz=MzA4NDI1MjYxNA==&mid=402906969&idx=1&sn=4f26ff2c2b0da47672387c193484872c&scene=6#rd	������������������������������������������&nbsp;���������������	6	0	1454169605	2016-01-31
    54305068	http://mp.weixin.qq.com/s?__biz=MzA3MDQ0NjQzNg==&mid=402560439&idx=5&sn=82e6b88ef24e4cae41af35f9ee5be441&scene=6#rd	������.���������������������������������������	0	0	1454169606	2016-01-31
    54305069	http://mp.weixin.qq.com/s?__biz=MzA5NTU3OTUzMw==&mid=402863839&idx=2&sn=cd861137484fccf2bd7995362af260f2&scene=6#rd	������������������������������������������������������(���������������������)	1	0	1454169606	2016-01-31
    54305070	http://mp.weixin.qq.com/s?__biz=MzIwMTU2MTAwNw==&mid=404331570&idx=2&sn=e358b61f0f7ec83ade0ae0d11a4391fa&scene=6#rd	������������������������������������������������������������������������������������	127	0	1454169606	2016-01-31
    54305071	http://mp.weixin.qq.com/s?__biz=MzAwOTI5NjczNA==&mid=407123843&idx=1&sn=d366d1481367c23dc90b7aabb406effa&scene=6#rd	������������������������������2016������������������������������������������	4	0	1454169606	2016-01-31
    54305072	http://mp.weixin.qq.com/s?__biz=MzA4ODMwNzYyNA==&mid=405571481&idx=5&sn=723658f5574b42e720de93b59f7caf81&scene=6#rd	������������������������������������������������������������	0	0	1454169606	2016-01-31
    54305073	http://mp.weixin.qq.com/s?__biz=MzA4NDQ3MjQzMg==&mid=401423189&idx=3&sn=267388e22ef339a03c071c3ab5ca3f8d&scene=6#rd	40���������������������14������������������~~	0	0	1454169607	2016-01-31
    54305074	http://mp.weixin.qq.com/s?__biz=MzA5NTcyNzk2NA==&mid=402026027&idx=2&sn=45946d7680c2bec7a998666a45a0e2cb&scene=6#rd	������������������������������������������������������������������������	8	0	1454169607	2016-01-31
    54305075	http://mp.weixin.qq.com/s?__biz=MjM5NDY5MDgyOQ==&mid=401144183&idx=2&sn=09a35bea6b207922f4d99cf648d2d00f&scene=6#rd	���������������������������������������������	182	0	1454169607	2016-01-31
    54305076	http://mp.weixin.qq.com/s?__biz=MzA3ODU3MTAyMA==&mid=401987174&idx=5&sn=d58682927cea9147b5f93ca84c74eda3&scene=6#rd	������������������������������������������������������	0	0	1454169608	2016-01-31
    54305077	http://mp.weixin.qq.com/s?__biz=MzA4MjQ4MjA4OQ==&mid=401708083&idx=1&sn=2db65343744d2b77ebe39fb83e30b565&scene=6#rd	������������������������������������������������������	229	4	1454169608	2016-01-31
    54305078	http://mp.weixin.qq.com/s?__biz=MzA5MDI3MDMzNQ==&mid=401241221&idx=5&sn=e1a81e76fd74a7bdf337e555bdbc25ee&scene=6#rd	10���������������������������������������	0	0	1454169608	2016-01-31
    54305079	http://mp.weixin.qq.com/s?__biz=MjM5ODk0MjYxNQ==&mid=402896418&idx=1&sn=38a6b05f69b3f937e7410354650ff237&scene=6#rd	���������������������������������������������������&nbsp;������������������������������������	54	5	1454169608	2016-01-31
    54305080	http://mp.weixin.qq.com/s?__biz=MzA5MzQ4NzIyMw==&mid=401916596&idx=2&sn=6355346e47267c7970a1d10dbe23aa51&scene=6#rd	������������������������������������������������������������	0	0	1454169608	2016-01-31
    54305081	http://mp.weixin.qq.com/s?__biz=MzA3MDE5NTI4Nw==&mid=401227263&idx=6&sn=32065ac244ae74988ec1141894ecec93&scene=6#rd	���������������33���������������������������������������������	1	0	1454169609	2016-01-31
    54305082	http://mp.weixin.qq.com/s?__biz=MzA3MzcxODE2OA==&mid=403230012&idx=3&sn=cc739b8a54fcc1c713ce9a4fc8a9b117&scene=6#rd	������������������������������������������������������������������	1	0	1454169609	2016-01-31
    54305083	http://mp.weixin.qq.com/s?__biz=MzA3NTQ4OTMwOA==&mid=401987432&idx=1&sn=e542f733c14b853158f3ca4af2e45a32&scene=6#rd	���������������������������������������������������������������������	0	0	1454169609	2016-01-31
    54305084	http://mp.weixin.qq.com/s?__biz=MzA3NTQ3NTgzNg==&mid=402985010&idx=4&sn=de2678e4aeddc78e0d387e8fef22ecc7&scene=6#rd	������23���������������������������������������������������������������������	0	0	1454169609	2016-01-31
    54305085	http://mp.weixin.qq.com/s?__biz=MzA5MzI5NzUxOA==&mid=402998522&idx=4&sn=8afe85c034d08ae4bfa6e7072095b3ae&scene=6#rd	������������������	0	0	1454169609	2016-01-31
    54305086	http://mp.weixin.qq.com/s?__biz=MzA5OTgzMjEwNg==&mid=402823200&idx=1&sn=5279911744f8ee87c16da75cf8fa49cc&scene=6#rd	������������������������������������������������������������	0	0	1454169610	2016-01-31
    54305087	http://mp.weixin.qq.com/s?__biz=MjM5MTE1MDMwMA==&mid=401680538&idx=1&sn=e8074a1c82d43b5a1735531549a758f5&scene=6#rd	������������������1000���������������������������	187	1	1454169610	2016-01-31
    54305088	http://mp.weixin.qq.com/s?__biz=MzA4OTU1ODEyOQ==&mid=401418716&idx=4&sn=ca623e9fe156fcab1e1a0fdfb7ec604d&scene=6#rd	������������������������20���������������������������������������������������������������������	0	0	1454169611	2016-01-31
    54305089	http://mp.weixin.qq.com/s?__biz=MzA3NTI3OTA4Mw==&mid=401490263&idx=3&sn=61679452c2f6be7910d72b5b5dc1d92a&scene=6#rd	������������������������0������������������������������2������������������2������������������2���������������������������������������������QQ������������������������������������	17	0	1454169611	2016-01-31
    54305090	http://mp.weixin.qq.com/s?__biz=MzA5NzQ1NzgzNQ==&mid=402622072&idx=4&sn=96e0f432eea0e68457e9af9dd81fdbf0&scene=6#rd	������������������������������������������������	404	8	1454169611	2016-01-31
    54305091	http://mp.weixin.qq.com/s?__biz=MzA3MTY3OTgzOA==&mid=402805311&idx=2&sn=e7158cca9ce1bedf962091b32b4d8d7a&scene=6#rd	?������������������������������������������	18	1	1454169611	2016-01-31
    54305092	http://mp.weixin.qq.com/s?__biz=MjM5MTE1MDMwMA==&mid=401680538&idx=2&sn=17246aa7e37532636c6f17814c6822b1&scene=6#rd	������������������925���������������������������	94	0	1454169611	2016-01-31
    54305093	http://mp.weixin.qq.com/s?__biz=MzA3MTQ0NDAzNg==&mid=402723688&idx=2&sn=50e1bde164acdd820196b5f9cc3fd011&scene=6#rd	70���������������������������������������������	2	0	1454169611	2016-01-31
    54305094	http://mp.weixin.qq.com/s?__biz=MzAxNTYxNjE1Nw==&mid=401296512&idx=1&sn=9db5f95f5db43f6fce38cd0e323def3e&scene=6#rd	���������������������������������&nbsp;110���&nbsp;&nbsp;������������&nbsp;���������&nbsp;45���	31	0	1454169611	2016-01-31
    54305095	http://mp.weixin.qq.com/s?__biz=MzA4NDQ0NzkzNw==&mid=402018771&idx=2&sn=6c4b7096e2e222169902eca04576b80e&scene=6#rd	������������������������������������������������������������	1	0	1454169612	2016-01-31
    54305096	http://mp.weixin.qq.com/s?__biz=MjM5NTMwNDU0MA==&mid=402196607&idx=2&sn=0eb7e00a99242e5247ca740eab979bcc&scene=6#rd	������������������������������������������������������������������������������������	129	0	1454169612	2016-01-31
    54305097	http://mp.weixin.qq.com/s?__biz=MzA4OTI3MTcyMw==&mid=402630038&idx=5&sn=19bd460860f2f883845f8a5009222153&scene=6#rd	������������������������������������������������������������������(e)���(du)���	6	0	1454169613	2016-01-31
    54305098	http://mp.weixin.qq.com/s?__biz=MzAxNzI4MDk4MA==&mid=402453848&idx=2&sn=3c57b19ed215999e38850611bc6446c7&scene=6#rd	���������������������������.���.������	351	0	1454169613	2016-01-31
    54305099	http://mp.weixin.qq.com/s?__biz=MjM5NDIxMDg3NQ==&mid=402104859&idx=1&sn=ea36d900c52d2c49f963fa56760f4582&scene=6#rd	���������������������������������������������	383	12	1454169613	2016-01-31
    54305100	http://mp.weixin.qq.com/s?__biz=MzA5Mjg0NjU3Ng==&mid=554615530&idx=8&sn=02b1f2ab0316cb8c631b239ed2c9b327&scene=6#rd	������������������������������������������!	349	1	1454169613	2016-01-31
    54305101	http://mp.weixin.qq.com/s?__biz=MjM5ODU0Njc2MQ==&mid=402196843&idx=4&sn=38688dc83e78829e8176b1a9be3730cb&scene=6#rd	������������������������&nbsp;���������������������������������	10	0	1454169614	2016-01-31
    54305102	http://mp.weixin.qq.com/s?__biz=MzA3OTA5MzEyMA==&mid=401058840&idx=3&sn=28e7a57fb709ed41383c627e721fba21&scene=6#rd	������������������������������������������������������������������	1	0	1454169614	2016-01-31
    54305103	http://mp.weixin.qq.com/s?__biz=MzA3NjE1MTQ2OA==&mid=402514312&idx=1&sn=c29e9c530c8f4970af7cd4f555d76f8f&scene=6#rd	���������������������������24���������������������������������������������������	90	0	1454169614	2016-01-31
    54305104	http://mp.weixin.qq.com/s?__biz=MzA4MzAzNjc3Nw==&mid=401367285&idx=1&sn=6c2cda57deb190dc869166e056f569bd&scene=6#rd	���������������������������&nbsp;������������	69	0	1454169614	2016-01-31
    54305105	http://mp.weixin.qq.com/s?__biz=MjM5MTYwNjA0MQ==&mid=402079404&idx=5&sn=887deec42f10ba31ad68b91c25bb09bb&scene=6#rd	���������������������������&nbsp;Manolo&nbsp;Blahnik&nbsp;������������Manolo���������������������	323	0	1454169614	2016-01-31
    54305106	http://mp.weixin.qq.com/s?__biz=MzA3NDc1NDA5MQ==&mid=1100606957&idx=4&sn=633618795686a459a805fd7bda306615&scene=6#rd	������������������2���	9	0	1454169615	2016-01-31
    54305107	http://mp.weixin.qq.com/s?__biz=MzAwODAzODQ3Nw==&mid=403771280&idx=3&sn=f76af46a704f800843d64324078bd4fc&scene=6#rd	������������������������������������������������������������������	3	0	1454169615	2016-01-31
    54305108	http://mp.weixin.qq.com/s?__biz=MjM5MTE5NDEwMQ==&mid=401457184&idx=1&sn=6f1a79234022ce26c0a0affcf30b9cd9&scene=6#rd	������������������������������������������������������������������	12	0	1454169615	2016-01-31
    54305109	http://mp.weixin.qq.com/s?__biz=MjM5ODg4Nzk4Mg==&mid=402059511&idx=3&sn=3e836dcf4c4943a68652bc71b2801861&scene=6#rd	������������������������������������!������������!���������&quot;������������&quot;������������!	1178	3	1454169616	2016-01-31
    54305110	http://mp.weixin.qq.com/s?__biz=MzA5NzUyMDcxMw==&mid=403861228&idx=6&sn=568b6c5a514529c05cfb9c2034f6a9eb&scene=6#rd	���������������������������������������������������������	0	0	1454169616	2016-01-31
    54305111	http://mp.weixin.qq.com/s?__biz=MzA4NDAzMTA5MA==&mid=403061719&idx=1&sn=a9e458019a497075ff8a9cb1b2ab9b41&scene=6#rd	������������������������������19������������������	35	0	1454169616	2016-01-31
    54305112	http://mp.weixin.qq.com/s?__biz=MzA4MjAwMzYxNw==&mid=401103267&idx=3&sn=a3636864ae25c38969d443e11d7ddea1&scene=6#rd	������������������������������2������501���������������������������	1213	2	1454169616	2016-01-31
    54305113	http://mp.weixin.qq.com/s?__biz=MjM5NDg5OTU1OA==&mid=402205913&idx=4&sn=90faefc8b3cd0bc7b6b584ac73333865&scene=6#rd	������������3���������������������������������������	1438	1	1454169617	2016-01-31
    54305114	http://mp.weixin.qq.com/s?__biz=MzAxNjQ3MTczOA==&mid=431982095&idx=1&sn=c0225bd91647efcf1c7de1e82d3a6f62&scene=6#rd	������������������������������������������������������������������	3	0	1454169617	2016-01-31
    54305115	http://mp.weixin.qq.com/s?__biz=MzA3NjUxNDAzNg==&mid=404742149&idx=1&sn=572a3899272d9a8962182dc4b2010c16&scene=6#rd	���������������������������������������������������������������	0	0	1454169617	2016-01-31
    54305116	http://mp.weixin.qq.com/s?__biz=MzA3OTA5MzEyMA==&mid=401058840&idx=5&sn=561924149a33d6cb91e9603026d55c6c&scene=6#rd	������������&nbsp;|&nbsp;1334���������������������������������������������������������	2	0	1454169617	2016-01-31
    54305117	http://mp.weixin.qq.com/s?__biz=MzA3OTA5MzEyMA==&mid=401058840&idx=2&sn=93354de11c035081d9cb01d954dfde07&scene=6#rd	���������������������������������������������������������������������	0	0	1454169617	2016-01-31
    54305118	http://mp.weixin.qq.com/s?__biz=MzAxNjUzMDE1MA==&mid=401392441&idx=1&sn=94ca4cadeb1b79f9d9e4959a9f19791e&scene=6#rd	������������&nbsp;���������������������������������	14	0	1454169617	2016-01-31
    54305119	http://mp.weixin.qq.com/s?__biz=MjM5MjE3MzkxNw==&mid=403315959&idx=1&sn=6abb9cf2b185b854f685a0411af9e88e&scene=6#rd	���������������������������.���������������������������	0	0	1454169618	2016-01-31
    54305120	http://mp.weixin.qq.com/s?__biz=MzA5NzU1MTAwMQ==&mid=403564258&idx=1&sn=5bbfb5b43f230b11f8fa5277f969f609&scene=6#rd	������������������������������������������������������������������������	14107	31	1454169619	2016-01-31
    54305121	http://mp.weixin.qq.com/s?__biz=MjM5NDMxNDg5MA==&mid=401618123&idx=3&sn=e4d501958334f7f53d68effec3842197&scene=6#rd	������������������������������	0	0	1454169619	2016-01-31
    54305122	http://mp.weixin.qq.com/s?__biz=MjM5NzIwNDE0Mg==&mid=402805118&idx=2&sn=b0581ece0a32c86e7c833a749eb886b3&scene=6#rd	���������������������������������������������������������������������������	2724	2	1454169619	2016-01-31
    54305123	http://mp.weixin.qq.com/s?__biz=MzAxNjI1MDU1OQ==&mid=402757481&idx=1&sn=33c583d272402af59384ee51e0c3ca65&scene=6#rd	���������������������������������3������������������������	305	4	1454169619	2016-01-31
    54305124	http://mp.weixin.qq.com/s?__biz=MzAwNTMxNDQzOQ==&mid=402757040&idx=1&sn=2000f5c47234ddcd57e213ee10e80f7b&scene=6#rd	�����������������������������������������������������������������������	27	0	1454169619	2016-01-31
    54305125	http://mp.weixin.qq.com/s?__biz=MjM5OTA2NDU4OA==&mid=401725615&idx=6&sn=a3ffbe059891caf1a62912558e21375e&scene=6#rd	������������������������������������������������������������	241	0	1454169620	2016-01-31
    54305126	http://mp.weixin.qq.com/s?__biz=MzIwMDMzODA4Nw==&mid=401795619&idx=2&sn=aa8f0d77dd3d9a5a50927d1d33bafbf5&scene=6#rd	������������������������������5.4���������������������������2.8���������	27	0	1454169620	2016-01-31
    54305127	http://mp.weixin.qq.com/s?__biz=MjM5MzAwMTIyOQ==&mid=402234478&idx=6&sn=76b779aa3b83fcf7bd40448f402088f0&scene=6#rd	������������������iPhone7������������������������������������	311	0	1454169620	2016-01-31
    54305128	http://mp.weixin.qq.com/s?__biz=MzI0ODEyNjc3Nw==&mid=401470364&idx=3&sn=6e8583cd48b4677ec091b8909d6d3215&scene=6#rd	���������������������������������������������������������������	0	0	1454169621	2016-01-31
    54305129	http://mp.weixin.qq.com/s?__biz=MzA5MDE5Njk3Nw==&mid=401761483&idx=6&sn=7c8b985f3b4c95b7cb507bae4e777a46&scene=6#rd	���������������������������������������10������������������	0	0	1454169621	2016-01-31
    54305130	http://mp.weixin.qq.com/s?__biz=MjM5Nzk2ODA5Mw==&mid=402463487&idx=3&sn=a5271d0240a31df1d7a0d4f4c0e2802c&scene=6#rd	������������������������������������������	84	0	1454169621	2016-01-31
    54305131	http://mp.weixin.qq.com/s?__biz=MjM5NTE5OTIwNg==&mid=404974702&idx=1&sn=e57f3760dae553a13d3caefe0dcf9ca8&scene=6#rd	������������2016������������������������������������������������������������	28	2	1454169622	2016-01-31
    54305132	http://mp.weixin.qq.com/s?__biz=MzA3MTIxNTU4NA==&mid=401849165&idx=5&sn=9abfbda97fa1f0178d47fa6b145c7ab7&scene=6#rd	���������������������������������������������������������������������	0	0	1454169622	2016-01-31
    54305133	http://mp.weixin.qq.com/s?__biz=MzAwNjAyNDUwNA==&mid=411377065&idx=2&sn=a35af11a10f47a88a3de53287d7e5d9c&scene=6#rd	������������������������������������������������	2014	4	1454169622	2016-01-31
    54305134	http://mp.weixin.qq.com/s?__biz=MjM5ODY4ODUwMA==&mid=402380989&idx=4&sn=cb2cfb56ca21c20e4914229ca74346e4&scene=6#rd	���������������������������������������������,���������������	0	0	1454169622	2016-01-31
    54305135	http://mp.weixin.qq.com/s?__biz=MzAwMzMxMjkyMA==&mid=402576981&idx=5&sn=e852c56f56ce2e32e180ee03d2b9f369&scene=6#rd	������������������������	181	0	1454169622	2016-01-31
    54305136	http://mp.weixin.qq.com/s?__biz=MjM5NzUxNDY0MA==&mid=402459134&idx=1&sn=300f025f45e4dede62597f7663d7907e&scene=6#rd	���������������������������������������������������������������	463	3	1454169622	2016-01-31
    54305137	http://mp.weixin.qq.com/s?__biz=MzA5MjM1MjAwOA==&mid=401850260&idx=3&sn=16258e055c52af520698a23639143809&scene=6#rd	������������������������������������������������������������������	154	1	1454169622	2016-01-31
    54305138	http://mp.weixin.qq.com/s?__biz=MzA3NTQ2MTA2MA==&mid=404689669&idx=1&sn=1b15ffddce21a2108185ed371f2f217f&scene=6#rd	20������������������������������������������������	1	0	1454169622	2016-01-31
    54305139	http://mp.weixin.qq.com/s?__biz=MzA5MDQ3MzU5OA==&mid=406782134&idx=2&sn=5d1e30c16e479c471164b17ae4c8dd4e&scene=6#rd	���-������������-������������������������������������	0	0	1454169623	2016-01-31
    54305140	http://mp.weixin.qq.com/s?__biz=MjM5MTQ1MzA5Mg==&mid=401419901&idx=3&sn=8dbbcd1c690c6a9376280f2f48184dd3&scene=6#rd	������������������������&nbsp;|&nbsp;������������	94	3	1454169623	2016-01-31
    54305141	http://mp.weixin.qq.com/s?__biz=MjM5OTY4OTIzMg==&mid=401814080&idx=4&sn=e59be4c5a94e27d0952a91689ac0692c&scene=6#rd	���������������������������������������������	3876	6	1454169624	2016-01-31
    54305142	http://mp.weixin.qq.com/s?__biz=MzA5NDkzNDc2NQ==&mid=402221453&idx=2&sn=729710a120f6831c82bbbe6a1addfb5e&scene=6#rd	���������������������������������������������������������������	0	0	1454169624	2016-01-31



```python
clicks = pd.read_table(open("../wechat_data_medium/weixin_click", 'r',  encoding='utf-8', errors='replace'), names=[
    'id',
    'url',
    'title',
    'read_number',
    'like_number',
    'timestamp',
    'date',
], low_memory=False, index_col=False)

clicks['timestamp'] = pd.to_datetime(clicks.timestamp, unit='s')
clicks['date'] = pd.to_datetime(clicks.date)

clicks.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>url</th>
      <th>title</th>
      <th>read_number</th>
      <th>like_number</th>
      <th>timestamp</th>
      <th>date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>54305043</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5OTk0NTMwMA...</td>
      <td>到了夏威夷～才知道女人，可以不必买胸罩！</td>
      <td>0</td>
      <td>0</td>
      <td>2016-01-30 16:00:00</td>
      <td>2016-01-31</td>
    </tr>
    <tr>
      <th>1</th>
      <td>54305044</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzA3MzMxMzkwOA...</td>
      <td>每年180万人因喝酒而死亡，你知道酒后都有哪些事不宜做么？为自己为亲人。</td>
      <td>0</td>
      <td>0</td>
      <td>2016-01-30 16:00:00</td>
      <td>2016-01-31</td>
    </tr>
    <tr>
      <th>2</th>
      <td>54305045</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5OTE5MzE4Mg...</td>
      <td>关注&amp;nbsp;|&amp;nbsp;“寨卡”病毒中南美洲迅速蔓延&amp;nbsp;巴西蒙上阴影</td>
      <td>33</td>
      <td>0</td>
      <td>2016-01-30 16:00:00</td>
      <td>2016-01-31</td>
    </tr>
    <tr>
      <th>3</th>
      <td>54305046</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5ODE2Mjk1Nw...</td>
      <td>㊙身为一个小姐，不能有感觉了再接客……</td>
      <td>910</td>
      <td>4</td>
      <td>2016-01-30 16:00:00</td>
      <td>2016-01-31</td>
    </tr>
    <tr>
      <th>4</th>
      <td>54305047</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5MTUyMjQ2MA...</td>
      <td>两次战败，被揍的稀巴烂的德国为何能迅速崛起？</td>
      <td>426</td>
      <td>6</td>
      <td>2016-01-30 16:00:00</td>
      <td>2016-01-31</td>
    </tr>
  </tbody>
</table>
</div>




```python
clicks.shape
```




    (308495, 7)




```python
clicks['clean_title'] = clicks.title.map(double_unescape_html_text)
clicks.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>url</th>
      <th>title</th>
      <th>read_number</th>
      <th>like_number</th>
      <th>timestamp</th>
      <th>date</th>
      <th>clean_title</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>54305043</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5OTk0NTMwMA...</td>
      <td>到了夏威夷～才知道女人，可以不必买胸罩！</td>
      <td>0</td>
      <td>0</td>
      <td>2016-01-30 16:00:00</td>
      <td>2016-01-31</td>
      <td>到了夏威夷～才知道女人，可以不必买胸罩！</td>
    </tr>
    <tr>
      <th>1</th>
      <td>54305044</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzA3MzMxMzkwOA...</td>
      <td>每年180万人因喝酒而死亡，你知道酒后都有哪些事不宜做么？为自己为亲人。</td>
      <td>0</td>
      <td>0</td>
      <td>2016-01-30 16:00:00</td>
      <td>2016-01-31</td>
      <td>每年180万人因喝酒而死亡，你知道酒后都有哪些事不宜做么？为自己为亲人。</td>
    </tr>
    <tr>
      <th>2</th>
      <td>54305045</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5OTE5MzE4Mg...</td>
      <td>关注&amp;nbsp;|&amp;nbsp;“寨卡”病毒中南美洲迅速蔓延&amp;nbsp;巴西蒙上阴影</td>
      <td>33</td>
      <td>0</td>
      <td>2016-01-30 16:00:00</td>
      <td>2016-01-31</td>
      <td>关注 | “寨卡”病毒中南美洲迅速蔓延 巴西蒙上阴影</td>
    </tr>
    <tr>
      <th>3</th>
      <td>54305046</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5ODE2Mjk1Nw...</td>
      <td>㊙身为一个小姐，不能有感觉了再接客……</td>
      <td>910</td>
      <td>4</td>
      <td>2016-01-30 16:00:00</td>
      <td>2016-01-31</td>
      <td>㊙身为一个小姐，不能有感觉了再接客……</td>
    </tr>
    <tr>
      <th>4</th>
      <td>54305047</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5MTUyMjQ2MA...</td>
      <td>两次战败，被揍的稀巴烂的德国为何能迅速崛起？</td>
      <td>426</td>
      <td>6</td>
      <td>2016-01-30 16:00:00</td>
      <td>2016-01-31</td>
      <td>两次战败，被揍的稀巴烂的德国为何能迅速崛起？</td>
    </tr>
  </tbody>
</table>
</div>



Let's check those duplicated entries.


```python
clicks.url.value_counts().head()
```




    http://mp.weixin.qq.com/s?__biz=MjM5MTMwMTI3NA==&mid=403745393&idx=4&sn=2914fcd0359e3b6bc67faaf987e521f7&scene=6#rd    22
    http://mp.weixin.qq.com/s?__biz=MzAwNzU5MTAxNw==&mid=403315080&idx=1&sn=dd35486fc99fa545e7cd077c62acf7ff&scene=6#rd    22
    http://mp.weixin.qq.com/s?__biz=MzA3ODI0NzU4OQ==&mid=404248127&idx=1&sn=2fd8eb67b833ff0230154a90ff2f1bdd&scene=6#rd    22
    http://mp.weixin.qq.com/s?__biz=MjM5MjgxOTEzOA==&mid=402731812&idx=4&sn=eaaf422e0e8dc0c3396b874af57726d6&scene=6#rd    22
    http://mp.weixin.qq.com/s?__biz=MzA5ODc2NDMwMQ==&mid=401169401&idx=3&sn=c67de6bd30171d671a340c692acedae1&scene=6#rd    22
    Name: url, dtype: int64




```python
clicks[clicks.url == clicks.url.value_counts().index[0]].head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>url</th>
      <th>title</th>
      <th>read_number</th>
      <th>like_number</th>
      <th>timestamp</th>
      <th>date</th>
      <th>clean_title</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>27447</th>
      <td>54332490</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5MTMwMTI3NA...</td>
      <td>2000万社会精英关注的顶级新媒体，都在这里了！</td>
      <td>347</td>
      <td>1</td>
      <td>2016-01-30 23:38:50</td>
      <td>2016-01-31</td>
      <td>2000万社会精英关注的顶级新媒体，都在这里了！</td>
    </tr>
    <tr>
      <th>27448</th>
      <td>54332491</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5MTMwMTI3NA...</td>
      <td>2000万社会精英关注的顶级新媒体，都在这里了！</td>
      <td>347</td>
      <td>1</td>
      <td>2016-01-30 23:38:50</td>
      <td>2016-01-31</td>
      <td>2000万社会精英关注的顶级新媒体，都在这里了！</td>
    </tr>
    <tr>
      <th>27449</th>
      <td>54332492</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5MTMwMTI3NA...</td>
      <td>2000万社会精英关注的顶级新媒体，都在这里了！</td>
      <td>347</td>
      <td>1</td>
      <td>2016-01-30 23:38:50</td>
      <td>2016-01-31</td>
      <td>2000万社会精英关注的顶级新媒体，都在这里了！</td>
    </tr>
    <tr>
      <th>27450</th>
      <td>54332493</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5MTMwMTI3NA...</td>
      <td>2000万社会精英关注的顶级新媒体，都在这里了！</td>
      <td>347</td>
      <td>1</td>
      <td>2016-01-30 23:38:50</td>
      <td>2016-01-31</td>
      <td>2000万社会精英关注的顶级新媒体，都在这里了！</td>
    </tr>
    <tr>
      <th>27451</th>
      <td>54332494</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5MTMwMTI3NA...</td>
      <td>2000万社会精英关注的顶级新媒体，都在这里了！</td>
      <td>347</td>
      <td>1</td>
      <td>2016-01-30 23:38:50</td>
      <td>2016-01-31</td>
      <td>2000万社会精英关注的顶级新媒体，都在这里了！</td>
    </tr>
  </tbody>
</table>
</div>



Let's keep only those with maximum read_number


```python
clicks_no_duplicates = clicks.sort_values('read_number', ascending=False).drop_duplicates(['url'])
```


```python
clicks_no_duplicates.shape
```




    (257749, 8)




```python
clicks_no_duplicates.date.describe()
```




    count                  257749
    unique                      1
    top       2016-01-31 00:00:00
    freq                   257749
    first     2016-01-31 00:00:00
    last      2016-01-31 00:00:00
    Name: date, dtype: object




```python
clicks_no_duplicates.timestamp.describe()
```




    count                  257749
    unique                  57569
    top       2016-01-31 12:56:22
    freq                       25
    first     2016-01-30 16:00:00
    last      2016-01-31 15:59:59
    Name: timestamp, dtype: object




```python
clicks_no_duplicates.read_number.describe(percentiles=[0.5, 0.75, 0.9, 0.95, 0.999])
```




    count    257749.000000
    mean       1566.651665
    std        5366.179613
    min           0.000000
    50%         227.000000
    75%        1173.000000
    90%        3432.000000
    95%        6366.000000
    99.9%     89144.984000
    max      100001.000000
    Name: read_number, dtype: float64




```python
clicks_no_duplicates.read_number.hist()
```




    <matplotlib.axes._subplots.AxesSubplot at 0x2e9113c18>




![png](exploratory-data-analysis_files/exploratory-data-analysis_150_1.png)



```python
import numpy as np
clicks_no_duplicates.read_number.map(np.log1p).hist()
```




    <matplotlib.axes._subplots.AxesSubplot at 0x19ed2d7b8>




![png](exploratory-data-analysis_files/exploratory-data-analysis_151_1.png)


Looks like the distribution is sort of log normal, which is interesting. But probably we shouldn't treat extreme values as outliers.


```python
clicks_no_duplicates[clicks_no_duplicates.read_number > 1200].sort_values('read_number', ascending=False).head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>url</th>
      <th>title</th>
      <th>read_number</th>
      <th>like_number</th>
      <th>timestamp</th>
      <th>date</th>
      <th>clean_title</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>93890</th>
      <td>54398933</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5MjAxNDM4MA...</td>
      <td>【提醒】吓一跳！据说10年后中国的房子将是这样</td>
      <td>100001</td>
      <td>1192</td>
      <td>2016-01-31 03:11:16</td>
      <td>2016-01-31</td>
      <td>【提醒】吓一跳！据说10年后中国的房子将是这样</td>
    </tr>
    <tr>
      <th>15478</th>
      <td>54320521</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzAwMDAyMzY3OA...</td>
      <td>找个日本女孩做女朋友是什么样的体验？</td>
      <td>100001</td>
      <td>713</td>
      <td>2016-01-30 19:48:13</td>
      <td>2016-01-31</td>
      <td>找个日本女孩做女朋友是什么样的体验？</td>
    </tr>
    <tr>
      <th>171031</th>
      <td>54476074</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5ODI3ODI4MA...</td>
      <td>为什么“被追尾”还要负全责？警察叔叔说：因为你做了这些……</td>
      <td>100001</td>
      <td>126</td>
      <td>2016-01-31 07:28:07</td>
      <td>2016-01-31</td>
      <td>为什么“被追尾”还要负全责？警察叔叔说：因为你做了这些……</td>
    </tr>
    <tr>
      <th>125716</th>
      <td>54430759</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5MDMyMzg2MA...</td>
      <td>单身，会不会成为一种“病”</td>
      <td>100001</td>
      <td>391</td>
      <td>2016-01-31 04:51:59</td>
      <td>2016-01-31</td>
      <td>单身，会不会成为一种“病”</td>
    </tr>
    <tr>
      <th>299120</th>
      <td>54604163</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5MjAxNDM4MA...</td>
      <td>【提醒】几个孩子瞬间被炸飞…路上随处可见的这个东西碰不得！</td>
      <td>100001</td>
      <td>389</td>
      <td>2016-01-31 15:16:08</td>
      <td>2016-01-31</td>
      <td>【提醒】几个孩子瞬间被炸飞…路上随处可见的这个东西碰不得！</td>
    </tr>
  </tbody>
</table>
</div>




```python
clicks_no_duplicates.like_number.describe(percentiles=[0.5, 0.75, 0.9, 0.95, 0.999])
```




    count    257749.000000
    mean          7.252498
    std          74.825677
    min           0.000000
    50%           1.000000
    75%           3.000000
    90%          12.000000
    95%          25.000000
    99.9%       525.000000
    max       15199.000000
    Name: like_number, dtype: float64




```python
import numpy as np
clicks_no_duplicates.like_number.map(np.log1p).hist()
```




    <matplotlib.axes._subplots.AxesSubplot at 0x2e96e3160>




![png](exploratory-data-analysis_files/exploratory-data-analysis_155_1.png)



```python
import matplotlib.pyplot as plt

fig, ax = plt.subplots()
clicks_no_duplicates.like_number.map(np.log1p).hist(ax=ax)
ax.set_yscale('log')
```


![png](exploratory-data-analysis_files/exploratory-data-analysis_156_0.png)


Interesting, is it a Pareto distribution?


```python
clicks_no_duplicates.sort_values('like_number', ascending=False).head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>url</th>
      <th>title</th>
      <th>read_number</th>
      <th>like_number</th>
      <th>timestamp</th>
      <th>date</th>
      <th>clean_title</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>162238</th>
      <td>54467281</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzA5OTQyMDgyOQ...</td>
      <td>2016年1月31日（星期日）三分钟新闻早餐（综合版）</td>
      <td>100001</td>
      <td>15199</td>
      <td>2016-01-31 06:50:59</td>
      <td>2016-01-31</td>
      <td>2016年1月31日（星期日）三分钟新闻早餐（综合版）</td>
    </tr>
    <tr>
      <th>117727</th>
      <td>54422770</td>
      <td>http://mp.weixin.qq.com/s?__biz=MTgwNTE3Mjg2MA...</td>
      <td>【冷兔•槽】每日一冷NO.1056</td>
      <td>100001</td>
      <td>14441</td>
      <td>2016-01-31 04:26:01</td>
      <td>2016-01-31</td>
      <td>【冷兔•槽】每日一冷NO.1056</td>
    </tr>
    <tr>
      <th>250517</th>
      <td>54555560</td>
      <td>http://mp.weixin.qq.com/s?__biz=MTgwNTE3Mjg2MA...</td>
      <td>【冷兔趣闻】女人心看不透，只因胸前肉太厚！</td>
      <td>100001</td>
      <td>11403</td>
      <td>2016-01-31 12:13:09</td>
      <td>2016-01-31</td>
      <td>【冷兔趣闻】女人心看不透，只因胸前肉太厚！</td>
    </tr>
    <tr>
      <th>167375</th>
      <td>54472418</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5OTIwODMzMQ...</td>
      <td>哈哈哈！再柔软的菊花，也抗不住这坚硬的雪花</td>
      <td>100001</td>
      <td>11104</td>
      <td>2016-01-31 07:16:42</td>
      <td>2016-01-31</td>
      <td>哈哈哈！再柔软的菊花，也抗不住这坚硬的雪花</td>
    </tr>
    <tr>
      <th>151397</th>
      <td>54456440</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5OTIwODMzMQ...</td>
      <td>哈哈哈！再柔软的菊花，也抗不住这坚硬的雪花</td>
      <td>100001</td>
      <td>10312</td>
      <td>2016-01-31 06:16:10</td>
      <td>2016-01-31</td>
      <td>哈哈哈！再柔软的菊花，也抗不住这坚硬的雪花</td>
    </tr>
  </tbody>
</table>
</div>



Ok, I suppose that we should be able to join  this dataset with articles based on URL.


```python
(~clicks_no_duplicates.url.isin(pages_df.url)).sum()
```




    244265



Wow, that many don't match? 

What about the other way around?


```python
(~pages_df.url.isin(clicks_no_duplicates.url)).sum()
```




    312



Ok, so it seems that 312 articles don't have any click info? Let's check why that might be 


```python
pages_missing_clicks = pages_df[(~pages_df.url.isin(clicks_no_duplicates.url))]
pages_missing_clicks.iloc[0].url
```




    'http://mp.weixin.qq.com/s?__biz=MzA5NTQ1NDkxMw==&mid=401765553&idx=2&sn=33190232a856d6ee4f791d91b565c709&scene=6#rd'




```python
clicks_no_duplicates[clicks_no_duplicates.url.str.contains('MzA5NTQ1NDkxMw==')].url.values
```




    array(['http://mp.weixin.qq.com/s?__biz=MzA5NTQ1NDkxMw==&mid=401770146&idx=1&sn=4867359ebef83764006a6c4ad77d0b1a&scene=6#rd',
           'http://mp.weixin.qq.com/s?__biz=MzA5NTQ1NDkxMw==&mid=401770146&idx=2&sn=2bcfd09617bc69371904a48039bcc63a&scene=6#rd'],
          dtype=object)



Hmm, it seems that there realy is no click for a given url. Let's check titles


```python
pages_missing_clicks.iloc[0].title
```




    '电地暖供暖系统能给家里带来哪些好处'




```python
clicks_no_duplicates[clicks_no_duplicates.url.str.contains('MzA5NTQ1NDkxMw==')].title.values
```




    array(['新型采暖方式环保节能，电地暖七大优势', '韩达电热膜的低碳优势表现在哪些方面'], dtype=object)



Titles don't match as well. For now I'll treat is as a simple hole in data, but it's worth investigateing why it's happening.

I'll conclude EDA here

# Summary of findings

- There are frequent html escaping problems with text columns 
- TODO
