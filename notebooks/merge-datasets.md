

```python
import pandas as pd
```


```python
#                 'title',
#                 'url',
#                 'content',
#                 'read_number',
#                 'like_number',
#                 'biz_id',
#                 'biz_description',
#                 'biz_name',
#                 'publish_date',
```


```python
biz = pd.read_pickle('./biz-parsed.pickle')
biz = biz[['biz_id', 'clean_biz_name', 'clean_biz_description']]
biz.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>biz_id</th>
      <th>clean_biz_name</th>
      <th>clean_biz_description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>MzIyNjA3ODA0OA==</td>
      <td>天津师大史学社</td>
      <td>天津师大史学社是天津师范大学学生自愿结成的进行历史学习与交流的学术型社团，致力于为广大师大学...</td>
    </tr>
    <tr>
      <th>1</th>
      <td>MzI5MTEwMzUxMA==</td>
      <td>爆笑女神经</td>
      <td>本平台为您推送史上最最无厘头的搞笑段子，欢迎关注姐！！！</td>
    </tr>
    <tr>
      <th>2</th>
      <td>MjM5NTQ0MzQ1Nw==</td>
      <td>专治各种不服</td>
      <td>花花世界，无奇不有，汇聚各种奇闻异事，亮点内容，让您的生活从此丰富多彩。不服哥：专治各种不服...</td>
    </tr>
    <tr>
      <th>3</th>
      <td>MzIyMDIwMjkyOQ==</td>
      <td>名牌皮具厂批发商会</td>
      <td>香奈儿LV爱马仕MIUMIU古驰等名牌原版 超A原单女包！各名牌经典，各名牌包包最新款一网打...</td>
    </tr>
    <tr>
      <th>4</th>
      <td>MzIwMjI2NDMzOQ==</td>
      <td>说话逻辑与技巧</td>
      <td>教会你如何说话才让人感觉最舒服、最感动;帮助你快速掌握说话的技巧,说话改变命运;口才决定人生!</td>
    </tr>
  </tbody>
</table>
</div>




```python
pages = pd.read_pickle('./pages-parsed.pickle')
pages = pages[['clean_title', 'url', 'publish_time', 'raw_content', 'biz_id']]
pages.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>clean_title</th>
      <th>url</th>
      <th>publish_time</th>
      <th>raw_content</th>
      <th>biz_id</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>关注 | “寨卡”病毒中南美洲迅速蔓延 巴西蒙上阴影</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5OTE5MzE4Mg...</td>
      <td>2016-01-30 15:01:21</td>
      <td>\n\n\n\n关注 | “寨卡”病毒中南美洲迅速蔓延 巴西蒙上阴影\n\n\n\n\n\n...</td>
      <td>MjM5OTE5MzE4Mg==</td>
    </tr>
    <tr>
      <th>1</th>
      <td>㊙身为一个小姐，不能有感觉了再接客……</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5ODE2Mjk1Nw...</td>
      <td>2016-01-30 15:11:35</td>
      <td>\n\n\n\n㊙身为一个小姐，不能有感觉了再接客……\n\n\n\n\n\n\n\n\n\...</td>
      <td>MjM5ODE2Mjk1Nw==</td>
    </tr>
    <tr>
      <th>2</th>
      <td>两次战败，被揍的稀巴烂的德国为何能迅速崛起？</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5MTUyMjQ2MA...</td>
      <td>2016-01-30 15:12:37</td>
      <td>\n\n\n\n两次战败，被揍的稀巴烂的德国为何能迅速崛起？\n\n\n\n\n\n\n\n...</td>
      <td>MjM5MTUyMjQ2MA==</td>
    </tr>
    <tr>
      <th>3</th>
      <td>一个老炮儿的无奈抗争</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzA3MDU3MDc2Mw...</td>
      <td>2016-01-30 15:12:25</td>
      <td>\n\n\n\n一个老炮儿的无奈抗争\n\n\n\n\n\n\n\n\n\n\n\n\n\n...</td>
      <td>MzA3MDU3MDc2Mw==</td>
    </tr>
    <tr>
      <th>4</th>
      <td>秘一个女子该有的精致生活</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzA3MzUwOTQwNg...</td>
      <td>2016-01-30 15:11:05</td>
      <td>\n\n\n\n秘一个女子该有的精致生活\n\n\n\n\n\n\n\n\n\n\n\n\n...</td>
      <td>MzA3MzUwOTQwNg==</td>
    </tr>
  </tbody>
</table>
</div>




```python
clicks = pd.read_pickle('./clicks-parsed.pickle')
clicks = clicks[['url', 'read_number', 'like_number']]
clicks.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>url</th>
      <th>read_number</th>
      <th>like_number</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>93890</th>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5MjAxNDM4MA...</td>
      <td>100001</td>
      <td>1192</td>
    </tr>
    <tr>
      <th>36574</th>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5MDc3NjQwMA...</td>
      <td>100001</td>
      <td>794</td>
    </tr>
    <tr>
      <th>272237</th>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5MjEzNzYzOA...</td>
      <td>100001</td>
      <td>121</td>
    </tr>
    <tr>
      <th>88533</th>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5NjAwNzI0MA...</td>
      <td>100001</td>
      <td>426</td>
    </tr>
    <tr>
      <th>173763</th>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5ODI3ODI4MA...</td>
      <td>100001</td>
      <td>282</td>
    </tr>
  </tbody>
</table>
</div>




```python
merged = pages.merge(biz, on=['biz_id'], how='left').merge(clicks, on=['url'], how='left')
```


```python
merged.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>clean_title</th>
      <th>url</th>
      <th>publish_time</th>
      <th>raw_content</th>
      <th>biz_id</th>
      <th>clean_biz_name</th>
      <th>clean_biz_description</th>
      <th>read_number</th>
      <th>like_number</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>关注 | “寨卡”病毒中南美洲迅速蔓延 巴西蒙上阴影</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5OTE5MzE4Mg...</td>
      <td>2016-01-30 15:01:21</td>
      <td>\n\n\n\n关注 | “寨卡”病毒中南美洲迅速蔓延 巴西蒙上阴影\n\n\n\n\n\n...</td>
      <td>MjM5OTE5MzE4Mg==</td>
      <td>法国侨报</td>
      <td>关注《法国侨报》微信平台，即时获取您身边最新、最快的新闻时事，抢先知晓华侨华人动态。侨报竭力...</td>
      <td>33.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>㊙身为一个小姐，不能有感觉了再接客……</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5ODE2Mjk1Nw...</td>
      <td>2016-01-30 15:11:35</td>
      <td>\n\n\n\n㊙身为一个小姐，不能有感觉了再接客……\n\n\n\n\n\n\n\n\n\...</td>
      <td>MjM5ODE2Mjk1Nw==</td>
      <td>我爱服装搭配</td>
      <td>服装搭配小知识，提升自身魅力哦</td>
      <td>910.0</td>
      <td>4.0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>两次战败，被揍的稀巴烂的德国为何能迅速崛起？</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5MTUyMjQ2MA...</td>
      <td>2016-01-30 15:12:37</td>
      <td>\n\n\n\n两次战败，被揍的稀巴烂的德国为何能迅速崛起？\n\n\n\n\n\n\n\n...</td>
      <td>MjM5MTUyMjQ2MA==</td>
      <td>煮酒谈史</td>
      <td>煮酒谈史，话古说今，聆听这个时代最真实的声音。</td>
      <td>426.0</td>
      <td>6.0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>一个老炮儿的无奈抗争</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzA3MDU3MDc2Mw...</td>
      <td>2016-01-30 15:12:25</td>
      <td>\n\n\n\n一个老炮儿的无奈抗争\n\n\n\n\n\n\n\n\n\n\n\n\n\n...</td>
      <td>MzA3MDU3MDc2Mw==</td>
      <td>晓南风</td>
      <td>晓南风是中南财经政法大学的公共平台，致力于解决大家在生活中遇到的问题，及时了解学校各种信息，...</td>
      <td>88.0</td>
      <td>7.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>秘一个女子该有的精致生活</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzA3MzUwOTQwNg...</td>
      <td>2016-01-30 15:11:05</td>
      <td>\n\n\n\n秘一个女子该有的精致生活\n\n\n\n\n\n\n\n\n\n\n\n\n...</td>
      <td>MzA3MzUwOTQwNg==</td>
      <td>最佳写作网</td>
      <td>最佳写作网，欢迎关注交流</td>
      <td>0.0</td>
      <td>0.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
merged.url.value_counts().head()
```




    http://mp.weixin.qq.com/s?__biz=MzA4Mzc1NzIyNA==&mid=401863976&idx=4&sn=da79968a2e7e8b20b6fdc57d28b0964c&scene=6#rd    1
    http://mp.weixin.qq.com/s?__biz=MzA3MzAxNzQ2Mw==&mid=402003469&idx=1&sn=668950ca6fc24cdc9971b092b3e95e72&scene=6#rd    1
    http://mp.weixin.qq.com/s?__biz=MzAxNTM3NjM1NQ==&mid=406710811&idx=2&sn=770b8e0018eea73f695edb4ba29ca79e&scene=6#rd    1
    http://mp.weixin.qq.com/s?__biz=MzA3NjUyNTEyNw==&mid=401765728&idx=5&sn=d0ad9dbe2c6ffa6c6e4006c7f11846de&scene=6#rd    1
    http://mp.weixin.qq.com/s?__biz=MzA4ODE1ODMxMA==&mid=404584164&idx=3&sn=5de12b97c40ca4b71c272b917ec5cfc3&scene=6#rd    1
    Name: url, dtype: int64




```python
merged['publish_date'] = merged.publish_time.dt.date
merged.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>clean_title</th>
      <th>url</th>
      <th>publish_time</th>
      <th>raw_content</th>
      <th>biz_id</th>
      <th>clean_biz_name</th>
      <th>clean_biz_description</th>
      <th>read_number</th>
      <th>like_number</th>
      <th>publish_date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>关注 | “寨卡”病毒中南美洲迅速蔓延 巴西蒙上阴影</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5OTE5MzE4Mg...</td>
      <td>2016-01-30 15:01:21</td>
      <td>\n\n\n\n关注 | “寨卡”病毒中南美洲迅速蔓延 巴西蒙上阴影\n\n\n\n\n\n...</td>
      <td>MjM5OTE5MzE4Mg==</td>
      <td>法国侨报</td>
      <td>关注《法国侨报》微信平台，即时获取您身边最新、最快的新闻时事，抢先知晓华侨华人动态。侨报竭力...</td>
      <td>33.0</td>
      <td>0.0</td>
      <td>2016-01-30</td>
    </tr>
    <tr>
      <th>1</th>
      <td>㊙身为一个小姐，不能有感觉了再接客……</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5ODE2Mjk1Nw...</td>
      <td>2016-01-30 15:11:35</td>
      <td>\n\n\n\n㊙身为一个小姐，不能有感觉了再接客……\n\n\n\n\n\n\n\n\n\...</td>
      <td>MjM5ODE2Mjk1Nw==</td>
      <td>我爱服装搭配</td>
      <td>服装搭配小知识，提升自身魅力哦</td>
      <td>910.0</td>
      <td>4.0</td>
      <td>2016-01-30</td>
    </tr>
    <tr>
      <th>2</th>
      <td>两次战败，被揍的稀巴烂的德国为何能迅速崛起？</td>
      <td>http://mp.weixin.qq.com/s?__biz=MjM5MTUyMjQ2MA...</td>
      <td>2016-01-30 15:12:37</td>
      <td>\n\n\n\n两次战败，被揍的稀巴烂的德国为何能迅速崛起？\n\n\n\n\n\n\n\n...</td>
      <td>MjM5MTUyMjQ2MA==</td>
      <td>煮酒谈史</td>
      <td>煮酒谈史，话古说今，聆听这个时代最真实的声音。</td>
      <td>426.0</td>
      <td>6.0</td>
      <td>2016-01-30</td>
    </tr>
    <tr>
      <th>3</th>
      <td>一个老炮儿的无奈抗争</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzA3MDU3MDc2Mw...</td>
      <td>2016-01-30 15:12:25</td>
      <td>\n\n\n\n一个老炮儿的无奈抗争\n\n\n\n\n\n\n\n\n\n\n\n\n\n...</td>
      <td>MzA3MDU3MDc2Mw==</td>
      <td>晓南风</td>
      <td>晓南风是中南财经政法大学的公共平台，致力于解决大家在生活中遇到的问题，及时了解学校各种信息，...</td>
      <td>88.0</td>
      <td>7.0</td>
      <td>2016-01-30</td>
    </tr>
    <tr>
      <th>4</th>
      <td>秘一个女子该有的精致生活</td>
      <td>http://mp.weixin.qq.com/s?__biz=MzA3MzUwOTQwNg...</td>
      <td>2016-01-30 15:11:05</td>
      <td>\n\n\n\n秘一个女子该有的精致生活\n\n\n\n\n\n\n\n\n\n\n\n\n...</td>
      <td>MzA3MzUwOTQwNg==</td>
      <td>最佳写作网</td>
      <td>最佳写作网，欢迎关注交流</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>2016-01-30</td>
    </tr>
  </tbody>
</table>
</div>




```python
merged = merged[[
    'clean_title',
    'url',
    'raw_content',
    'read_number',
    'like_number',
    'biz_id',
    'clean_biz_description',
    'clean_biz_name',
    'publish_date',
]]
```


```python
merged.to_csv("./output.csv", index=False, header=False)
```


```python
merged.shape
```




    (13796, 9)




```python
pages.shape
```




    (13796, 5)


